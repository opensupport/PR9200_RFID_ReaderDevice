;******************** (C) COPYRIGHT 2010 Advanced Design Technolgy ********************
;* File Name          : startup_ADTEXM0.s
;* Author             : nsheo@adtek.co.kr
;* Version            : V1.0.2
;* Date               : 12/10/2010
;* Description        : This module performs:
;*                      - Set the initial SP (using Image$$ARM_LIB_STACKHEAP$$ZI$$Limit)
;*                      - Set the initial PC == Reset_Handler
;*                      - Set the vector table entries with the exceptions ISR address
;*                      - Branches to __main in the C library (which eventually
;*                        calls main()).
;*                      After Reset the CortexM3 processor is in Thread mode,
;*                      priority is Privileged, and the Stack is set to Main.
;**************************************************************************************

;-------------------------------------------------------------------
; Copyright (C) 2011 PHYCHIPS
;
; @file     startup_pr9200.s
; @brief	CMSIS Cortex-M0 Core Peripheral Access Layer Source File
;
;-------------------------------------------------------------------
; History
;-------------------------------------------------------------------
; 2011/10/25	jsyi	Remove SystemInit


                PRESERVE8
                THUMB

; Vector Table Mapped to Address 0 at Reset
                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors
                EXPORT  __Vectors_End
                EXPORT  __Vectors_Size
				IMPORT	||Image$$ARM_LIB_STACKHEAP$$ZI$$Limit||
__Vectors       DCD     ||Image$$ARM_LIB_STACKHEAP$$ZI$$Limit||              ; Top of Stack
                DCD     Reset_Handler             ; Reset Handler
                DCD     NMI_Handler               ; NMI Handler
                DCD     HardFault_Handler         ; Hard Fault Handler
                DCD     MemManage_Handler         ; MPU Fault Handler
                DCD     BusFault_Handler          ; Bus Fault Handler
                DCD     UsageFault_Handler        ; Usage Fault Handler
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     0                         ; Reserved
                DCD     SVC_Handler               ; SVCall Handler
                DCD     DebugMon_Handler          ; Debug Monitor Handler
                DCD     0                         ; Reserved
                DCD     PendSV_Handler            ; PendSV Handler
                DCD     SysTick_Handler           ; SysTick Handler

                ; External Interrupts
                DCD	WDT_IRQHandler			  ;Watchdog Timer Interrupt
                DCD	TIMER0_IRQHandler		  ;TIMER0 Interrupt
                DCD	TIMER1_IRQHandler         ;TIMER1 Interrupt
                DCD	TIMER2_IRQHandler		  ;TIMER2 Interrupt
                DCD	TIMER3_IRQHandler  	  	  ;TIMER3 Interrupt
                DCD	UART0_RX_IRQHandler		  ;UART PORT0 RX Interrupt
                DCD	UART0_TX_IRQHandler		  ;UART PORT0 TX Interrupt
                DCD	UART1_RX_IRQHandler       ;UART PORT1 RX Interrupt
                DCD	UART1_TX_IRQHandler		  ;UART PORT1 TX Interrupt
                DCD	SPI_RX_IRQHandler		  ;SPI RX Interrupt
                DCD	SPI_TX_IRQHandler		  ;SPI TX Interrupt
                DCD	I2C_IRQHandler			  ;I2C Interrupt
                DCD	External_0_IRQHandler  	  ;External 0 Interrupt
                DCD	External_1_IRQHandler  	  ;External 1 Interrupt
                DCD	External_2_IRQHandler  	  ;External 2 Interrupt
                DCD	External_3_IRQHandler  	  ;External 3 Interrupt
                DCD	External_4_IRQHandler  	  ;External 4 Interrupt
                DCD	External_5_IRQHandler  	  ;External 5 Interrupt
                DCD	GPIO0_IRQHandler		  ;GPIO0 Interrupt
                DCD	GPIO1_IRQHandler		  ;GPIO1 Interrupt
                DCD	WAKEINT_TX_IRQHandler	  ;Wake Tx Interrupt
                DCD	UART0_ERR_IRQHandler	  ;UART PORT0 ERROR Interrupt
                DCD	UART1_ERR_IRQHandler      ;UART PORT1 ERROR Interrupt
                DCD	SPI0_ERR_IRQHandler		  ;SPI ERROR Interrupt              
                DCD	RSSI_IRQHandler	  		  ;Reserved
                DCD	MT2_IRQHandler	  	  ;Reserved
                DCD	MT1_IRQHandler	  	   ;Reserved
                DCD	TX_DONE_IRQHandler	  ;Reserved
                DCD	RX_FAIL_IRQHandler	  ;Reserved
                DCD	RX_SUCCESS_IRQHandler	  ;Reserved
                DCD	RX_RAW_IRQHandler	  ;Reserved
                DCD	LBT_IRQHandler	  ;Reserved	
__Vectors_End

__Vectors_Size  EQU  __Vectors_End - __Vectors

                AREA    |.text|, CODE, READONLY
 

; Reset Handler

Reset_Handler	PROC
                EXPORT  Reset_Handler             [WEAK]
;				IMPORT	SystemInit		; jsyi 
; 				LDR	R0, =SystemInit		; jsyi 
;				BLX		R0				; jsyi 
                IMPORT  __main
                LDR     R0, =__main
                BX      R0
                ENDP


; Dummy Exception Handlers (infinite loops which can be modified)                

NMI_Handler     PROC
                EXPORT  NMI_Handler               [WEAK]
                B       .
                ENDP
HardFault_Handler\
                PROC
                EXPORT  HardFault_Handler         [WEAK]
                B       .
                ENDP
MemManage_Handler\
                PROC
                EXPORT  MemManage_Handler         [WEAK]
                B       .
                ENDP
BusFault_Handler\
                PROC
                EXPORT  BusFault_Handler          [WEAK]
                B       .
                ENDP
UsageFault_Handler\
                PROC
                EXPORT  UsageFault_Handler        [WEAK]
                B       .
                ENDP
SVC_Handler     PROC
                EXPORT  SVC_Handler               [WEAK]
                B       .
                ENDP
DebugMon_Handler\
                PROC
                EXPORT  DebugMon_Handler          [WEAK]
                B       .
                ENDP
PendSV_Handler  PROC
                EXPORT  PendSV_Handler            [WEAK]
                B       .
                ENDP
SysTick_Handler PROC
                EXPORT  SysTick_Handler           [WEAK]
                B       .
                ENDP

; PR9200 interrupt handlers
Default_Handler PROC
                EXPORT	WDT_IRQHandler			  [WEAK]
                EXPORT	TIMER0_IRQHandler		  [WEAK]
                EXPORT	TIMER1_IRQHandler         [WEAK]
                EXPORT	TIMER2_IRQHandler		  [WEAK]
                EXPORT	TIMER3_IRQHandler  	  	  [WEAK]
                EXPORT	UART0_RX_IRQHandler		  [WEAK]
				EXPORT	UART0_TX_IRQHandler		  [WEAK]
                EXPORT	UART1_RX_IRQHandler       [WEAK]
                EXPORT	UART1_TX_IRQHandler		  [WEAK]
                EXPORT	SPI_RX_IRQHandler		  [WEAK]
				EXPORT	SPI_TX_IRQHandler		  [WEAK]
                EXPORT	I2C_IRQHandler			  [WEAK]
                EXPORT	External_0_IRQHandler  	  [WEAK]
                EXPORT	External_1_IRQHandler  	  [WEAK]
                EXPORT	External_2_IRQHandler  	  [WEAK]
                EXPORT	External_3_IRQHandler  	  [WEAK]
                EXPORT	External_4_IRQHandler  	  [WEAK]
                EXPORT	External_5_IRQHandler  	  [WEAK]
                EXPORT	GPIO0_IRQHandler		  [WEAK]
                EXPORT	GPIO1_IRQHandler		  [WEAK]
                EXPORT	WAKEINT_TX_IRQHandler	  [WEAK]
                EXPORT	UART0_ERR_IRQHandler	  [WEAK]
                EXPORT	UART1_ERR_IRQHandler      [WEAK]
                EXPORT	SPI0_ERR_IRQHandler	  	  [WEAK]
                EXPORT	RSSI_IRQHandler	  		  [WEAK]
                EXPORT	MT2_IRQHandler	  		  [WEAK]
                EXPORT	MT1_IRQHandler	  		  [WEAK]
                EXPORT	TX_DONE_IRQHandler	  	  [WEAK]
                EXPORT	RX_FAIL_IRQHandler	  	  [WEAK]
				EXPORT	RX_SUCCESS_IRQHandler	  [WEAK]
				EXPORT	RX_RAW_IRQHandler	  	  [WEAK]
				EXPORT	LBT_IRQHandler	  [WEAK]
				
; Dummy Exception Handlers (infinite loops which can be modified)

WDT_IRQHandler
TIMER0_IRQHandler
TIMER1_IRQHandler
TIMER2_IRQHandler
TIMER3_IRQHandler
UART0_RX_IRQHandler
UART0_TX_IRQHandler
UART1_RX_IRQHandler
UART1_TX_IRQHandler
SPI_RX_IRQHandler
SPI_TX_IRQHandler
I2C_IRQHandler
External_0_IRQHandler
External_1_IRQHandler
External_2_IRQHandler
External_3_IRQHandler
External_4_IRQHandler
External_5_IRQHandler
GPIO0_IRQHandler
GPIO1_IRQHandler
WAKEINT_TX_IRQHandler
UART0_ERR_IRQHandler
UART1_ERR_IRQHandler
SPI0_ERR_IRQHandler
RSSI_IRQHandler
MT2_IRQHandler
MT1_IRQHandler
TX_DONE_IRQHandler
RX_FAIL_IRQHandler
RX_SUCCESS_IRQHandler
RX_RAW_IRQHandler
LBT_IRQHandler	   
                B       .
                ENDP

                ALIGN
                END

