/******************** (C) COPYRIGHT 2010 Advanced Design Technolgy ********************
 * File Name		: system_ADTM3F3SM.c
 * Author			: nsheo@adtek.co.kr
 * Version			: V1.0.0
 * Date				: 05/08/2010
 * brief			: CMSIS Cortex-M3 Device Peripheral Access Layer System Source File.
 **************************************************************************************/
#include "system_PR9200.h"

/*
* -------------------------------------------------------------------
* Copyright (C) 2011 PHYCHIPS
* 
* @file     system_PR9200.h 
* @brief	CMSIS Cortex-M0 Device Peripheral Access Layer Source File
*   		for PR9200
*           
* -------------------------------------------------------------------
*  History
* -------------------------------------------------------------------
* 2011/10/25	jsyi 	Initial Release
*/


#if (0) // jsyi
uint32_t SystemCoreClock	= SYS_DIG_CLK;


/* void SystemCoreClockUpdate(void)
 * brief : Update SystemCoreClock according to Clock Register Values
 */
void SystemCoreClockUpdate(void)
{
	/* Needs to be implemented */	
}


/* void SystemInit(void)
 * brief : Initialize the system
 */
void SystemInit(void)
{
	/* SMC/DMC Configuration */
	/* PLL Setting */
}



/*------------------------------------------------------------------------------
  System Hadler Functions
 *------------------------------------------------------------------------------*/
volatile uint32_t msTicks=0;
void SysTick_Handler(void)
{
	msTicks++;
}
/*------------------------------------------------------------------------------
  delays number of tick Systicks (happens every 1 ms)
 *------------------------------------------------------------------------------*/
void Delay_ms(uint32_t dlyTicks)
{
  uint32_t curTicks;

  curTicks = msTicks;
  while ((msTicks - curTicks) < dlyTicks);
}

#endif
