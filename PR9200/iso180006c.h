//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	iso180006c.h
//! @brief	ISO18000-6C Protocol Stack
//! 
//! $Id: iso180006c.h 1630 2012-06-22 01:48:25Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release


#ifndef     ISO180006C_H
#define     ISO180006C_H

//!-------------------------------------------------------------------
//! Include Files
//!-------------------------------------------------------------------
#include "config.h"
#include "commontypes.h"
#include "rfidtypes.h"
#include "modem.h"

//!-------------------------------------------------------------------
//! Data References
//!-------------------------------------------------------------------



//!-------------------------------------------------------------------
//! Definitions
//!-------------------------------------------------------------------
#define TAG_ID_MAX		100

#define CUR_STATE_SEL_QRY  			(0x00)
#define CUR_STATE_QRY_REP  			(0x01)
#define CUR_STATE_RN16  			(0x02)
#define CUR_STATE_ACK  				(0x03)
#define CUR_STATE_PCUIICRC  		(0x04)
#define CUR_STATE_NACK				(0x06)
#define CUR_STATE_SEL_EAS_ALARM 	(0x08)
#define CUR_STATE_QRY_ADJ  			(0x09)

#define INVENTORY_TAG_BUF_FULL		(-1)
#define INVENTORY_MAX_TAG_IN_ROUND	(50)
//!-------------------------------------------------------------------
//! Strunctures and enumerations
//!-------------------------------------------------------------------
typedef struct iso180006c_prm_sel_type
{
    uint8   target      ;
    uint8   action      ;
    uint8   mem_bank    ;
    uint16  pointer     ;
    uint8   len      	;
    uint8   mask[64]    ;
    uint8   truncate    ;
} iso180006c_prm_sel_type;

typedef struct iso180006c_prm_qry_type
{
    uint8   dr          ;
    uint8   m           ;
    uint8   trext       ;
    uint8   sel         ;
    uint8   session     ;
    uint8   target      ;
	uint8   q           ;
    uint8	updn		;
} iso180006c_prm_qry_type;

typedef struct iso180006c_prm_type
{
	iso180006c_prm_sel_type sel;
	iso180006c_prm_qry_type qry;
} iso180006c_prm_type;

typedef struct iso180006c_inventory_prm_type
{
	BOOL	update;
	uint8	anti_col;
} iso180006c_inventory_prm_type;	

typedef struct iso180006c_tag_buffer_type
{
	uint8 count;
	rfid_pkt_rx_type id[TAG_ID_MAX];
} iso180006c_tag_buffer_type;

typedef struct iso180006c_rfid_pkt_type
{
 	rfid_packet_type sel;
	rfid_packet_type qry;
	rfid_packet_type qryrep;
#ifdef __FEATURE_TYPEC_QRYADJ__
	rfid_packet_type qryadj;
#endif
} iso180006c_rfid_pkt_type;

//!-------------------------------------------------------------------
//! Macros
//!-------------------------------------------------------------------

//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
void iso180006c_init(void);
BOOL iso180006c_set_modulation_param(modem_rx_blf_type rx_blf, modem_rx_mod_type rx_mod, modem_rx_dr_type rx_dr);
void iso180006c_set_anticol_mode(protocol_anti_col_type mode);
void iso180006c_reset_inventory_mode(void);

rfid_pkt_rx_type* iso180006c_pop_tag(void);

void iso180006c_pktbuild_sel(rfid_packet_type *pkt);
void iso180006c_pktbuild_qry(rfid_packet_type *pkt);
void iso180006c_pktbuild_qryrep(rfid_packet_type *pkt);
#if(0)
void iso180006c_pktbuild_qryadj(rfid_packet_type *pkt);
void iso180006c_pktbuild_nack(rfid_packet_type *pkt);
#endif

int8 iso180006c_select_inventory(void);
BOOL iso180006c_handle_acquisition(const uint8 *target_ptr, uint8 target_len, rfid_pkt_rx_type *pc_epc_crc);

BOOL iso180006c_req_rn(rfid_packet_type *new_rn);
BOOL iso180006c_read(const protocol_c_membank_type mb, const uint16 wort_prt, const uint8 word_count, rfid_packet_long_type *tag_data);
BOOL iso180006c_write(const protocol_c_membank_type mb, const uint16 wort_prt, const uint16 word_data);
BOOL iso180006c_kill(const uint32 kill_password, const uint8 recom);
BOOL iso180006c_access(const uint32 access_password);
BOOL iso180006c_lock(const uint32 mask_and_action);

BOOL iso180006c_blockwrite(const protocol_c_membank_type mb, const uint16 wort_prt, const uint8 word_count, const uint16 *word_data);
BOOL iso180006c_blockerase(const protocol_c_membank_type mb, const uint16 wort_prt, const uint8 word_count);

#ifdef __FEATURE_SIM_TAG__
void iso180006c_push_simtag(void);
#endif

#ifdef __FEATURE_NXP_UCODE__
BOOL iso180006c_readprotect(void);
BOOL iso180006c_reset_readprotect(uint32 reset_readprotect_password);
BOOL iso180006c_change_eas(uint8 change);
BOOL iso180006c_eas_alarm(rfid_packet_long_type *tag_data);
BOOL iso180006c_calibrate(rfid_packet_long_type *tag_data);
#endif

#endif

