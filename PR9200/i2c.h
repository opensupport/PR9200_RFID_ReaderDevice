//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	i2c.h
//! @brief	I2C Device Driver
//! 
//! $Id: i2c.h 1671 2012-07-13 00:36:02Z jsyi $			
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release


#ifndef I2C_H
#define I2C_H



//Control Register
#define I2C_ACK_GEN_EN	BIT7
#define I2C_NORMAL_MODE	BIT6	//FASTMODE 0x00
#define I2C_INT_EN		BIT5
#define I2C_INT_PEND	BIT4
// Fast   Mode : I2C SCLK = PCLK / 16  / PRESCALE 
// Normal Mode : I2C SCLK = PCLK / 512 / PRESCALE 
#define I2C_PRESCALE_1	(0x00)
#define I2C_PRESCALE_2	(0x01)
#define I2C_PRESCALE_3	(0x02)
#define I2C_PRESCALE_4	(0x03)
#define I2C_PRESCALE_5	(0x04)
#define I2C_PRESCALE_6	(0x05)
#define I2C_PRESCALE_7	(0x06)
#define I2C_PRESCALE_8	(0x07)
#define I2C_PRESCALE_9	(0x08)
#define I2C_PRESCALE_10	(0x09)
#define I2C_PRESCALE_11	(0x0a)
#define I2C_PRESCALE_12	(0x0b)
#define I2C_PRESCALE_13	(0x0c)
#define I2C_PRESCALE_14	(0x0d)
#define I2C_PRESCALE_15	(0x0e)
#define I2C_PRESCALE_16	(0x0f)

// Status Register
#define	I2C_SLV_RX_MODE	(0<<6)
#define	I2C_SLV_TX_MODE	(1<<6)
#define	I2C_MST_RX_MODE	(2<<6)
#define	I2C_MST_TX_MODE	(3<<6)
#define I2C_GEN_START	(BIT5)
#define I2C_GNE_STOP	(BIT5_CLR)
#define I2C_BUSY		(BIT5)
#define	I2C_DATA_OUT_EN	(BIT4)
#define	I2C_ARBIT_FAIL	(BIT3)
#define	I2C_IAR_MATCH	(BIT2)
#define	I2C_IAR_ZERO	(BIT1)
#define	I2C_RX_NACK		(BIT0)

#define I2C_HOST_REQ_W	(BIT0_CLR)
#define I2C_HOST_REQ_R	(BIT0_SET)


#define I2C_GEN_NACK() 	I2C->ICCR &= ~I2C_ACK_GEN_EN;
											   	
#if defined(__FEATURE_I2C_SLAVE_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 	
void i2c_slave_init(void);
#endif

void i2c_master_init(void);
uint8 i2c_master_read_start(uint8 savle_addr);
uint8 i2c_master_write_start(uint8 savle_addr);	
uint8 i2c_master_read(void);
BOOL i2c_master_write(uint8 data);
BOOL i2c_gen_stop(void);

#endif
