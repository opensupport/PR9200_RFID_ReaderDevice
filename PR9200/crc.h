//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	crc.h
//! @brief	CRC Module
//! 
//! $Id: crc.h 1597 2012-05-04 07:48:02Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2011/07/25	sjpark		initial release


#ifndef CRC_H
#define CRC_H


#include "commontypes.h"		 

#define CRC_INIT 0xFFFF


uint16 crc_polynomial(uint8 b, uint16 acc);					  
uint16 crc_sum(uint8* message, uint16 length, uint16 crc);			
uint32 crc_verify(uint8* message, uint16 length);						 
void crc_append(uint8* message, uint16 length);

#endif

