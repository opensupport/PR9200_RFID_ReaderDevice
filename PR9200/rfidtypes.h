//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	rfidtypes.h
//! @brief	Type (re)definition and macros for RFID
//! 
//! $Id: rfidtypes.h 1702 2012-08-02 02:05:39Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/21	jsyi	initial release

#ifndef RFIDTYPES_H
#define RFIDTYPES_H

#include "commontypes.h"

//!-------------------------------------------------------------------                 
//! SYSTEM ERROR CODE
//!-------------------------------------------------------------------

typedef int8 SYS_S;

#define SYS_S_OK 								(0)

#define SYS_S_HAL_ERR_BASE						(-126)
#define SYS_S_HAL_ERR_INVALID_PARAM				(-125)
#define	SYS_S_HAL_ERR_TEST_MODE					(-124)
#define	SYS_S_HAL_ERR_RFIDBLK_CTRL				(-123)
#define	SYS_S_HAL_ERR_CW_CTRL					(-122)
#define SYS_S_HAL_ERR_DEEP_SLEEP				(-121)
#define SYS_S_HAL_ERR_SET_REGION				(-120)
#define SYS_S_HAL_ERR_SET_CH					(-119)

#define SYS_S_HAL_ERR_ERASE_REGISTRY			(-118)
#define SYS_S_HAL_ERR_WRITE_REGISTRY			(-117)
#define SYS_S_HAL_ERR_REG_ADD_NOT_EXIST			(-116)

#define SYS_S_HAL_ERR_I2C_NACK					(-115)

#define SYS_S_HAL_ERR_NULL_DEV_TYPE				(-114)
#define SYS_S_HAL_ERR_GPIO_RESERVED				(-113)
#define SYS_S_HAL_ERR_NULL_EXTINT_TYPE			(-112)

#define SYS_S_HAL_ERR_TIMER_ALREADY_REGISTERED	(-111)

#define SYS_S_PRT_ERR_BASE						(-60)
#define SYS_S_PRT_ERR_INVALID_PARAM				(-59)
#define SYS_S_PRT_ERR_BUSY						(-58)
#define SYS_S_PRT_ERR_NO_TAG					(-57)
#define SYS_S_PRT_ERR_ACCESS_TAG				(-56)
#define SYS_S_PRT_ERR_READ_MEM					(-55)
#define SYS_S_PRT_ERR_MEM_OVERRUN				(-54) // Memory overrun or unsupported PC value
#define SYS_S_PRT_ERR_MEM_LOCK					(-53) // Memory locked
#define SYS_S_PRT_ERR_TAG_LOW_POWER				(-52) // Insufficient power
#define SYS_S_PRT_ERR_TAG_KNOWN					(-51) // Known error
#define SYS_S_PRT_ERR_WRITE_MEM					(-50) 
#define SYS_S_PRT_ERR_KILL_TAG					(-49) 
#define SYS_S_PRT_ERR_LOCK_TAG					(-48) 
#define SYS_S_PRT_ERR_READPROTECT_TAG			(-47) 
#define SYS_S_PRT_ERR_RESET_READPROTECT_TAG		(-46)
#define SYS_S_PRT_ERR_CHANGE_EAS_TAG			(-45)
#define SYS_S_PRT_ERR_EAS_ALARM_TAG				(-44)
#define SYS_S_PRT_ERR_CALIBRATE_TAG				(-43)
#define SYS_S_PRT_ERR_ERASE_MEM					(-42) 
#define SYS_S_PRT_ERR_INVALID_CMD_SEL			(-41)

		
//!-------------------------------------------------------------------                 
//! HAL
//!------------------------------------------------------------------- 



#define HAL_FREQ_HOPPING_TBL_MAX	50


typedef enum hal_rfidblk_ctrl_type
{
	HAL_RFIDBLK_OFF,
	HAL_RFIDBLK_ON	
} hal_rfidblk_ctrl_type;

typedef enum hal_fh_ctrl_type
{
	HAL_FH_OFF,
	HAL_FH_ON
} hal_fh_ctrl_type;

typedef enum hal_lbt_ctrl_type
{
	HAL_LBT_OFF,
	HAL_LBT_ON
} hal_lbt_ctrl_type;

typedef enum hal_cw_ctrl_type
{
	HAL_CW_OFF,
	HAL_CW_ON
} hal_cw_ctrl_type;

typedef enum hal_pwrmgmt_mode_type
{
	PMODE_S0_DEEPSLEEP,
	PMODE_S1_SLEEP,
	PMODE_S2_IDLE,
	PMODE_S3_RFPLLRX,
	PMODE_S4_RFPLLRXTX
} hal_pwrmgmt_mode_type;

typedef struct hal_pwrmgmt_type
{
	hal_pwrmgmt_mode_type 	pwr_mode;
} hal_pwrmgmt_type;

typedef enum
{
	HAL_GPIO_DEV_NONE = 0,
	HAL_GPIO_DEV_RESERVE,
	HAL_GPIO_DEV_SIO_SEL,
	HAL_GPIO_DEV_USER,	
	HAL_GPIO_DEV_EXT_WAKEUP,
	HAL_GPIO_DEV_LED,
	HAL_GPIO_DEV_BEEP,
	HAL_GPIO_DEV_PWR_SWITCH,
	HAL_GPIO_DEV_ANT_SWITCH0,
	HAL_GPIO_DEV_ANT_SWITCH1,
	HAL_GPIO_DEV_EXT_PA_ENABLE,
	HAL_GPIO_DEV_PA_MODE_SEL,
	HAL_GPIO_DEV_EXT_LINK_ENABLE, //ex. bluetooth
	HAL_GPIO_DEV_WAKE_UP_OUT,
	HAL_GPIO_DEV_OUT,
	HAL_GPIO_DEV_IN,
	HAL_GPIO_DEV_EXT_INT,
} hal_gpio_device_type;

typedef enum
{
	HAL_GPIO_EXTINT_LEVEL_HIGH = 0,
	HAL_GPIO_EXTINT_LEVEL_LOW,
	HAL_GPIO_EXTINT_EDGE_BOTH,
	HAL_GPIO_EXTINT_EDGE_SINGLE_HIGH,	
	HAL_GPIO_EXTINT_EDGE_SINGLE_LOW,
	HAL_GPIO_EXTINT_NONE
} hal_gpio_extint_type;

typedef void(*hal_gpio_portout_pf	) (void *io);
typedef void(*hal_gpio_extint_pf	) (void *io);
typedef void(*hal_gpio_handler_pf	) (void);
typedef uint8(*hal_gpio_portin_pf	) (void *io);

typedef struct
{
	hal_gpio_device_type dev;
	uint8 port;
	uint8 bit;
	uint8 pol; // polartiry	
} hal_gpio_id_type;
						  
typedef struct 
{
	hal_gpio_id_type id;
	hal_gpio_portout_pf on_cb;
	hal_gpio_portout_pf off_cb;
	hal_gpio_portout_pf toggle_cb;
} hal_gpio_out_type;

typedef struct 
{
	hal_gpio_id_type id;
	hal_gpio_portin_pf in_cb;
} hal_gpio_in_type;

typedef struct 
{
	hal_gpio_id_type id;
	hal_gpio_extint_type mode;
	hal_gpio_portin_pf in_cb;
	hal_gpio_extint_pf enint_cb;
	hal_gpio_extint_pf disint_cb;
	hal_gpio_handler_pf handler_cb;
}hal_gpio_ext_in_type;

#define HAL_GPIO_POL_NORMAL		(0)
#define HAL_GPIO_POL_INVERT		(1)
#define HAL_GPIO_POL_NA			(2)

typedef void(*hal_timer_pf	) ();
typedef void(*hal_timer_load_pf)(uint32 val);

typedef struct hal_timer_type
{
	hal_timer_pf 		start_cb;
	hal_timer_pf 		stop_cb;
	hal_timer_load_pf 	load_cb;
	hal_timer_pf 		irq_handler_cb;
}hal_timer_type;

typedef struct hal_rf_freq_hopping_table_type
{
	uint8 size;
	uint8* table_ptr;
} hal_rf_freq_hopping_table_type;


typedef struct hal_fhlbt_prm_type
{
	uint16	tx_on_time;
	uint16	tx_off_time;
	uint16	sense_time;
	int16  	lbt_rf_level;	
	uint8	fh_enable;
	uint8	lbt_enable;
	uint8	cw_enable;	
} hal_fhlbt_prm_type;

typedef struct hal_serial_no_type
{
	uint8 size;
	uint8* ptr;
} hal_serial_no_type;

//!-------------------------------------------------------------------                 
//! PROTOCOL
//!------------------------------------------------------------------- 
#define INVENTORY_COUNT_INFINITE	(0xFFFF)
#define INVENTORY_MTNU_INFINITE		(0xFF)
#define INVENTORY_MTIME_INFINITE	(0xFF)

#define TAG_CB_COUNT_MAX			(2)

typedef struct protocol_modulation_type
{
	uint8	rx_mod;
	uint16	rx_blf;
	uint8	rx_dr;
} protocol_modulation_type;

typedef void(*protocol_tag_report_cb_type)(uint8 rssi, uint8 len, uint8* data);
typedef void(*protocol_tag_completed_cb_type)(uint8 discontinued, uint8 detected);

typedef enum
{
	ANTI_COL_MODE0, 
	ANTI_COL_MODE1,	
	ANTI_COL_MODE2, 
	ANTI_COL_MODE3, 
	ANTI_COL_MODE4, 
	ANTI_COL_MODE5, 
	ANTI_COL_MODE6, 	
	ANTI_COL_MODE_MAX
} protocol_anti_col_type;

typedef struct protocol_c_prm_sel_type
{
    uint8   target      ;
    uint8   action      ;
    uint8   mem_bank     ;
    uint16  pointer     ;
    uint8   len      ;
    uint8   mask[64]    ;
    uint8   truncate    ;
} protocol_c_prm_sel_type;

typedef struct protocol_c_prm_qry_type      
{
    uint8   dr          ;
    uint8   m           ;
    uint8   trext       ;
    uint8   sel         ;
    uint8   session     ;
    uint8   target      ;
	uint8   q           ;
} protocol_c_prm_qry_type;


typedef enum protocol_command_code_type
{
	CMD_INVENTORY_SINGLE,
	CMD_INVENTORY_MULTIPLE_CYCLE,
	CMD_INVENTORY_MULTIPLE_CYCLETIMENUM,
	CMD_INVENTORY_ONE_TAG,
	CMD_WRITE,
	CMD_BLOCK_WRITE,
	CMD_READ,
	CMD_BLOCK_ERASE,
	CMD_CODE_MAX
} protocol_command_code_type;

typedef enum protocol_c_membank_type
{
	MEM_RESERVE = 0x00,
	MEM_EPC		= 0x01,
	MEM_TID		= 0x02,
	MEM_USER	= 0x03,
	MEM_TID_USER= 0x04
} protocol_c_membank_type;

typedef struct protocol_c_prm_read_erase_type
{
	uint8 target_id_len; 	// EPC length of target tag_buf
	uint8 *target_id_ptr; 	// EPC pointer of target tag_buf 
	uint32 access_pw; 		// access password
	protocol_c_membank_type mem_bank; 		// memory bank
	uint16 start_add; 		// start address
	uint8 word_count; 		// word count
	uint8 cmd_sel;			// read/erase
	uint8 retry;
} protocol_c_prm_read_erase_type;

typedef struct protocol_c_prm_write_type
{
	uint8 target_id_len; 	// EPC length of target tag_buf
	uint8 *target_id_ptr; 	// EPC pointer of target tag_buf 
	uint32 access_pw; 		// access password
	protocol_c_membank_type mem_bank; 		// memory bank
	uint16 start_add; 		// start address
	uint8 word_count; 		// word count
	uint16 *word_data_ptr;	// word data pointer
	uint8 cmd_sel;
	uint8 retry;
} protocol_c_prm_write_type;

typedef struct protocol_c_prm_kill_type
{
	uint8 target_id_len; 	// EPC length of target tag_buf
	uint8 *target_id_ptr; 	// EPC pointer of target tag_buf 
	uint32 kill_pw; 		// access password
	uint8 recom;			// Recommissioning bits
	uint8 retry;
} protocol_c_prm_kill_type;

typedef struct protocol_c_prm_lock_type
{
	uint8 target_id_len; 	// EPC length of target tag_buf
	uint8 *target_id_ptr; 	// EPC pointer of target tag_buf 
	uint32 access_pw; 		// access password
	uint32 mask_action;
	uint8 retry;
} protocol_c_prm_lock_type;

typedef struct rfid_pkt_rx_type
{
	uint8 byte_length;
	uint8 air_pkt[72];
} rfid_pkt_rx_type;

#ifdef __FEATURE_NXP_UCODE__
typedef struct protocol_c_prm_readprotect_type
{
	uint8 target_id_len;
	uint8 *target_id_ptr;
	uint32 access_pw;
	uint8 retry;
}protocol_c_prm_readprotect_type;

typedef struct protocol_c_prm_reset_readprotect_type
{ 
	uint32 readprotect;
	uint8 retry;
}protocol_c_prm_reset_readprotect_type;

typedef struct protocol_c_prm_change_eas_type
{
	uint8 target_id_len;
	uint8 *target_id_ptr;
	uint32 access_pw;	
	uint8 change;
	uint8 retry;
}protocol_c_prm_change_eas_type;

typedef struct protocol_c_prm_eas_alarm_type
{
//	uint8 dr;
//	uint8 m;
//	uint8 trext;
	uint8 retry;	
}protocol_c_prm_eas_alarm_type;

typedef struct protocol_c_prm_calibrate_type
{
	uint8 target_id_len;
	uint8 *target_id_ptr;
	uint32 access_pw;
	uint8 retry;
}protocol_c_prm_calibrate_type;
#endif


//!-------------------------------------------------------------------                 
//! FSM
//!-------------------------------------------------------------------
typedef struct rfid_inventory_prm_type
{
	protocol_command_code_type	cmd_sel;
	uint16	count;
	uint8	mtnu; 	// maximum number of tag_buf to read
	uint8	mtime; 	// maximum elapsed time to tagging
} rfid_inventory_prm_type;

typedef struct rfid_data_type
{
	uint16 len;
	uint8* ptr_data;	
} rfid_data_type;

//!-------------------------------------------------------------------                 
//! REGISTRY
//!------------------------------------------------------------------- 
#define	REG_VER	 (0x01)

typedef struct registry_item_type
{
	uint8 size;
	void* item;
} registry_item_type;

typedef struct reg_active_type
{
	uint8 val;
} reg_active_type;

typedef struct reg_ver_type
{
	reg_active_type active;
	uint8 ver;
} reg_ver_type;

typedef struct reg_fw_data_type
{
	reg_active_type active;
	uint8 date[3];
} reg_fw_data_type;

typedef struct reg_band_type
{
	reg_active_type active;
	uint8 region;			// Operating Region
	uint8 cur_ch; 		// Current Channel Number
	uint8 cur_ch_ext;		// Current Channel Number Extension
} reg_band_type;

typedef struct reg_tx_type
{
	reg_active_type active;
	int16 power;
} reg_tx_type;

typedef struct reg_fhlbt_type
{
	reg_active_type active;
	hal_fhlbt_prm_type prm;
} reg_fhlbt_type;

typedef struct reg_anticol_type
{
	reg_active_type active;
	protocol_anti_col_type mode;
}	reg_anticol_type;

typedef struct reg_modulation_type
{
	reg_active_type active;
	uint8 mode;
}	reg_modulation_type;

typedef struct reg_prt_prm_type
{
	reg_active_type active;
	uint8 qry_q;
}	reg_prt_prm_type;

typedef struct reg_fh_tbl_type
{
	reg_active_type	active;
	uint8 size;
	uint8 item[HAL_FREQ_HOPPING_TBL_MAX];
}	reg_fh_tbl_type;

// for teset
typedef struct reg_sys_reset_type
{
	reg_active_type active;
	uint8 reset;
	protocol_c_prm_sel_type sel;
	protocol_c_prm_qry_type qry;
	rfid_inventory_prm_type param;
}reg_sys_reset_type;

typedef struct reg_serial_no_type
{
	reg_active_type active;
	uint8 serial_no[12];
}	reg_serial_no_type;

typedef struct reg_sys_off_type
{
	reg_active_type active;
	uint16 time;
}	reg_sys_off_type;
	
#endif // RFIDTYPES_H
