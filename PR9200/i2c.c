//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	i2c.c
//! @brief	I2C Device Driver
//! 
//! $Id: i2c.c 1748 2012-09-06 08:07:56Z sjpark $			
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release


//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include <STRING.H>
#include "config.h"
#include "PR9200.h"
#include "sio.h"
#include "rcp.h"
#include "i2c.h"

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------  
extern sio_byte_buf_type 	sio_rx_buf;
extern sio_pkt_queue_type 	sio_tx_buf_queue;
 
//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------  
#define I2C_SET_PORT()		(GPIO1->AFSEL |= 0x06)
#define I2C_CLR_PORT()		(GPIO1->AFSEL &= ~(0x06))
#define I2C_CLR_PENDING()	(I2C->ICCR &= ~(I2C_INT_PEND))
#define I2C_WAIT_PENDING()	while(!(I2C->ICCR & I2C_INT_PEND))
#define I2C_SLAVE_ADD		(0x92) // 1001001x (0x49)

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  

//!---------------------------------------------------------------
//!	@brief
//!		Check ACK 
//!
//! @param
//!		None
//!
//! @return
//!		NAK(0) / ACK(1)
//!
//!---------------------------------------------------------------
uint8 i2c_check_ack(void)
{
	if((I2C->ICSR) & 0x01)		// Slave NAK
		return 0;
	else return 1;
}

#if defined(__FEATURE_I2C_SLAVE_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 	

//!---------------------------------------------------------------
//!	@brief
//!		Initiate the I2C slave
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void i2c_slave_init(void)
{
	I2C_SET_PORT();	
	SIO_INIT_IRQ();
	SIO_CLR_IRQ();

	sio_byte_buf_flush(&sio_rx_buf);
	sio_pkt_queue_flush(&sio_tx_buf_queue);

	// IR 
	I2C->ICCR 	= 0;
	I2C->ICSR 	= 0;
	I2C->IAR 	= I2C_SLAVE_ADD; 
	I2C->ICCR 	= I2C_ACK_GEN_EN | I2C_INT_EN;
 	I2C->ICSR 	= I2C_SLV_RX_MODE | I2C_DATA_OUT_EN; 
	
	NVIC_EnableIRQ(I2C_IRQn);
}

//!---------------------------------------------------------------
//!	@brief
//!		Write a character to the I2C 
//!
//! @param
//!		*buf : data
//!		len : data length
//! @return
//!		None
//!
//!---------------------------------------------------------------
void i2c_slave_write(uint8 *buf, uint8 len)
{	
  	uint8 i;
 	uint8 dummy = dummy;

	I2C->ICSR &= 0x3F; // SLV_RX_MODE
	I2C->ICSR |= I2C_SLV_TX_MODE;
//	I2C->ICSR |= I2C_DATA_OUT_EN;	//Serial Output Enable		
	
	for ( i = 0; i < len; i++ )
  	{ 	
		if(i2c_check_ack()==0x0)
			break;

		I2C->IDSR = *buf;		
		I2C_CLR_PENDING();
		I2C_WAIT_PENDING(); //wait for Interrupt pending				
		buf++;
	}
	
//	I2C->ICSR &= ~I2C_DATA_OUT_EN;	//Serial Output Disable
	I2C->ICSR &= 0x3F; // SLV_RX_MODE

}


//!---------------------------------------------------------------
//!	@brief
//!		I2C Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void I2C_IRQHandler(void) 
{
	uint8 inbyte;

	inbyte = I2C->IDSR;

//	if((I2C->ICSR & I2C_IAR_MATCH) && (inbyte & I2C_HOST_REQ_R))  // SLV_TX_MODE
	if(inbyte == (I2C_SLAVE_ADD | I2C_HOST_REQ_R))
	{
		sio_rx_buf.len = 0;				
		
		if(sio_tx_buf_queue.count > 0)
		{	
			sio_byte_buf_type* q;					
			q = sio_pkt_dequeue(&sio_tx_buf_queue);	
			if(q != NULL)
				i2c_slave_write(&(q->data[0]), q->len);
		} 
		SIO_CLR_IRQ();

		if(sio_tx_buf_queue.count > 0)
		{					
			delay_ms(1); // 1 ms
			SIO_SET_IRQ();
		}
		I2C_CLR_PENDING();	
		return;
	}			

	if ( sio_rx_buf.len  >= (SIO_BYTE_BUF_MAX - 1) ) 
	{
		sio_rx_buf.len = 0;
		sio_byte_buf_flush(&sio_rx_buf);		
	}
	
	if ( (sio_rx_buf.len == 0 ) && (inbyte != RCP_PKT_PRAMBL)  ) 
	{
		SIO_CLR_IRQ();
		I2C_CLR_PENDING();	
		return;
	}
	
	sio_byte_buf_enqueue(&sio_rx_buf, inbyte);

	if(sio_rx_buf.len  >= (sio_rx_buf.data[4] + RCP_HEADEND_CRC_LEN))
	{
		if(	sio_rx_buf.data[sio_rx_buf.data[4] + RCP_PRAMBL_LEN + RCP_HEADER_LEN] == RCP_PKT_ENDMRK) 
		{			
			sio_post_received_data(&sio_rx_buf.data[0],sio_rx_buf.len);		
		}
		else
		{					
			sio_byte_buf_flush(&sio_rx_buf);				
		}
		I2C_CLR_PENDING();	
		return;
	}

	I2C_CLR_PENDING();	 	
}

#endif


//!---------------------------------------------------------------
//!	@brief
//!		Initailize the I2C master
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void i2c_master_init()
{
	I2C_SET_PORT();

	I2C->ICCR = 0;
	I2C->ICCR  = I2C_MST_TX_MODE  | I2C_PRESCALE_10 | I2C_INT_EN; 
}

//!---------------------------------------------------------------
//!	@brief
//!		Set the master to the write mode
//!		Generates the start condition and send the slave address + w
//!
//! @param
//!		slave address
//!
//! @return
//!		NAK(0) / ACK(1)
//!
//!---------------------------------------------------------------
uint8 i2c_master_write_start(uint8 savle_addr)
{
	I2C->ICSR = 0;									
	I2C->ICSR |= I2C_DATA_OUT_EN;					
	I2C->IDSR = savle_addr;							
	I2C->ICSR |= I2C_MST_TX_MODE | I2C_GEN_START;	
	I2C_WAIT_PENDING();								

	return i2c_check_ack();
}

//!---------------------------------------------------------------
//!	@brief
//!		Set the master to the read mode
//!		Generates the start condition and send the slave address + r
//!
//! @param
//!		slave address
//!
//! @return
//!		NAK(0) / ACK(1)
//!
//!---------------------------------------------------------------
uint8 i2c_master_read_start(uint8 savle_addr)
{
	I2C->ICCR |= I2C_ACK_GEN_EN;	
	I2C->ICSR = 0;				
	I2C->ICSR |= I2C_DATA_OUT_EN;
	I2C->IDSR = savle_addr;		
	I2C->ICSR |= I2C_MST_RX_MODE | I2C_GEN_START;	

	I2C_WAIT_PENDING();			

	return i2c_check_ack();
}

//!---------------------------------------------------------------
//!	@brief
//!		Set the master to the read mode
//!		Generates the restart condition and send the slave address + r
//!
//! @param
//!		slave address
//!
//! @return
//!		NAK(0) / ACK(1)
//!
//!---------------------------------------------------------------
uint8 i2c_read_restart(uint8 savle_addr)
{
	I2C->ICCR |= I2C_ACK_GEN_EN;	
	I2C->ICSR &= ~(0x3<<6);			
	I2C->ICSR |= I2C_DATA_OUT_EN;	
	I2C->IDSR = savle_addr;		
	I2C->ICSR |= I2C_MST_RX_MODE | I2C_GEN_START;

	if((I2C->ICCR & I2C_INT_PEND)) I2C_CLR_PENDING();
	I2C_CLR_PENDING();			

	return i2c_check_ack();
}

//!---------------------------------------------------------------
//!	@brief
//!		Generates the stop condition
//!
//! @param
//!		None
//!
//! @return
//!  	BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL i2c_gen_stop(void)
{
	uint8 temp;
	if(!(I2C->ICCR & I2C_INT_PEND)) return 0;	
	temp = I2C->ICSR & 0xc0;
	temp |= I2C_DATA_OUT_EN | I2C_GNE_STOP;	
	I2C->ICSR = temp;

	I2C_CLR_PENDING();

	while(I2C->ICSR & 0x20);

	return TRUE;
}

//!---------------------------------------------------------------
//!	@brief
//!		Write a data to the I2C port
//!
//! @param
//!		uint8 data
//!
//! @return
//!  	BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL i2c_master_write(uint8 data)
{
	if(!(I2C->ICCR & I2C_INT_PEND)) return 0;		
	if((I2C->ICSR&0xc0) != I2C_MST_TX_MODE) return 0;	
	I2C->IDSR = data;

	I2C_CLR_PENDING();		
	I2C_WAIT_PENDING();			

	return TRUE;
}

//!---------------------------------------------------------------
//!	@brief
//!		Read a data from the I2C port
//!
//! @param
//!		None
//!
//! @return
//!  	uint8 - read data
//!
//!---------------------------------------------------------------
uint8 i2c_master_read(void)
{
	if(!(I2C->ICCR & I2C_INT_PEND)) return 0;			
	if((I2C->ICSR&0xc0) != I2C_MST_RX_MODE) return 0;

	I2C_CLR_PENDING();
	I2C_WAIT_PENDING();								

	return (I2C->IDSR & 0xff);
}
