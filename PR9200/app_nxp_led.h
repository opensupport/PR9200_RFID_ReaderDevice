//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	app_nxp_led.h
//! @brief  Example of user application - Device Driver
//! 
//! $Id: app_nxp_led.h 1738 2012-08-31 07:56:46Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release


#ifndef APP_NXP_LED_H
#define APP_NXP_LED_H



//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "config.h"
#include "commontypes.h"
#include "event.h"


//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 
#define LED_TIME_1S			(1000)		// 1sec
#define LED_TIME_100MS		(100)		// 100ms
#define LED_TIME_200MS		(200)		// 200ms
#define LED_TIME_500MS		(500)		// 300ms
#define LED_TIME_900MS		(900)		// 900ms

#define LED_TIME_60S		(60000)		// 60sec
#define LED_TIME_30S		(30000)		// 30sec
#define LED_TIME_20S		(20000)		// 30sec
#define LED_TIME_10S		(10000)		// 10sec
#define LED_TIME_ZERO		(0)		



typedef enum
{
	EVENT_NXP_LED_ON = EVENT_CUSTOM_BASE,
	EVENT_NXP_LED_OFF,
	EVENT_NXP_LED_STOP,
} nxp_led_event_type;




typedef enum
{
	MODE0,
	MODE1,
	MODE2
}led_perfom_type;


typedef struct
{
	uint8 					cur_mode;
	uint8 					cur_val;
} nxp_led_ctxt_type;

typedef struct
{
	uint32 	on_cnt;
	uint32 	off_cnt;
	uint32	duration;
} nxp_led_perform_type;

//!-------------------------------------------------------------------                 
//! Macros
//!------------------------------------------------------------------- 



//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
void nxp_led_init(void);
void nxp_led_stop(void);
void nxp_led_off(void);
void nxp_led_perform(uint8 mode, uint8 val);
void nxp_led_set_time(uint8 mode, nxp_led_perform_type *param);

#endif


