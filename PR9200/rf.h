//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	rf.h
//! @brief	Implementation of Interface for RF registers
//! 
//! $Id: rf.h 1702 2012-08-02 02:05:39Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release
//! 2011/07/25	sjpark	modified for PR9200

#ifndef RF_H
#define RF_H


#include "config.h"
#include "commontypes.h"
#include "event.h"


// Reference clocks
#define 	RF_REF_CLK  		(SYS_ANLG_CLK)


#define		RF_BASE				(0x40010000)		// RF Register ver0.36 (20110208)	

#define		RR_TOP_ADD			(RF_BASE)
#define		RR_BOT_ADD			(RF_BASE + 0x0063)	// RF REGISTER : BOTTOM 

// RX PATH 
#define 	RR_BLOCK_ENABLE				(0x0000 + RF_BASE)  // Block enable signal collection ( RFLDO,IREF,ADC,LPF,RXM,LCA,PREAMP)
#define		RR_RX_GAIN_H_I				(0x0001 + RF_BASE)	// GAIN_IRXM<1:0>, GAIN_LNB_I<1:0>
#define		RR_RX_GAIN_L_I				(0x0002 + RF_BASE)	// GAIN_LPF1_I, GAIN_LPF2_I, GAIN_PGA_I
#define		RR_RX_GAIN_H_Q				(0x0003 + RF_BASE)	// GAIN_QRXM<1:0>, GAIN_LNB_Q<1:0>
#define		RR_RX_GAIN_L_Q				(0x0004 + RF_BASE)	// GAIN_LPF1_Q, GAIN_LPF2_Q, GAIN_PGA_Q
#define		RR_RX_DCOC_SLOW_FASTb		(0x0005 + RF_BASE)	// DCOC SLOW_FASTb selection, DCOC_SLOW_FASTb,MILLER_HPFEN,SEL_DCOC_MODE,,,,
#define		RR_RX_FILT_BW_I				(0x0006 + RF_BASE)	// adjust I path Filter BW ,  TRIM_RXLPF_CAP_I<5:0> 
#define		RR_RX_FILT_BW_Q				(0x0007 + RF_BASE)	// adjust I path Filter BW ,  TRIM_RXLPF_CAP_Q<5:0> 
#define		RR_RX_DCOC_BW				(0x0008 + RF_BASE)	// TRIM_DCOC_BW_Q<2:0>, TRIM_DCOC_BW_I<2:0>
#define		RR_RX_DCOC_CAP				(0x0009 + RF_BASE)	// TRIM_DCOC2_CAP_Q<1:0>,TRIM_DCOC2_CAP_I<1:0>,TRIM_DCOC1_CAP_Q<1:0>,TRIM_DCOC1_CAP_I<1:0> 
#define		RR_RX_FILT_AFT				(0x000A + RF_BASE)	// Rx filter cal : AFT , SEL_CAL_RXLPF, RXAFT_BWSEL,,,,   
#define		RR_RX_PREAMP_CTRL			(0x000B + RF_BASE)	// Rx preamp & LCA control : TRIM_V15,PREAMP_GCTRL, TRIM_VBN ,,,,   
#define 	RR_RX_MIX_DCOC				(0x000E + RF_BASE)  // RXM_DCOC_HP1, RXM_DCOC_BW
#define 	RR_RX_TP					(0x000F + RF_BASE)	// select Rx test point  
#define		RR_RX_SUB_ENABLE			(0x0010 + RF_BASE)  // Rx sub block enable : ENB_LCABUF, ENB_PWRD, ENB_PAMP_LP, ENB_RXAFT,ENB_DCOC,,, 

// TX PATH
#define 	RR_TX_SUB_ENABLE			(0x0021 + RF_BASE)  // Block sub block enable : ENB_LP_TX,ENB_HP_TX,ENB_TXLPF,ENB_TXDAC
#define 	RR_TX_DAC_OFFSET			(0x0022 + RF_BASE)  // TX DAC dc offset trimming
#define 	RR_TX_LPF_BW				(0x0023 + RF_BASE)  // TX LPF BW control : TRIM_XBW, TRIM_WIDE_BW_EN, TXLPF_CAP
#define 	RR_TX_LPF_GAIN				(0x0024 + RF_BASE)  // TX LPF Gain control : GAIN_TXLPF
#define 	RR_TX_LPF_AFT				(0x0025 + RF_BASE)  // TX LPF auto cal. : TXAFT_BWSEL
#define 	RR_TX_TP					(0x0026 + RF_BASE) // select Tx test point
#define 	RR_TX_PA_FB_RC_TRIM			(0x0027 + RF_BASE)	// PA feedback RC trim
#define 	RR_TX_PA_FB_RES_TRIM		(0x0028 + RF_BASE)	// PA feedback Resistor trim  
											
// VCO&PLL PATH									  
#define 	RR_PLL_SUB_ENABLE			(0x0016 + RF_BASE)  // PRESCALER_SEL,ENB_RDIV,ENB_LOB_TX,ENB_LOB_RX,ENB_AFC,ENB_PLL
#define 	RR_PLL_VCO_TRIM				(0x0017 + RF_BASE)	// VCO trim : TRIM_VCO<3:0>
#define		RR_PLL_AFC_VCOCTRL			(0x0018 + RF_BASE)	// AFC control signal : AFC_MUX_EN, PLL_OPEN, F_LOW,F_HIGH
#define 	RR_PLL_P_HIGH				(0x0019 + RF_BASE)	// PLL P counter value (High) : P<11:8>
#define 	RR_PLL_P_LOW				(0x001A + RF_BASE)	// PLL P counter value (low) : P<7:0>
#define 	RR_PLL_S					(0x001B + RF_BASE)	// PLL S counter value  : S<4:0>
#define 	RR_PLL_RDIV					(0x001C + RF_BASE)	// PLL RDIV(Reference Divider) : RDIV<6:0>
#define 	RR_PLL_LOOP_FILT			(0x001D + RF_BASE)	// PLL Loop Filter trimming value : LF_C3,LF_R2
#define 	RR_PLL_PFD_CTRL				(0x001E + RF_BASE)	// PLL PFD control : TRIM_PFD_ICON,TRIM_DELAY
#define 	RR_PLL_PHASE_SHIFT_I		(0x001F + RF_BASE)	// Phase shifter phase control : I_Phase_SEL, TRIM_I_Phase
#define 	RR_PLL_PHASE_SHIFT_Q		(0x0020 + RF_BASE)	// Phase shifter phase control : Q_Phase_SEL, TRIM_Q_Phase

#define 	RR_TEMP_EN					(0x0043 + RF_BASE)	
#define 	RR_TEMP_TRIM				(0x0045 + RF_BASE)	

#define		RR_GP_DAC_DATA2				(0x0046 + RF_BASE)
#define		RR_GP_DAC_DATA1         	(0x0047 + RF_BASE)
#define		RR_ADC_DATA					(0x0048 + RF_BASE)
#define		RR_ADC_END               	(0x0049 + RF_BASE)
#define		RR_TEMP_SENSE				(0x0049 + RF_BASE)

#define		RR_PLL_DSM_CTRL				(0x004B + RF_BASE) // DSM control signal : SEED,MASH_SEL,ENB_DITHER,ENB_DSM 
#define 	RR_PLL_F_HIGH				(0x004C + RF_BASE) // Fractional-N PLL F value control : F<23:16>
#define 	RR_PLL_F_MID				(0x004D + RF_BASE) // Fractional-N PLL F value control : F<15:8>
#define 	RR_PLL_F_LOW				(0x004E + RF_BASE) // Fractional-N PLL F value control : F<7:0>

#define 	RR_PLL_K_HIGH				(0x004F + RF_BASE) // Fractional-N PLL K value control : K<23:16>
#define 	RR_PLL_K_MID				(0x0050 + RF_BASE) // Fractional-N PLL K value control : F<15:8>
#define 	RR_PLL_K_LOW				(0x0051 + RF_BASE) // Fractional-N PLL K value control : F<7:0>
										 // AUX PART
#define 	RR_AUX_SUB_ENABLE			(0x0043 + RF_BASE)	// Aux. part enable : EN_TEMP, ENB_GPDAC, ENB_DACBUF, ENB_GPADC, ENB_GPADC_CLK,SEL_ATEST12 
#define		RR_AUX_TEMP_SENSE			(0x0049 + RF_BASE)	// Temp sensor output (READ ONLY) : TEMP_SENSE<3:0>

#define 	RR_AFC_TRIM					(0x0053 + RF_BASE)
#define 	RR_AFC_CTL_CNT				(0x0054 + RF_BASE)
#define   	RR_AFC_CNT_H_0				(0x0055 + RF_BASE)
#define		RR_AFC_CNT_L_0				(0x0056 + RF_BASE)
#define 	RR_AFC_CNT_H_1				(0x0057 + RF_BASE)
#define 	RR_AFC_CNT_L_1				(0x0058 + RF_BASE)

#define 	RR_RX_TP_EN					(0x000C + RF_BASE)
										  

#define 	VCO_REGION_MAX		(13)
#define		UNCAL				(0xFF)

#define		VCO_COARSE_BOT		(0)

#define		VCO_COARSE_CN1		(1)

#define		VCO_COARSE_EU		(2)

#define		VCO_COARSE_US1		(3)
#define		VCO_COARSE_US2		(4)
#define		VCO_COARSE_US3		(5)
#define		VCO_COARSE_US4		(6)
#define		VCO_COARSE_US5		(7)

#define		VCO_COARSE_KR1		(8)
#define		VCO_COARSE_KR2		(9)

#define		VCO_COARSE_CN2		(10)

#define		VCO_COARSE_JP		(11)

#define		VCO_COARSE_TOP		(12)

#define		PWR_CLASS0			(0)
#define		PWR_CLASS1			(1)				
#define		PWR_CLASS2			(2)
#define		PWR_CLASS3			(3)

#define		PWR_TABLE_SIZE		(50)
#define		TEMP_COMP_TABLE_SIZE	(8)


#define 	RX_LPF_DEFAULT		29
#define 	RX_LPF_TRIM			30

#define 	RX_DCOC1_DEFAULT	0x33
#define 	RX_DCOC1_TRIM		0x03

//!-------------------------------------------------------------------                 
//! Strunctures and enumerations
//!------------------------------------------------------------------- 

//!-------------------------------------------------------------------                 
//! Macros
//!------------------------------------------------------------------- 
#define RF_SET_LDO_ON()		EMI_SET_BIT(RR_BLOCK_ENABLE,BIT7)
#define RF_SET_LDO_OFF() 	EMI_CLR_BIT(RR_BLOCK_ENABLE,BIT7)

#define RF_SET_DAC_ON()		EMI_CLR_BIT(RR_TX_SUB_ENABLE,BIT0)
#define RF_SET_DAC_OFF()	EMI_SET_BIT(RR_TX_SUB_ENABLE,BIT0)

#if defined (__FEATURE_RF_TX_HP_MODE__)
#define RF_SET_TX_ON()		EMI_CLR_BIT(RR_TX_SUB_ENABLE, (BIT4|BIT3|BIT2|BIT1|BIT0))
#elif defined (__FEATURE_RF_TX_LP_MODE__)
#define RF_SET_TX_ON()		EMI_CLR_BIT(RR_TX_SUB_ENABLE, (BIT6|BIT1|BIT0))
#endif

#define RF_SET_TX_OFF()		EMI_SET_BIT(RR_TX_SUB_ENABLE, (BIT6|BIT4|BIT3|BIT2|BIT1|BIT0))

#define RF_SET_PLL_ON()		EMI_CLR_BIT(RR_PLL_SUB_ENABLE, BIT0)
#define RF_SET_PLL_OFF()	EMI_SET_BIT(RR_PLL_SUB_ENABLE, BIT0)

#define RF_AFC_ON() 		EMI_CLR_BIT(RR_PLL_SUB_ENABLE,BIT1)
#define RF_AFC_OFF()		EMI_SET_BIT(RR_PLL_SUB_ENABLE,BIT1)


#define	RF_TRIM_VCO(a)		EMI_WRITE(RR_PLL_VCO_TRIM, a  )

#define  RF_AFC_MUX_ON() 	EMI_CLR_BIT(RR_PLL_AFC_VCOCTRL,0x08)
#define  RF_AFC_MUX_OFF()	EMI_SET_BIT(RR_PLL_AFC_VCOCTRL,0x08)
								
#define	RF_AFC_SET_CNT(a)	EMI_WRITE(RR_AFC_CTL_CNT,a)
#define	RF_AFC_SET_TRIM(a)	EMI_WRITE(RR_AFC_TRIM, a )


#ifdef	__DEBUG_RF__
#define	debug_msg_str_rf(a,...)	debug_msg_str(a,##__VA_ARGS__)
#else
#define	debug_msg_str_rf(a,...)
#endif


extern int8 RFID_SYSTEM_TEMP;

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  

void rf_init(void);
void rf_init_register(void);
BOOL rf_vco_coarse_tune(void);
BOOL rf_set_ch(uint8 ch, uint8 ch_ext);
BOOL rf_set_ch_offset(uint8 offset);
void rf_set_next_ch(void);
BOOL rf_set_region(uint8 region);
void rf_set_tx_filt(uint8 tari);
void rf_set_rx_blf(uint16 blf, uint8 rx_mod);
void rf_rotate_phase(void);
void rf_get_temperature(EVENT e);
void rf_get_hopping_table(hal_rf_freq_hopping_table_type *tbl);
void rf_set_hopping_table(hal_rf_freq_hopping_table_type *tbl);
void rf_update_vco(void);
void rf_set_vco_trim(uint8 vco_band);
void rf_set_gain(uint8 gain_i, uint8 gain_q);

#ifdef __FEATURE_RF_TEMP_COMP__
uint8 rf_temp_comp(void);
#endif


#endif
