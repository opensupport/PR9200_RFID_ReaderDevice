//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	registry.c
//! @brief	Registry Module
//! 
//! $Id: registry.c 1724 2012-08-21 07:39:11Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release


//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "pr9200.h"
#include "rfidtypes.h"
#include "config.h"
#include "debug.h"
#include "modem.h"
#include "rf.h"
#include "rcp.h"
#include "iso180006c.h"
#include "registry.h"

#define REG_ITEM_NUM 	(sizeof(registry_map)/sizeof(registry_item_type))
#define REG_SET_ITEM(a)	{sizeof(a),(void*)&a}

reg_ver_type 		reg_ver = {{REG_INACTIVE},REG_VER}; // register version
reg_fw_data_type	reg_fw_data = {{REG_READONLY},FW_YEAR,FW_MONTH,FW_DAY};
reg_band_type 		reg_band = {{REG_INACTIVE},REGION_KOREA,1,0}; // RF_BAND, RF_CH, RF_CH_EXT
reg_tx_type				reg_tx = {{REG_INACTIVE},145}; // tx power : -5.5
reg_fhlbt_type		reg_fhlbt = {{REG_INACTIVE},200, 20, 10, -630, 1, 0, 0}; // tx_on_time, tx_off_time, sense_time, fh, lbt, cw
reg_anticol_type	reg_anticol = {{REG_INACTIVE},ANTI_COL_MODE0}; // mode
reg_modulation_type reg_modulation = {{REG_INACTIVE},0}; // mode
reg_prt_prm_type	reg_prt_prm = {{REG_INACTIVE},2}; // qry_q
reg_serial_no_type	reg_serial_no = {{REG_INACTIVE}, {65, 74, 84, 82, 48, 48, 48, 48, 48, 48, 48, 49}}; // AJTR0000001
reg_sys_off_type	reg_sys_off = {{REG_INACTIVE}, 300}; // Auto Poweroff Time

/*
reg_fh_tbl_type 	reg_fh_tbl = {{REG_INACTIVE}, 
									HAL_FREQ_HOPPING_TBL_MAX, 
									{47,19,20,23,46,16,45,48,32,36,
									 14,44,49,35, 4,38, 1,33, 2,12,
									 50, 3,31, 5, 7,34,15,10,37,25,
									 22,13,39,40, 9,43, 8,28,18,24,
									 27,21,42, 6,29,30,17,26,41,11}};

reg_fh_tbl_type 	reg_fh_tbl = {{REG_INACTIVE}, 
									HAL_FREQ_HOPPING_TBL_MAX, 
									{47,19,20,23,46,16,45,48,36,
									 14,44,49,35,38,33,12,
									 50, 7,34,15,10,37,25,
									 22,13,39,40, 9,43, 8,18,24,
									 27,21,42, 6,17,26,41,11}};

reg_fh_tbl_type 	reg_fh_tbl = {{REG_INACTIVE}, 
									HAL_FREQ_HOPPING_TBL_MAX, 
									{19,20,23,16,14,12,7,15,10,25,22,13,9,8,18,24,27,21,17,26,11}}; 
*/									
reg_fh_tbl_type 	reg_fh_tbl = {{REG_INACTIVE}, 
									16, 
									{20,23,14,5,25,22,8,28,24,27,21,29,30,17,26,11}}; // frequency hopping table

reg_sys_reset_type reg_sys_rst = {{REG_INACTIVE}, FALSE, NULL};		

registry_item_type registry_map[] = 
{ 
	REG_SET_ITEM(reg_ver),
	REG_SET_ITEM(reg_fw_data),
	REG_SET_ITEM(reg_band),
	REG_SET_ITEM(reg_tx),
	REG_SET_ITEM(reg_fhlbt),
	REG_SET_ITEM(reg_anticol),
	REG_SET_ITEM(reg_modulation),
	REG_SET_ITEM(reg_prt_prm),
	REG_SET_ITEM(reg_fh_tbl),
	REG_SET_ITEM(reg_sys_rst),	
	REG_SET_ITEM(reg_serial_no),
	REG_SET_ITEM(reg_sys_off),
};




uint32 reg_get_data_len(void)
{
	uint32 i, len = 0;

	for(i = 0; i < REG_ITEM_NUM; i++)
	{
		len += ((registry_map[i].size + 3) & 0xFFFFFFFC);
	}	
	
	return len;
}

//!---------------------------------------------------------------
//!	@brief
//!		Erase the registery
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
uint8 reg_erase(void)
{
	uint32 raddr, rdata;
	
	EFLSH_PAGE_ERASE(REG_ADDRESS);	

	for(raddr = REG_ADDRESS; raddr < REG_ADDRESS + FLASH_PAGE_SIZE; raddr +=4)
	{
		rdata = *(volatile uint32*)(raddr);		
				
		if(rdata != 0xffffffff)
		{
		 	return 0;
		}
	}
	
	return 1;
}


//!---------------------------------------------------------------
//!	@brief
//!		Write all the items to the reistry 
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------

uint8 reg_write(void)
{
	uint32 item_add, raddr, i, len;	
//	uint32 __packed *pckdData;  // to avoid alignment hardware fault	
	uint8 *pdata;
	uint32 data;
		
	reg_ver.active.val = REG_READONLY;
	reg_ver.ver = REG_VER;	
	
	raddr = REG_ADDRESS;	
	for(item_add = 0; item_add < REG_ITEM_NUM; item_add++)
	{
		len = ((registry_map[item_add].size + 3) >> 2); // 4 byte alignment

		//pckdData = (uint32*) registry_map[item_add].item;
		pdata = (uint8*) registry_map[item_add].item;

		for(i = 0; i < len; i++)
		{	
			//EFLSH_WRITE_WORD(raddr + (i * 4), *pckdData++);
			
			data = 	   pdata[0] | (pdata[1] << 8) 
					| (pdata[2] << 16) | (pdata[3] << 24); // for gcc compiler			
					
			EFLSH_WRITE_WORD(raddr + (i * 4), data);

			// verify
			if(data != *((uint32*)(raddr + (i * 4)))) 
				return 0;
				
			pdata += 4;
		}

		raddr += len * 4;
	}

	return 1;	
}



//!---------------------------------------------------------------
//!	@brief
//!		Load all the items from the registry to the registry map
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
uint8 reg_read(uint16 item_add, uint8 *ret_len, uint8 *ret_byte)
{
	uint8 i;	
	uint8 *psrc;

	if(item_add >= REG_ITEM_NUM)
		return 0;
	
	*ret_len = registry_map[item_add].size;

//	ret_byte = (uint8*) registry_map[item_add].item;

//	psrc =  (uint8*) (REG_ADDRESS + ((*ret_len + 3) & 0xFFFFFFFC));
	psrc = (uint8*) registry_map[item_add].item; 
			
	for(i = 0; i < *ret_len; i++)
	{		
		*ret_byte++ = *psrc++;
	}	

	return 1;
}


uint8 reg_load(void)
{
	uint32 item_add, len, i;
	uint8  *paddr, *pdest, *psrc;

	if(reg_get_data_len() >= FLASH_PAGE_SIZE)
	{
		debug_msg_str_reg("Size of registery is too long");
		while(1); 
	}

	paddr = (uint8*) REG_ADDRESS;

	if( (((reg_ver_type*)paddr)->active.val == REG_READONLY) && (((reg_ver_type*)paddr)->ver == reg_ver.ver) )
	{			
		for(item_add = 0; item_add < REG_ITEM_NUM; item_add++)
		{
			len = registry_map[item_add].size;

			if(((reg_active_type*)paddr)->val == REG_ACTIVE)
			{	
				pdest = (uint8*) registry_map[item_add].item;
				psrc =  (uint8*) paddr;
					
				for(i = 0; i < len; i++)
				{		
					*pdest++ = *psrc++;
				}
			}

			paddr += ((len + 3) & 0xFFFFFFFC);// 4 byte alignment
		}

	}
	else
	{	
		debug_msg_str_reg("Registry is empty.");
		return 0;
	}


	return 1;
}


