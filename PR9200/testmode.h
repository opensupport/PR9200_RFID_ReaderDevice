//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	testmode.h
//! @brief	Implementation of test service
//! 
//! $Id: testmode.h 1510 2012-03-20 08:58:35Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release




#ifndef TESTMODE_H
#define TESTMODE_H

#include "config.h"
#include "commontypes.h"


typedef struct
{
	uint8 test_mode;
	uint8 test_cmd;
} testmode_prm_type;


void test_parse_rcp_cmd(void);
void test_sel_repeat(void);
void test_qry_repeat(void);
void test_qryrep_repeat(void);
void test_modem_rx_fer(void);
void test_epc_set_sel_mask(void);
void test_epc_qry(void);
void test_epc_selqry(void);
void test_epc_qry_ack(void);
void test_epc_qry_qryrep(void);
void test_set_singletone(void);
void test_clr_singletone(void);

#endif
