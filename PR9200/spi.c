//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	spi.c
//! @brief	SPI Device Driver
//! 
//! $Id: spi.c 1755 2012-09-14 08:33:41Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi	initial release

//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include <STRING.H>
#include "config.h"
#include "PR9200.h"
#include "rcp.h"
#include "sio.h"
#include "spi.h"

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!------------------------------------------------------------------- 
extern sio_byte_buf_type 	sio_rx_buf;
extern sio_pkt_queue_type 	sio_tx_buf_queue;

//!-------------------------------------------------------------------                 
//! Definition Declaration                                                       
//!-------------------------------------------------------------------  
#define SPI_BUF_EMPTY 		(RCP_PKT_ENDMRK)
#define SPI_MODE_MOSI 		(0)
#define SPI_MODE_MISO 		(1)

#define SPI_SET_PORT()		(GPIO0->AFSEL |= 0xF0)
#define SPI_CLR_PORT()		(GPIO0->AFSEL &= ~(0xF0))

#if defined(__FEATURE_SPI_SLAVE_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 

uint8 spi_mode = SPI_MODE_MOSI;

//!---------------------------------------------------------------
//!	@brief
//!		flushes the spi rx fifo	
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void spi_slave_flush_rx_fifo(void)
{
	volatile uint8 dummy;
	uint8 i;
	
	for(i = 0; i < 8; i++)
	{
		if(!(SSP->SR & RNE)) break;

		dummy = SSP->DR; 		
	}  	
}

//!---------------------------------------------------------------
//!	@brief
//!		Dequeue a tx packet form the sio buffer
//!
//! @param
//!		None
//!
//! @return
//!		*sio_byte_buf_type
//!
//!---------------------------------------------------------------
sio_byte_buf_type* spi_slave_dequeue_tx_pkt(void)
{
	sio_byte_buf_type* ret;
	
	if(sio_tx_buf_queue.count == 0)
		return NULL;

	ret = &(sio_tx_buf_queue.pkt[sio_tx_buf_queue.tail]);

	sio_tx_buf_queue.tail++;
	sio_tx_buf_queue.tail %= SIO_PKT_QUEUE_MAX;
	sio_tx_buf_queue.count--;

	return ret;
}

 

//!---------------------------------------------------------------
//!	@brief
//!		Initialize the SPI slave
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void spi_slave_init(void)
{	
	SPI_SET_PORT();	
	SIO_INIT_IRQ();
	SIO_CLR_IRQ();

	SSP->CR0 = SSP_IDLE_HIGH_VALID_FALLING | DSS_8Bit;	// 8bit data
	SSP->CR1 = LSBF | SLV | SSE;	// LSB first, Slave Mode, SPI Enable
	
 	// SPI	
	spi_slave_flush_rx_fifo();	
	sio_byte_buf_flush(&sio_rx_buf);
	sio_pkt_queue_flush(&sio_tx_buf_queue);

	// IR 
	SSP->IMSC = RXIM | RTIM | RORIM;		 // rx interrupt set

	NVIC_EnableIRQ(SPI0_ERR_IRQn);
	NVIC_EnableIRQ(SPI_RX_IRQn);
}


//!---------------------------------------------------------------
//!	@brief
//!		Write a block of data to the SPI port
//!
//! @param
//!		*buf: buffer pointer
//!		len: buffer length
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void spi_slave_write(uint8 *buf, uint8 len)
{	
  	uint8 i;
 	volatile uint8 dummy;
	
	for ( i = 0; i < len; i++ )
  	{			
		while ( (SSP->SR & (TNF|BSY)) != TNF );
		SSP->DR = *buf;
		buf++;

		while ( (SSP->SR & (BSY|RNE)) != RNE );
	
		dummy = SSP->DR;

		if( dummy == RCP_PKT_PRAMBL)
		{
			sio_byte_buf_enqueue(&sio_rx_buf, dummy);
			SIO_CLR_IRQ();
			break;
		}
				
	 	while ( SSP->SR & BSY );	
	}

}




//!---------------------------------------------------------------
//!	@brief
//!		Process the routine to complete a SPI transaction	
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void spi_slave_process()
{
	uint8 inbyte;
	
//	__disable_irq();
	while(SSP->SR & RNE)	
	{	
		inbyte = SSP->DR;

		if ( sio_rx_buf.len  >= (SIO_BYTE_BUF_MAX - 1) ) 
		{
			sio_rx_buf.len = 0;
			sio_byte_buf_flush(&sio_rx_buf);
		}
			
		if ( (sio_rx_buf.len == 0 ) && (inbyte != RCP_PKT_PRAMBL)  ) 
		{
			SIO_CLR_IRQ();
			continue;
		}		

		if ( sio_rx_buf.len == 1)
		{
			if( inbyte == RCP_REQ_SIO_RSP )
			{
				sio_rx_buf.len = 0;				
				spi_mode = SPI_MODE_MISO;
				
				if(sio_tx_buf_queue.count > 0)
				{	
					sio_byte_buf_type* q;					
					q = sio_pkt_dequeue(&sio_tx_buf_queue);	
					if(q != NULL)
						spi_slave_write(&(q->data[0]), q->len);								
				}
				
				spi_mode = SPI_MODE_MOSI;
				SIO_CLR_IRQ();
				spi_slave_flush_rx_fifo();

/*
				if(sio_tx_buf_queue.count > 0)
				{	
					delay_irq_10us(30);
					SIO_SET_IRQ();
				}
*/				
				return;
			}			
			else if ( (inbyte != RCP_MSG_CMD) && (inbyte != RCP_MSG_TEST) && (inbyte != RCP_MSG_CAP) )
			{					
				sio_rx_buf.len = 0;
				spi_slave_flush_rx_fifo();				
			}
		}

		sio_byte_buf_enqueue(&sio_rx_buf, inbyte);

		if(sio_rx_buf.len  >= (sio_rx_buf.data[4] + RCP_HEADEND_CRC_LEN))
		{
			if(	sio_rx_buf.data[sio_rx_buf.data[4] + RCP_PRAMBL_LEN + RCP_HEADER_LEN] == RCP_PKT_ENDMRK) 
			{					 
				sio_post_received_data(&sio_rx_buf.data[0],sio_rx_buf.len);				
			}
			else
			{					
				sio_byte_buf_flush(&sio_rx_buf);				
				spi_slave_flush_rx_fifo();
			}
			
			return;
		}
		
	}
//	__enable_irq();


}


//!---------------------------------------------------------------
//!	@brief
//!		SPI Error Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void SPI0_ERR_IRQHandler(void)
{		
	spi_slave_process();
}


//!---------------------------------------------------------------
//!	@brief
//!		SPI Rx Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void SPI_RX_IRQHandler(void)
{
	spi_slave_process();
}

#endif

