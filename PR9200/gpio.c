//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	gpio.c
//! @brief	GPIO Device Driver
//! 
//! $Id: gpio.c 1606 2012-05-10 11:10:44Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release


//!-------------------------------------------------------------------
//! Include Files
//!------------------------------------------------------------------- 
#include <STRING.H>
#include "pr9200.h"
#include "sio.h"
#include "registry.h"
#include "rfidtypes.h"
#include "gpio.h"



//!-------------------------------------------------------------------
//! External Data References                                          
//!-------------------------------------------------------------------


//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    

/*

typedef struct
{
	hal_gpio_device_type dev;
	uint8 pol; // polarity	
} gpio_map_type;


const gpio_map_type	gpio_map_default[GPIO_PIN_CNT] = 
{
	{HAL_GPIO_DEV_RESERVE, HAL_GPIO_POL_NA}, 			//p0.0
	{HAL_GPIO_DEV_RESERVE, HAL_GPIO_POL_NA}, 			//p0.1
	{HAL_GPIO_DEV_EXT_WAKEUP, HAL_GPIO_POL_NORMAL}, 	//p0.2
	{HAL_GPIO_DEV_LED, HAL_GPIO_POL_NORMAL}, 			//p0.3
	{HAL_GPIO_DEV_BEEP, HAL_GPIO_POL_NORMAL}, 			//p0.4
	{HAL_GPIO_DEV_WAKE_UP_OUT, HAL_GPIO_POL_NORMAL},	//p0.5
	{HAL_GPIO_DEV_RESERVE, HAL_GPIO_POL_NA}, 			//p0.6
	{HAL_GPIO_DEV_RESERVE, HAL_GPIO_POL_NA}, 			//p0.7
	{HAL_GPIO_DEV_RESERVE, HAL_GPIO_POL_NA}, 			//p1.0
	{HAL_GPIO_DEV_EXT_LINK_ENABLE, HAL_GPIO_POL_NORMAL},//p1.1
	{HAL_GPIO_DEV_EXT_PA_ENABLE, HAL_GPIO_POL_NORMAL}, 	//p1.2
	{HAL_GPIO_DEV_RESERVE, HAL_GPIO_POL_NA}, 			//p1.3
	{HAL_GPIO_DEV_RESERVE, HAL_GPIO_POL_NA} 			//p1.4
};
*/

//gpio_map_type		gpio_map[GPIO_PIN_CNT];
//hal_gpio_out_type 	led_working_ctxt 			= {NULL, NULL, gpio_null, gpio_null};
//hal_gpio_out_type 	pa_en_ctxt 			= {NULL, NULL, gpio_null, gpio_null};
//hal_gpio_out_type 	ext_link_en_ctxt 	= {NULL, NULL, gpio_null, gpio_null};
//gpio_toggle_type 	beep_ctxt  		= {NULL, NULL, gpio_null};

typedef struct
{
	uint8 sense;
	uint8 edge;
	uint8 event;
}hal_gpio_interrupt_type;

const hal_gpio_interrupt_type GPIO_EXTINT_CONFIG[] =
{
	{GPIO_SENSE_LEVEL,	GPIO_EDGE_NONE,	GPIO_EVENT_HIGH},		// level high	
	{GPIO_SENSE_LEVEL,	GPIO_EDGE_NONE,	GPIO_EVENT_LOW},		// level low		
	{GPIO_SENSE_EDGE,	GPIO_EDGE_BOTH,	GPIO_EVENT_NONE},		// edge both		
	{GPIO_SENSE_EDGE,	GPIO_EDGE_SINGLE,	GPIO_EVENT_HIGH},	// edge single high		
	{GPIO_SENSE_EDGE,	GPIO_EDGE_SINGLE,	GPIO_EVENT_LOW}		// edge single low			
};


const uint32 GPIO_BASE[] = {GPIO0_BASE, GPIO1_BASE};


hal_gpio_handler_pf	ex1_cb = NULL;
hal_gpio_handler_pf	ex2_cb = NULL;
hal_gpio_handler_pf	ex5_cb = NULL;

//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------

//!---------------------------------------------------------------
//!	@brief
//!		External 1 Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
#ifndef __FEATURE_RF_TX_HP_MODE__	   
void External_1_IRQHandler(void)		
{
	if(ex1_cb != NULL)	ex1_cb();

	gpio_clear_interrupt(0, 2);
}
#endif

//!---------------------------------------------------------------
//!	@brief
//!		External 2 Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void External_2_IRQHandler(void)		
{
	if(ex2_cb != NULL)	ex2_cb();

	gpio_clear_interrupt(1, 0);
}



//!---------------------------------------------------------------
//!	@brief
//!		External 5 Interrupt Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void External_5_IRQHandler(void)		
{
	if(ex5_cb != NULL)	ex5_cb();

	gpio_clear_interrupt(1, 6);
}


//!---------------------------------------------------------------
//!	@brief
//!		Initialize GPIO profile
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_init(void)
{

}

//!---------------------------------------------------------------
//!	@brief
//!		Set the direction in GPIO port
//!
//! @param
//!		port: port number
//! 		bit: bit position
//!		dir: 0(Input), 1(Output)
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_set_dir( uint8 port, uint8 bit, uint8 dir)
{	
	if(dir)  	
	{
		((GPIO_Type *)GPIO_BASE[port])->DIR |= 1<<bit;
	}  
	else
	{
		((GPIO_Type *)GPIO_BASE[port])->DIR &= ~(1<<bit);
	}	
}

//!---------------------------------------------------------------
//!	@brief
//!		null function 
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_null(uint8 port, uint8 bit)
{
	/* do nothing */	
}

//!---------------------------------------------------------------
//!	@brief
//!		Set a bitvalue in a specific bit position in GPIO Port
//!
//! @param
//!		port: port number
//! 		bit: bit position
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_set_bit(void *io)
{		
	((GPIO_Type *)GPIO_BASE[((hal_gpio_out_type*)io)->id.port])->DATA |= 1<<((hal_gpio_out_type*)io)->id.bit;	
}

//!---------------------------------------------------------------
//!	@brief
//!		Clear a bitvalue in a specific bit position in GPIO Port
//!
//! @param
//!		port: port number
//! 		bit: bit position
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_clr_bit(void* io)
{
	((GPIO_Type *)GPIO_BASE[((hal_gpio_out_type*)io)->id.port])->DATA &= ~(1<<((hal_gpio_out_type*)io)->id.bit);	
}

//!---------------------------------------------------------------
//!	@brief
//!		Toggle a bitvalue in a specific bit position in GPIO Port	
//!
//! @param
//!		port: port number
//! 		bit: bit position
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_toggle_bit(void* io)
{
	((GPIO_Type *)GPIO_BASE[((hal_gpio_out_type*)io)->id.port])->DATA ^= 1<<(((hal_gpio_out_type*)io)->id.bit);
}

//!---------------------------------------------------------------
//!	@brief
//!		Get a bitvalue in a specific bit position in GPIO Port
//!
//! @param
//!		port: port number
//! 		bit: bit position
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
uint8 gpio_get_bit(void* io)
{	
	return ( (((GPIO_Type *)GPIO_BASE[((hal_gpio_in_type*)io)->id.port])->DATA) & (1<<(((hal_gpio_in_type*)io)->id.bit)) ? 1 : 0);
}

//!---------------------------------------------------------------
//!	@brief
//!		Configures interrupt  in a specific bit position in GPIO Port
//!
//! @param
//!		port: port number
//! 	bit: bit position
//!		sense: edge(0), level(1)
//!		edge: single(0), both(1)
//!		event: low level or falling edge(0), high level or risigne edge(1)
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_set_interrupt(uint8 port, uint8 bit, uint8 mode)
{

	if(GPIO_EXTINT_CONFIG[mode].sense)
		((GPIO_Type *)GPIO_BASE[port])->IS |= 1<<bit;
	else 
	{
		((GPIO_Type *)GPIO_BASE[port])->IS &= ~(1<<bit);

		if(GPIO_EXTINT_CONFIG[mode].edge)
			((GPIO_Type *)GPIO_BASE[port])->IBE |= 1<<bit;
		else 
			((GPIO_Type *)GPIO_BASE[port])->IBE &= ~(1<<bit);
	}	

	if(GPIO_EXTINT_CONFIG[mode].event)
		((GPIO_Type *)GPIO_BASE[port])->IEV |= 1<<bit;
	else 
		((GPIO_Type *)GPIO_BASE[port])->IEV &= ~(1<<bit);
	
}



//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		port: port number
//! 		bit: bit position
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_enable_interrupt(void* io)
{
	((GPIO_Type *)GPIO_BASE[((hal_gpio_ext_in_type*)io)->id.port])->IE |= 1<<((hal_gpio_ext_in_type*)io)->id.bit;	

	if((((hal_gpio_ext_in_type*)io)->id.port == 0) &&(((hal_gpio_ext_in_type*)io)->id.bit == 2))
	{
		NVIC_EnableIRQ(EXT1_IRQn);
	}
	else if((((hal_gpio_ext_in_type*)io)->id.port == 1) &&(((hal_gpio_ext_in_type*)io)->id.bit == 0))
	{
		NVIC_EnableIRQ(EXT2_IRQn);
	}
	else if((((hal_gpio_ext_in_type*)io)->id.port == 1) &&(((hal_gpio_ext_in_type*)io)->id.bit == 7))
	{
		NVIC_EnableIRQ(EXT5_IRQn);
	}
	
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		port: port number
//! 		bit: bit position
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_disable_interrupt(void* io)
{
	((GPIO_Type *)GPIO_BASE[((hal_gpio_ext_in_type*)io)->id.port])->IE &= ~(1<<((hal_gpio_ext_in_type*)io)->id.bit);	
	
	if((((hal_gpio_ext_in_type*)io)->id.port == 0) &&(((hal_gpio_ext_in_type*)io)->id.bit == 2))
	{
		NVIC_DisableIRQ(EXT1_IRQn);
	}
	else if((((hal_gpio_ext_in_type*)io)->id.port == 1) &&(((hal_gpio_ext_in_type*)io)->id.bit == 0))
	{
		NVIC_DisableIRQ(EXT2_IRQn);
	}
	else if((((hal_gpio_ext_in_type*)io)->id.port == 1) &&(((hal_gpio_ext_in_type*)io)->id.bit == 7))
	{
		NVIC_DisableIRQ(EXT5_IRQn);
	}
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		port: port number
//! 		bit: bit position
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_clear_interrupt( uint8 port, uint8 bit)
{
	((GPIO_Type *)GPIO_BASE[port])->IC |= 1<<bit;
}




//!---------------------------------------------------------------
//!	@brief
//!		Load the GPIO profile from the gpio map
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void gpio_register_dev(hal_gpio_id_type* dev)
{
	switch(dev->dev)
	{
	case HAL_GPIO_DEV_NONE:
		break;
	case HAL_GPIO_DEV_ANT_SWITCH0:
	case HAL_GPIO_DEV_ANT_SWITCH1:
	case HAL_GPIO_DEV_EXT_LINK_ENABLE:
	case HAL_GPIO_DEV_EXT_PA_ENABLE:
	case HAL_GPIO_DEV_LED:
	case HAL_GPIO_DEV_BEEP:
	case HAL_GPIO_DEV_OUT:
		{
			gpio_set_dir(((hal_gpio_out_type*)dev)->id.port,((hal_gpio_out_type*)dev)->id.bit,GPIO_DIR_OUT);
	
			if(((hal_gpio_out_type*)dev)->id.pol == HAL_GPIO_POL_NORMAL)
			{
				((hal_gpio_out_type*)dev)->on_cb = gpio_set_bit;
				((hal_gpio_out_type*)dev)->off_cb = gpio_clr_bit;
			}
			else
			{
				((hal_gpio_out_type*)dev)->on_cb = gpio_clr_bit;
				((hal_gpio_out_type*)dev)->off_cb = gpio_set_bit;
			}
			
			((hal_gpio_out_type*)dev)->toggle_cb = gpio_toggle_bit;
		}
		break;
	case HAL_GPIO_DEV_EXT_WAKEUP:
	case HAL_GPIO_DEV_PA_MODE_SEL:
	case HAL_GPIO_DEV_IN:
		{
			gpio_set_dir(((hal_gpio_in_type*)dev)->id.port,((hal_gpio_in_type*)dev)->id.bit,GPIO_DIR_IN);
			((hal_gpio_in_type*)dev)->in_cb = gpio_get_bit;
		}
		break;		
	case HAL_GPIO_DEV_EXT_INT:
		{
			gpio_set_dir(((hal_gpio_ext_in_type*)dev)->id.port,((hal_gpio_ext_in_type*)dev)->id.bit,GPIO_DIR_IN);
			gpio_set_interrupt(((hal_gpio_ext_in_type*)dev)->id.port, ((hal_gpio_ext_in_type*)dev)->id.bit, ((hal_gpio_ext_in_type*)dev)->mode);
			((hal_gpio_ext_in_type*)dev)->in_cb = gpio_get_bit;
			((hal_gpio_ext_in_type*)dev)->enint_cb = gpio_enable_interrupt;
			((hal_gpio_ext_in_type*)dev)->disint_cb = gpio_disable_interrupt;

			if(((hal_gpio_ext_in_type*)dev)->id.port == 0 && ((hal_gpio_ext_in_type*)dev)->id.bit == 2)	 // External1 (P0.3)
				ex1_cb = ((hal_gpio_ext_in_type*)dev)->handler_cb;
			else if(((hal_gpio_ext_in_type*)dev)->id.port == 1 && ((hal_gpio_ext_in_type*)dev)->id.bit == 0) // External2 (P1.0)			
				ex2_cb = ((hal_gpio_ext_in_type*)dev)->handler_cb;
			else if(((hal_gpio_ext_in_type*)dev)->id.port == 1 && ((hal_gpio_ext_in_type*)dev)->id.bit == 7) // External5 (P1.7)	
				ex5_cb = ((hal_gpio_ext_in_type*)dev)->handler_cb; 

		}
		break;	
	}
		
}




