//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	app_beep.h
//! @brief  Example of user application - Beep
//! 
//! $Id: app_beep.h 1606 2012-05-10 11:10:44Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release


#ifndef APP_BEEP_H
#define APP_BEEP_H



//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "config.h"
#include "commontypes.h"



//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 

#define BEEP_NOTE_A			341
#define BEEP_NOTE_AS		322	// A#
#define BEEP_NOTE_B     	304
#define BEEP_NOTE_C     	286
#define BEEP_NOTE_CS 		271	// C#
#define BEEP_NOTE_D     	255
#define BEEP_NOTE_DS 		241	// D#
#define BEEP_NOTE_E     	227
#define BEEP_NOTE_F     	215
#define BEEP_NOTE_FS 		203	// F#
#define BEEP_NOTE_G     	191
#define BEEP_NOTE_GS		181	// G#



#define BEEP_DUR_WHOLE		(120000)
#define BEEP_DUR_HALF		(BEEP_DUR_WHOLE/2)
#define BEEP_DUR_QUARTER	(BEEP_DUR_HALF/2)
#define BEEP_DUR_EIGHTH		(BEEP_DUR_QUARTER/2)
#define BEEP_DUR_SIXTEENTH	(BEEP_DUR_EIGHTH/2)




//!-------------------------------------------------------------------                 
//! Macros
//!------------------------------------------------------------------- 



//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
void beep_init(void);
void beep_perform(void);

#endif


