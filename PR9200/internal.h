//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	internal.h
//! @brief	Misc. library for internal use
//! 
//! $Id: internal.h 1626 2012-06-08 02:51:56Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/16	jsyi		initial release

#ifndef INTERNAL_H
#define INTERNAL_H

//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------
#include "commontypes.h"

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  
void internal_init(void);
void internal_rcp_parse(void *p);
void internal_modem_set_reg(void);
void internal_rf_set_reg(void);
void internal_modem_get_reg(void);
void internal_rf_get_reg(void);
void internal_set_tx_mod_rx_blf(void);
void internal_scan_rssi(void);

#if(0)
void internal_blockwrite_c_data(void); // Write Type C Data
void internal_blockerase_c_data(void); // Write Type C Data
#endif

#endif // INTERNAL_H

