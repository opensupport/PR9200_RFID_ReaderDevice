//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	event.c
//! @brief	simple event fifo
//! 
//! $Id: event.c 1760 2012-09-17 09:46:56Z sjpark $			
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/27	jsyi		initial release
//! 2012/05/24	jsyi		added event fifo

#include "pr9200.h"
#include "timer.h"
#include "event.h"
#include "hal.h"
#include "rfidtypes.h"
#include "protocol.h"
#include "rcp.h"
#include "registry.h"


//!-------------------------------------------------------------------
//! Definitions
//!-------------------------------------------------------------------
uint64 RFID_SYSTEM_TICK = 0;
#ifdef __DEBUG__
uint8 event_debug_count = 0;
/*
uint32 timer_s = 0;
uint32 timer_e = 0;
uint32 timer_d = 0;
*/
#endif

BOOL bt_connection_state;
#ifdef __READ_BTN_ENABLE__
BOOL read_btn_press;
extern rfid_inventory_prm_type rfid_inventory_prm;
extern int16 rfid_fsm_inventory_count;
#endif

hal_gpio_out_type	blue_led = {{HAL_GPIO_DEV_OUT, 0, 7, HAL_GPIO_POL_NORMAL},
		NULL,NULL,NULL};
hal_gpio_out_type	red_led = {{HAL_GPIO_DEV_OUT, 1, 2, HAL_GPIO_POL_NORMAL},
		NULL,NULL,NULL};
//hal_gpio_in_type bat_status_low_in = {{HAL_GPIO_DEV_IN, 1, 4, HAL_GPIO_POL_NORMAL}, NULL};

uint32 pwr_off_time;

#define	EVENT_TIMER_IRQ		TIMER0_IRQn	 
#define	EVENT_TIMER			TIMER0

event_timer_list_item_type	event_timer_list[EVENT_TIMER_LIST_ITEM_MAX] = {0,};
event_timer_list_item_type*	event_timer_list_head = NULL;

event_list_item_type		event_list[EVENT_NUM_MAX] = {0,};
event_list_item_type		*event_list_tail = NULL;
event_list_item_type		*event_list_head = NULL;



void event_timer_irq_handler(void);
void event_init(void)
{	
	// System Tick
	SysTick_Config(SYS_DIG_CLK / SYSTEM_TICK_FREQ);
#ifdef __READ_BTN_ENABLE__	
	read_btn_press = FALSE;
	//bt_connection_state = FALSE;
#endif
	pwr_off_time = reg_sys_off.time*1000;

	bt_connection_state = FALSE;
	hal_gpio_register(&blue_led);
	blue_led.on_cb(&blue_led);

//	hal_gpio_register(&bat_status_low_in);
	hal_gpio_register(&red_led);
	red_led.off_cb(&red_led);
}

//!---------------------------------------------------------------
//!	@brief
//!		SysTick Handler
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void SysTick_Handler(void)
{ 
    RFID_SYSTEM_TICK++;
	
	if( (NULL != event_timer_list_head)
		&& (event_timer_list_head->ms <= RFID_SYSTEM_TICK) )
	{
		event_timer_irq_handler();
	}
}


//!---------------------------------------------------------------
//!	@brief
//!		Adds a timer list item
//!
//! @param
//!		id: specify a software timer
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
static void event_timer_add(event_timer_list_item_type *new_node)
{
	if(NULL == event_timer_list_head)
	{
		event_timer_list_head = new_node;
	}
	else
	{
		event_timer_list_item_type*	pnode;

		pnode = event_timer_list_head;	

		while(NULL != pnode)
		{
			if(pnode->ms > new_node->ms)
			{
				if(NULL != pnode->prev) // mid
				{
					new_node->next = pnode;
					new_node->prev = pnode->prev;					
					pnode->prev->next = new_node;
					pnode->prev = new_node;
				}
				else // first
				{
					new_node->next =  pnode;
					pnode->prev = new_node;
					event_timer_list_head = new_node;
				}

				break;
			}
			else if((pnode->ms <= new_node->ms) && (NULL == pnode->next)) // last
			{
				pnode->next = new_node;
				new_node->prev = pnode;

				break;
			}

			pnode = pnode->next;
		}
	}

	
}


//!---------------------------------------------------------------
//!	@brief
//!		frees the node
//!
//! @param
//!		id: specify	a software timer
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
static void event_timer_free(event_timer_list_item_type *rnode)
{
	if( (NULL == rnode->prev) && (NULL == rnode->next) )
	{
		event_timer_list_head = NULL;
	}
	else if(rnode->prev == NULL) // first
	{		
		event_timer_list_head = rnode->next;
		event_timer_list_head->prev = NULL;
		rnode->next = NULL;
	}	

	else if(rnode->next == NULL) // last
	{
		rnode->prev->next = NULL;
		rnode->prev = NULL;
	}
	else // mid
	{
		rnode->prev->next	=  rnode->next;
		rnode->next->prev	=  rnode->prev;
		rnode->prev = NULL;
		rnode->next = NULL;
	}
}




//!---------------------------------------------------------------
//!	@brief
//!		Handler for timer event
//!
//! @param
//! 	None	
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void event_timer_irq_handler(void)
{		
	if(NULL != event_timer_list_head)
	{  	
		event_timer_list_item_type* pnode;

		event_timer_list_head->ms = 0;
		pnode = event_timer_list_head;

		while((NULL != pnode) && (0 == pnode->ms))
		{
			event_timer_free(pnode);
			
			if(NULL != pnode->e.handler)
			{
				event_post(pnode->e);
				pnode->e.handler = NULL;
			}

			pnode->state = EVENT_TIMER_STATE_STOPPED;
			pnode = event_timer_list_head;
		}
	}	
}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//! 	None	
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void event_timer_flush(event_handler h)
{
	if(NULL != event_timer_list_head)
	{  	
		event_timer_list_item_type* pnode;
		event_timer_list_item_type* pnext;
		pnode = event_timer_list_head;

		while((NULL != pnode))
		{			
			pnext = pnode->next;			
			if( h == pnode->e.handler )
			{
				event_timer_free(pnode);
				pnode->e.handler = NULL;
				pnode->ms = 0;
				pnode->state = EVENT_TIMER_STATE_STOPPED;	   
			}
			pnode = pnext;			
		}
	}
	
}


//!---------------------------------------------------------------
//!	@brief
//!		allocates a node
//!
//! @param
//! 	EVENT e:
//!		uint32 ms:
//!
//! @return
//!		a pointer to the allocated note
//!
//!---------------------------------------------------------------
static event_timer_list_item_type* event_timer_alloc_node
	(EVENT e, uint32 ms)
{
	uint8	i;
	event_timer_list_item_type* pnode;
	pnode = &event_timer_list[0];

	for(i=0; i<EVENT_TIMER_LIST_ITEM_MAX; i++)
	{			
		if(pnode->state == EVENT_TIMER_STATE_STOPPED)
		{				
			pnode->state = EVENT_TIMER_STATE_RUNNING;
			pnode->ms = (RFID_SYSTEM_TICK + ms);
			pnode->e = e;
			pnode->prev = NULL;
			pnode->next = NULL;
			return pnode;
		}
		pnode++;
	}		
	return NULL;
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		EVENT e	
//!		uint32 ms(delay time)
//!		
//! @return
//!		None
//!
//!---------------------------------------------------------------
void event_post_delayed(EVENT e, uint32 ms)
{
	event_timer_list_item_type* new_node;
	
	new_node = event_timer_alloc_node(e, ms);
	
	if(NULL == new_node) 
		return;

	event_timer_add(new_node);	
}

//!---------------------------------------------------------------
//!	@brief
//!		allocate a node
//!
//! @param
//! 	None
//!
//! @return
//!		pointer to allocated event	
//!
//!---------------------------------------------------------------
static event_list_item_type* event_alloc_node(void)	
{
	uint8 i;
	
	for(i = 0; i < EVENT_NUM_MAX; i++)
	{		
		if( (event_list[i].e.handler == NULL) && (event_list[i].e.param == NULL) )
		{
#ifdef __DEBUG__		
			event_debug_count++;
#endif			
			return &event_list[i];
		}
	}

	return NULL;
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//! 	EVENT e
//!
//! @return
//!		None	
//!
//!---------------------------------------------------------------
void event_post(EVENT e)
{		
	if(e.handler != NULL)
	{
		event_list_item_type *pnode;

		__disable_irq();		
		pnode = event_alloc_node();
				
		if(pnode != NULL)
		{	
			pnode->e = e;

			if( event_list_tail == NULL)
			{
				event_list_head = pnode;
				event_list_tail = pnode;
			}
			else
			{
				pnode->prev = event_list_tail;
				event_list_tail->next = pnode;
 				event_list_tail = pnode;
			}	
			
		}
		__enable_irq();
 
	}
	
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//! 	EVENT e
//!
//! @return
//!		None	
//!
//!---------------------------------------------------------------
void event_send(EVENT e)
{		
	if(e.handler != NULL)
	{
		event_list_item_type *pnode;
		
		__disable_irq();
		pnode = event_alloc_node();
		if(pnode != NULL)
		{
			pnode->e = e;

			if( event_list_head == NULL)
			{
				event_list_head = pnode;
				event_list_tail = pnode;
			}
			else				
			{
				event_list_head->prev = pnode;
				pnode->next = event_list_head;
				event_list_head = pnode;
			}
		}
		__enable_irq();
			
	}

	
}


//!---------------------------------------------------------------
//!	@brief
//!		frees the node
//!
//! @param
//!		id: specify	a software timer
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
static void event_fifo_free(event_list_item_type *rnode)
{
	if( (NULL == rnode->prev) && (NULL == rnode->next) )
	{
		event_list_head = NULL;
		event_list_tail = NULL;
	}
	else if(rnode->prev == NULL) // first
	{		
		event_list_head = rnode->next;
		event_list_head->prev = NULL;
		rnode->next = NULL;
	}
	else if(rnode->next == NULL) // last
	{
		event_list_tail = rnode->prev;
		event_list_tail->next = NULL;		
		rnode->prev = NULL;		
	}
	else // mid
	{
		rnode->prev->next	=  rnode->next;
		rnode->prev = NULL;
		rnode->next->prev	=  rnode->prev;
		rnode->next = NULL;
	}
}


//!---------------------------------------------------------------
//!	@brief
//!		frees the node
//!
//! @param
//!		event_handler h
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void event_fifo_flush(event_handler h)
{

	if(NULL != event_list_head)
	{  	
		event_list_item_type* pnode;
		event_list_item_type* pnext;
		pnode = event_list_head;

		while((NULL != pnode))
		{			
			pnext = pnode->next;			
			if( h == pnode->e.handler )
			{
				event_fifo_free(pnode);
				pnode->e.handler = NULL;
				pnode->e.param = NULL;
			}
			pnode = pnext; 
		}
	}
  	
}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//! 	event_handler h
//!
//! @return
//!		None	
//!
//!---------------------------------------------------------------
void event_flush(event_handler h)
{
	__disable_irq();
	// remove node
	event_fifo_flush(h);
	// remove node from event timer
	event_timer_flush(h);
	__enable_irq();	
}

uint8 EVENT_IDLE_POSTED = FALSE;
										
void event_idle_expired(EVENT e)
{
	event_timer_flush(e.handler);
#ifndef __ENGINEER_MODE__
	hal_power_off();
#endif
	EVENT_IDLE_POSTED = FALSE;
}

void event_clear()
{
	event_timer_flush(event_idle_expired);
 	EVENT_IDLE_POSTED = FALSE;
}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//! 	None	
//!
//! @return
//!		None	
//!
//!---------------------------------------------------------------
void event_idle(void)
{	
	if(!EVENT_IDLE_POSTED)
	{
		EVENT e;
		e.param = EVENT_ENTRY;
		e.handler = event_idle_expired;
		if(bt_connection_state)
		{
			event_post_delayed(e, pwr_off_time);
		}
		else
		{
			event_post_delayed(e, 300000);
		}
		EVENT_IDLE_POSTED = TRUE;
	}

	//__WFI();
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//! 	None	
//!
//! @return
//!		None		
//!
//!---------------------------------------------------------------
void event_dispatch(void)
{		
	uint32 i;

	i = 0;
	while(1) 
	{
		if(event_list_head != NULL)
		{				
			event_list_item_type *pnode;			
			// remove first node
			__disable_irq();
			pnode = event_list_head;
			event_fifo_free(pnode);		
			__enable_irq();
			pnode->e.handler(pnode->e);
			// clear node 
			pnode->e.handler = NULL;
			pnode->e.param = NULL;
			event_clear();		
#ifdef __DEBUG__
			event_debug_count--;
#endif						
		}
		else
		{
			event_idle();
		}
#ifdef __READ_BTN_ENABLE__
		if(read_btn_press && bt_connection_state)
		{
			
			rfid_inventory_prm_type param;
			SYS_S ret;	
			read_btn_press = FALSE;
			rfid_fsm_inventory_count = 0;
			//debug_msg_str_2("read");

			param.cmd_sel = CMD_INVENTORY_MULTIPLE_CYCLETIMENUM;
			param.mtnu = 1;
			param.mtime = 0;
			param.count = 100;

			ret = protocol_perform_inventory(&param);
			
			if (ret==SYS_S_OK)
			{
				rcp_write_msg_ack(RCP_MSG_RSP,RCP_CMD_STRT_AUTO_READ_EX);
			}
		}
		if(!bt_connection_state)
		{
			read_btn_press = FALSE;
		}
#endif

		if(!bt_connection_state)
		{
			if(i<80000)
				i++;
			else
			{
				blue_led.toggle_cb(&blue_led);	
				i = 0;
			}
		}
		else
			blue_led.on_cb(&blue_led);

// 		if(bat_status_low_in.in_cb(&bat_status_low_in))
// 			red_led.on_cb(&red_led);
// 		else
// 			red_led.off_cb(&red_led);
	}
	
}



