//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	iap_spi.h
//! @brief	SPI Device Driver for iap
//! 
//! $Id: iap_spi.h 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/08/31	sjpark	initial release

#ifndef IAP_SPI_H
#define IAP_SPI_H


void iap_spi_slave_init( void );
void iap_spi_send_frame(uint8 cmd, uint8 *data, uint8 len, uint8 h_crc, uint8 l_crc);
uint8 iap_spi_get_status(void);
int32 iap_spi_get_char(void);

#endif



