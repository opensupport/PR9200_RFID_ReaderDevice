//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	rf.c
//! @brief	Interface for RF registers
//! 
//! $Id: rf.c 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release
//! 2011/07/25	sjpark	modified for PR9200


//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "PR9200.h"
#include "registry.h"
#include "event.h"
#include "rf.h"
#include "rfidfsm.h"
#include "rcp.h"

//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 

// China band 1
#define CN1_CH_NUM_S		1
#define CN1_CH_NUM_E		20
#define CN1_CH_REF_F		-255317
#define CN1_CH_SPACE_F		13653
#define CN1_CH_PPL_P		11
#define CN1_CH_PLL_S		0

// EU band
#define EU_CH_NUM_S			1
#define EU_CH_NUM_E			15
#define EU_CH_REF_F			60075
#define EU_CH_SPACE_F		10923
#define EU_CH_PPL_P			11
#define EU_CH_PLL_S			1

// US band
#define US_CH_NUM_S			1
#define US_CH_NUM_E			50
#define US_CH_REF_F			-1029460
#define US_CH_SPACE_F		27307
#define US_CH_PPL_P			12
#define US_CH_PLL_S			0

// Korea band
#define KR_CH_NUM_S			1
#define KR_CH_NUM_E			32
#define KR_CH_REF_F			802815
#define KR_CH_SPACE_F		10923
#define KR_CH_PPL_P			11
#define KR_CH_PLL_S			3

// China band 2
#define CN2_CH_NUM_S		1
#define CN2_CH_NUM_E		20
#define CN2_CH_REF_F		995327
#define CN2_CH_SPACE_F		13654
#define CN2_CH_PPL_P		11
#define CN2_CH_PLL_S		3

// Japan band
#define JP_CH_NUM_S			1
#define JP_CH_NUM_E			38
#define JP_CH_REF_F			742741
#define JP_CH_SPACE_F		10923
#define JP_CH_PPL_P			11
#define JP_CH_PLL_S			3

// Temperature comp.
#define TEMP_COMP_TABLE_SIZ 	8

//!-------------------------------------------------------------------                 
//! Definitions - AFC section
//!------------------------------------------------------------------- 
#define VCO_TRM_MAX			(15)
#define VCO_BAND_MIN_BW		(150)
#define VCO_RANGE_MIN		(7000)

#define BOT_FREQ_S			(8190)
#define BOT_FREQ_E			(8210)

#define CN1_FREQ_S			(8390)
#define CN1_FREQ_E			(8460)

#define EU_FREQ_S			(8640)
#define EU_FREQ_E			(8690)

#define US1_FREQ_S			(9010)
#define US1_FREQ_E			(9090)

#define US2_FREQ_S			(9060)
#define US2_FREQ_E			(9140)

#define US3_FREQ_S			(9110)
#define US3_FREQ_E			(9190)

#define US4_FREQ_S			(9160)
#define US4_FREQ_E			(9240)

#define US5_FREQ_S			(9210)
#define US5_FREQ_E			(9290)

#define KR1_FREQ_S			(9160)
#define KR1_FREQ_E			(9220)

#define KR2_FREQ_S			(9190)
#define KR2_FREQ_E			(9240)

#define CN2_FREQ_S			(9190)
#define CN2_FREQ_E			(9260)

#define JP_FREQ_S			(9190)
#define JP_FREQ_E			(9230)

#define TOP_FREQ_S			(9790)
#define TOP_FREQ_E			(9810)

#if (SYS_OSC_CLK 	== 	19200000UL)
#define CORE_AFC_ADJ_COEF_L		(9)
#define CORE_AFC_ADJ_COEF_H		(7)
#elif (SYS_OSC_CLK 	== 	38400000UL)
#define CORE_AFC_ADJ_COEF_L		(6)
#define CORE_AFC_ADJ_COEF_H		(7)
#endif

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------  

extern	rfid_fsm_ctxt_type		rfid_fsm_ctxt;
extern  rssi_scan_type 			rssi_scan_c;

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------  
int32 	rf_pll_ref_f; 
int16 	rf_pll_space_f; 
int32	rf_pll_cur_f;

uint8	rf_cur_vco_band; 
uint8 	rf_vco_trim[VCO_REGION_MAX];	

uint16 	rf_gain_table[] = {0xDC00, 0xDD00, 0xDE00, 0xDF00, 0xDF40, 0xDF80, 0xDFC0, 0xDFC8, 0xDFD0, 0xDFD8, 0xDFE0};

#ifdef __FEATURE_RF_TEMP_COMP__
BOOL 	rf_temp_comp_enable = FALSE;
int8 	rf_tgain_temp_offset_idx 				= 0;
int8 	temp_comp_table[TEMP_COMP_TABLE_SIZE] 	
	= {-3, 	-2, -1, 0, 	1, 	2,	 3,   4}; 
#endif

// Random channel table for FHSS
/*uint8 rnd_ch_table[HAL_FREQ_HOPPING_TBL_MAX] 
		= {	47,19,20,23,46,
			16,45,48,32,36,
			14,44,49,35, 4,
			38, 1,33, 2,12,
			50, 3,31, 5, 7,
			34,15,10,37,25,
			22,13,39,40, 9,
			43, 8,28,18,24,
			27,21,42, 6,29,
			30,17,26,41,11};
uint8 rnd_ch_table_size = sizeof(rnd_ch_table);
*/
int8 RFID_SYSTEM_TEMP = 0;

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  


//!---------------------------------------------------------------
//!	@brief
//!		Initialize RF blocks
//!
//! @param
//! 	none
//!
//! @return
//! 	none
//!---------------------------------------------------------------
void rf_init()
{
	//EVENT e;
	//uint8 i;
	rf_init_register();

	// Initialize DAC
	RF_SET_DAC_ON();
	modem_set_dac_ramp_up(1);
	modem_set_dac_ramp_dn(1);
	RF_SET_DAC_OFF();

	// VCO COARSE TUNE CAL
	rf_vco_coarse_tune();	

	RF_SET_PLL_OFF();
	RF_SET_LDO_OFF();

	// BAND, CHANNEL ==> HAL
	/*
	rf_set_region(reg_band.region);
	if(!rf_set_ch(reg_band.cur_ch, reg_band.cur_ch_ext))
	{
		rf_set_ch(rssi_scan_c.start_ch, 0);
	}
	*/
	
	// temperature monitoring
	//e.handler = rf_get_temperature;
	//e.param = EVENT_ENTRY;
	//rf_get_temperature(e);
	
	
#ifdef __FEATURE_RF_TEMP_COMP__
	rf_temp_comp();
#endif

}



//!---------------------------------------------------------------
//!	@brief
//!		Initialize the Rf register
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rf_init_register()
{
//SLRM900IS_KR_NXP_20120907_wkoh.rf
#ifdef __FEATURE_RF_TX_HP_MODE__				//USE TCXO
	EMI_WRITE(RF_BASE + 0x00, 0x07);
	EMI_WRITE(RF_BASE + 0x01, 0xDF);
	EMI_WRITE(RF_BASE + 0x02, 0x81);
	EMI_WRITE(RF_BASE + 0x03, 0xDF);
	EMI_WRITE(RF_BASE + 0x04, 0x81);
	EMI_WRITE(RF_BASE + 0x05, 0xC6);
	EMI_WRITE(RF_BASE + 0x06, 0x94);
	EMI_WRITE(RF_BASE + 0x07, 0x94);
	EMI_WRITE(RF_BASE + 0x08, 0xAA);
	EMI_WRITE(RF_BASE + 0x09, 0xFF);
	EMI_WRITE(RF_BASE + 0x0A, 0x3F);
	EMI_WRITE(RF_BASE + 0x0B, 0x47);
	EMI_WRITE(RF_BASE + 0x0C, 0x40);
	EMI_WRITE(RF_BASE + 0x0D, 0xAA);
	EMI_WRITE(RF_BASE + 0x0E, 0xBD);
	EMI_WRITE(RF_BASE + 0x0F, 0x0F);
	EMI_WRITE(RF_BASE + 0x10, 0xF0);
	EMI_WRITE(RF_BASE + 0x11, 0x7F);
	EMI_WRITE(RF_BASE + 0x12, 0xDD);
	EMI_WRITE(RF_BASE + 0x13, 0xDD);
	EMI_WRITE(RF_BASE + 0x14, 0xD8);
	EMI_WRITE(RF_BASE + 0x15, 0xFF);
	EMI_WRITE(RF_BASE + 0x16, 0xF2);
	EMI_WRITE(RF_BASE + 0x17, 0x0A);
	EMI_WRITE(RF_BASE + 0x18, 0xF8);
	EMI_WRITE(RF_BASE + 0x19, 0x00);
	EMI_WRITE(RF_BASE + 0x1A, 0x0C);
	EMI_WRITE(RF_BASE + 0x1B, 0x00);
	EMI_WRITE(RF_BASE + 0x1C, 0x83);
	EMI_WRITE(RF_BASE + 0x1D, 0x99);
	EMI_WRITE(RF_BASE + 0x1E, 0xE4);
	EMI_WRITE(RF_BASE + 0x1F, 0xFF);
	EMI_WRITE(RF_BASE + 0x20, 0xFF);
	EMI_WRITE(RF_BASE + 0x21, 0xC0);
	EMI_WRITE(RF_BASE + 0x22, 0x00);
	EMI_WRITE(RF_BASE + 0x23, 0x1F);
	EMI_WRITE(RF_BASE + 0x24, 0xBD);
	EMI_WRITE(RF_BASE + 0x25, 0x90);
	EMI_WRITE(RF_BASE + 0x26, 0x27);
	EMI_WRITE(RF_BASE + 0x27, 0xFF);
	EMI_WRITE(RF_BASE + 0x28, 0x8B);
	EMI_WRITE(RF_BASE + 0x29, 0xFF);
	EMI_WRITE(RF_BASE + 0x2A, 0xFF);
	EMI_WRITE(RF_BASE + 0x2B, 0xDD);
	EMI_WRITE(RF_BASE + 0x2C, 0xDC);
	EMI_WRITE(RF_BASE + 0x2D, 0xFD);
	EMI_WRITE(RF_BASE + 0x2E, 0xDD);
	EMI_WRITE(RF_BASE + 0x2F, 0xDD);
	EMI_WRITE(RF_BASE + 0x30, 0xDD);
	EMI_WRITE(RF_BASE + 0x31, 0xDD);
	EMI_WRITE(RF_BASE + 0x32, 0xDD);
	EMI_WRITE(RF_BASE + 0x33, 0xDD);
	EMI_WRITE(RF_BASE + 0x34, 0xAC);
	EMI_WRITE(RF_BASE + 0x35, 0xD4);
	EMI_WRITE(RF_BASE + 0x36, 0xCC);
	EMI_WRITE(RF_BASE + 0x37, 0xDD);
	EMI_WRITE(RF_BASE + 0x38, 0xD9);
	EMI_WRITE(RF_BASE + 0x39, 0xDD);
	EMI_WRITE(RF_BASE + 0x3A, 0xDD);
	EMI_WRITE(RF_BASE + 0x3B, 0xCC);
	EMI_WRITE(RF_BASE + 0x3C, 0x9D);
	EMI_WRITE(RF_BASE + 0x3D, 0xEA);
	EMI_WRITE(RF_BASE + 0x3E, 0xF1);
	EMI_WRITE(RF_BASE + 0x3F, 0xFD);
	EMI_WRITE(RF_BASE + 0x40, 0xDD);
	EMI_WRITE(RF_BASE + 0x41, 0xFF);
	EMI_WRITE(RF_BASE + 0x42, 0xFF);
	EMI_WRITE(RF_BASE + 0x43, 0x7F);
	EMI_WRITE(RF_BASE + 0x44, 0xFF);
	EMI_WRITE(RF_BASE + 0x45, 0x5F);
	EMI_WRITE(RF_BASE + 0x46, 0xFF);
	EMI_WRITE(RF_BASE + 0x47, 0xFF);
	EMI_WRITE(RF_BASE + 0x48, 0x00);
	EMI_WRITE(RF_BASE + 0x49, 0x0F);
	EMI_WRITE(RF_BASE + 0x4A, 0xF0);
	EMI_WRITE(RF_BASE + 0x4B, 0xF0);
	EMI_WRITE(RF_BASE + 0x4C, 0x00);
	EMI_WRITE(RF_BASE + 0x4D, 0x40);
	EMI_WRITE(RF_BASE + 0x4E, 0x00);
	EMI_WRITE(RF_BASE + 0x4F, 0x0F);
	EMI_WRITE(RF_BASE + 0x50, 0xFF);
	EMI_WRITE(RF_BASE + 0x51, 0xFF);
	EMI_WRITE(RF_BASE + 0x52, 0xFE);
	EMI_WRITE(RF_BASE + 0x53, 0x0C);
	EMI_WRITE(RF_BASE + 0x54, 0xC0);
	EMI_WRITE(RF_BASE + 0x55, 0x04);
	EMI_WRITE(RF_BASE + 0x56, 0x87);
	EMI_WRITE(RF_BASE + 0x57, 0x04);
	EMI_WRITE(RF_BASE + 0x58, 0xB3);
	EMI_WRITE(RF_BASE + 0x59, 0x04);
	EMI_WRITE(RF_BASE + 0x5A, 0x9F);
	EMI_WRITE(RF_BASE + 0x5B, 0x04);
	EMI_WRITE(RF_BASE + 0x5C, 0xCE);
	EMI_WRITE(RF_BASE + 0x5D, 0x04);
	EMI_WRITE(RF_BASE + 0x5E, 0xB7);
	EMI_WRITE(RF_BASE + 0x5F, 0x04);
	EMI_WRITE(RF_BASE + 0x60, 0xEA);
	EMI_WRITE(RF_BASE + 0x61, 0xFF);
	EMI_WRITE(RF_BASE + 0x62, 0xFF);
	EMI_WRITE(RF_BASE + 0x63, 0xFF);
#elif defined(__FEATURE_RF_TX_LP_MODE__)
	EMI_WRITE(RF_BASE + 0x00, 0x07);
	EMI_WRITE(RF_BASE + 0x01, 0xDE);
	EMI_WRITE(RF_BASE + 0x02, 0x04);
	EMI_WRITE(RF_BASE + 0x03, 0xDE);
	EMI_WRITE(RF_BASE + 0x04, 0x04);
	EMI_WRITE(RF_BASE + 0x05, 0xC6);
	EMI_WRITE(RF_BASE + 0x06, 0x94);
	EMI_WRITE(RF_BASE + 0x07, 0x94);
	EMI_WRITE(RF_BASE + 0x08, 0xAA);
	EMI_WRITE(RF_BASE + 0x09, 0xFF);
	EMI_WRITE(RF_BASE + 0x0A, 0x3F);
	EMI_WRITE(RF_BASE + 0x0B, 0x47);
	EMI_WRITE(RF_BASE + 0x0C, 0x40);
	EMI_WRITE(RF_BASE + 0x0D, 0xAA);
	EMI_WRITE(RF_BASE + 0x0E, 0x1D);
	EMI_WRITE(RF_BASE + 0x0F, 0x0F);
	EMI_WRITE(RF_BASE + 0x10, 0xF0);
	EMI_WRITE(RF_BASE + 0x11, 0x7F);
	EMI_WRITE(RF_BASE + 0x12, 0xDD);
	EMI_WRITE(RF_BASE + 0x13, 0xDD);
	EMI_WRITE(RF_BASE + 0x14, 0xD8);
	EMI_WRITE(RF_BASE + 0x15, 0xFF);
	EMI_WRITE(RF_BASE + 0x16, 0xEA);
	EMI_WRITE(RF_BASE + 0x17, 0x00);
	EMI_WRITE(RF_BASE + 0x18, 0xF8);
	EMI_WRITE(RF_BASE + 0x19, 0x00);
	EMI_WRITE(RF_BASE + 0x1A, 0x0B);
	EMI_WRITE(RF_BASE + 0x1B, 0x03);
	EMI_WRITE(RF_BASE + 0x1C, 0x83);
	EMI_WRITE(RF_BASE + 0x1D, 0x19);
	EMI_WRITE(RF_BASE + 0x1E, 0xE4);
	EMI_WRITE(RF_BASE + 0x1F, 0xFF);
	EMI_WRITE(RF_BASE + 0x20, 0xFF);
	EMI_WRITE(RF_BASE + 0x21, 0x9D);
	EMI_WRITE(RF_BASE + 0x22, 0x00);
	EMI_WRITE(RF_BASE + 0x23, 0x19);
	EMI_WRITE(RF_BASE + 0x24, 0xBD);
	EMI_WRITE(RF_BASE + 0x25, 0x90);
	EMI_WRITE(RF_BASE + 0x26, 0x27);
	EMI_WRITE(RF_BASE + 0x27, 0xFF);
	EMI_WRITE(RF_BASE + 0x28, 0x8E);
	EMI_WRITE(RF_BASE + 0x29, 0xFF);
	EMI_WRITE(RF_BASE + 0x2A, 0xFF);
	EMI_WRITE(RF_BASE + 0x2B, 0xDD);
	EMI_WRITE(RF_BASE + 0x2C, 0xDC);
	EMI_WRITE(RF_BASE + 0x2D, 0xDD);
	EMI_WRITE(RF_BASE + 0x2E, 0xDD);
	EMI_WRITE(RF_BASE + 0x2F, 0xDD);
	EMI_WRITE(RF_BASE + 0x30, 0xDD);
	EMI_WRITE(RF_BASE + 0x31, 0xDD);
	EMI_WRITE(RF_BASE + 0x32, 0xDD);
	EMI_WRITE(RF_BASE + 0x33, 0xDD);
	EMI_WRITE(RF_BASE + 0x34, 0xDD);
	EMI_WRITE(RF_BASE + 0x35, 0xD8);
	EMI_WRITE(RF_BASE + 0x36, 0xDD);
	EMI_WRITE(RF_BASE + 0x37, 0xDD);
	EMI_WRITE(RF_BASE + 0x38, 0xDD);
	EMI_WRITE(RF_BASE + 0x39, 0xDD);
	EMI_WRITE(RF_BASE + 0x3A, 0xDD);
	EMI_WRITE(RF_BASE + 0x3B, 0xDD);
	EMI_WRITE(RF_BASE + 0x3C, 0xDD);
	EMI_WRITE(RF_BASE + 0x3D, 0xFD);
	EMI_WRITE(RF_BASE + 0x3E, 0xF1);
	EMI_WRITE(RF_BASE + 0x3F, 0xFD);
	EMI_WRITE(RF_BASE + 0x40, 0x9D);
	EMI_WRITE(RF_BASE + 0x41, 0xCF);
	EMI_WRITE(RF_BASE + 0x42, 0xFF);
	EMI_WRITE(RF_BASE + 0x43, 0x7F);
	EMI_WRITE(RF_BASE + 0x44, 0xFF);
	EMI_WRITE(RF_BASE + 0x45, 0xFF);
	EMI_WRITE(RF_BASE + 0x46, 0xFF);
	EMI_WRITE(RF_BASE + 0x47, 0xFF);
	EMI_WRITE(RF_BASE + 0x48, 0x00);
	EMI_WRITE(RF_BASE + 0x49, 0x0F);
	EMI_WRITE(RF_BASE + 0x4A, 0xF0);
	EMI_WRITE(RF_BASE + 0x4B, 0xF0);
	EMI_WRITE(RF_BASE + 0x4C, 0x00);
	EMI_WRITE(RF_BASE + 0x4D, 0x24);
	EMI_WRITE(RF_BASE + 0x4E, 0x22);
	EMI_WRITE(RF_BASE + 0x4F, 0x00);
	EMI_WRITE(RF_BASE + 0x50, 0x25);
	EMI_WRITE(RF_BASE + 0x51, 0x80);
	EMI_WRITE(RF_BASE + 0x52, 0xFE);
	EMI_WRITE(RF_BASE + 0x53, 0x0C);
	EMI_WRITE(RF_BASE + 0x54, 0xC0);
	EMI_WRITE(RF_BASE + 0x55, 0x00);
	EMI_WRITE(RF_BASE + 0x56, 0x00);
	EMI_WRITE(RF_BASE + 0x57, 0x00);
	EMI_WRITE(RF_BASE + 0x58, 0x00);
	EMI_WRITE(RF_BASE + 0x59, 0x00);
	EMI_WRITE(RF_BASE + 0x5A, 0x00);
	EMI_WRITE(RF_BASE + 0x5B, 0x00);
	EMI_WRITE(RF_BASE + 0x5C, 0x00);
	EMI_WRITE(RF_BASE + 0x5D, 0x00);
	EMI_WRITE(RF_BASE + 0x5E, 0x00);
	EMI_WRITE(RF_BASE + 0x5F, 0x00);
	EMI_WRITE(RF_BASE + 0x60, 0x00);
	EMI_WRITE(RF_BASE + 0x61, 0xFF);
	EMI_WRITE(RF_BASE + 0x62, 0xFF);
	EMI_WRITE(RF_BASE + 0x63, 0xFF);


		
#endif
}


//!---------------------------------------------------------------
//!	@brief
//!		Fractional-N PLL F value control
//!
//! @param
//!		0 to 16777216 (2^24)
//!
//! @return
//!  	none
//!---------------------------------------------------------------
#define RF_SET_PLL_F(val) 	EMI_WRITE(RR_PLL_F_HIGH, (uint8) ((((int32)val) >> 16) & 0xFF)  	);\
							EMI_WRITE(RR_PLL_F_MID, (uint8) ((((int32)val) >> 8 ) & 0xFF)  	);\
							EMI_WRITE(RR_PLL_F_LOW, (uint8) (((int32)val) & 0xFF) 		 	)

//!---------------------------------------------------------------
//!	@brief
//!		Fractional-N PLL K value control
//!
//! @param
//!		0 to 16777216 (2^24)
//!
//! @return
//!  	none
//!---------------------------------------------------------------
#define RF_SET_PLL_K(val) 	EMI_WRITE(RR_PLL_K_HIGH, (uint8) ((((int32)val) >> 16) & 0xFF)  	);\
							EMI_WRITE(RR_PLL_K_MID, (uint8) ((((int32)val) >> 8 ) & 0xFF)  	);\
							EMI_WRITE(RR_PLL_K_LOW, (uint8) (((int32)val) & 0xFF) 		 	)

#define RF_SET_PLL_P(val) 	EMI_WRITE(RR_PLL_P_HIGH, (uint8) ((((int32)val) >> 8) & 0x0F)  	);\
							EMI_WRITE(RR_PLL_P_LOW, (uint8) (((int32)val) & 0xFF) 		 	)


#define RF_SET_VCO_TRM(a)	EMI_WRITE(RR_PLL_VCO_TRIM, (a) )


void rf_set_vco_trim(uint8 vco_band)
{
	rf_cur_vco_band = vco_band;
	RF_SET_VCO_TRM(rf_vco_trim[rf_cur_vco_band]);
}

void rf_update_vco(void)
{
	rf_set_vco_trim(rf_cur_vco_band);
}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		
//!
//! @return
//!  	None
//!---------------------------------------------------------------
void rf_get_hopping_table(hal_rf_freq_hopping_table_type *tbl)
{
	tbl->size = reg_fh_tbl.size;
	tbl->table_ptr = &reg_fh_tbl.item[0];
}

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		
//!
//! @return
//!  	None
//!---------------------------------------------------------------
void rf_set_hopping_table(hal_rf_freq_hopping_table_type *tbl)
{
	uint8 i;

	reg_fh_tbl.size = tbl->size;

	for(i=0; i<reg_fh_tbl.size ; i++)
	{
		reg_fh_tbl.item[i] = tbl->table_ptr[i];
	}
}

//!---------------------------------------------------------------
//!	@brief
//!		Set the current region
//!
//! @param
//!		region 
//!
//! @return
//!  	Sucess(TRUE) /fail (FALSE)
//!---------------------------------------------------------------

BOOL rf_set_region(uint8 region)
{
	reg_band.region = region;

	switch(reg_band.region)
	{
		case REGION_CHINA1:
			rssi_scan_c.start_ch = CN1_CH_NUM_S;
			rssi_scan_c.stop_ch = CN1_CH_NUM_E;
   
			EMI_WRITE(RR_PLL_S, CN1_CH_PLL_S);
			RF_SET_PLL_K(1048575);			
			
			RF_SET_PLL_P(CN1_CH_PPL_P);
			
			rf_set_vco_trim(VCO_COARSE_CN1);

			reg_band.cur_ch_ext = 0;

			rf_pll_ref_f = CN1_CH_REF_F;
			rf_pll_space_f = CN1_CH_SPACE_F;  
			break;

		case REGION_EUROPE:
			rssi_scan_c.start_ch = EU_CH_NUM_S;
			rssi_scan_c.stop_ch = EU_CH_NUM_E;

			EMI_WRITE(RR_PLL_S, EU_CH_PLL_S);
			RF_SET_PLL_K(1048575);

			RF_SET_PLL_P( EU_CH_PPL_P);

			rf_set_vco_trim(VCO_COARSE_EU);

			
			reg_band.cur_ch_ext = 0;
			
			rf_pll_ref_f = EU_CH_REF_F;
			rf_pll_space_f = EU_CH_SPACE_F;
			break;
		case REGION_USA:
			rssi_scan_c.start_ch = US_CH_NUM_S;
			rssi_scan_c.stop_ch = US_CH_NUM_E;	

			EMI_WRITE(RR_PLL_S, US_CH_PLL_S);
			RF_SET_PLL_K(1048575);

			RF_SET_PLL_P(US_CH_PPL_P);

			rf_set_vco_trim(VCO_COARSE_US1);
			
			reg_band.cur_ch_ext = 0;

			rf_pll_ref_f = US_CH_REF_F;
			rf_pll_space_f = US_CH_SPACE_F;
			break;

		case REGION_KOREA:
			rssi_scan_c.start_ch = KR_CH_NUM_S;
			rssi_scan_c.stop_ch = KR_CH_NUM_E;	

			EMI_WRITE(RR_PLL_S, KR_CH_PLL_S);
			RF_SET_PLL_K(1048575);

			RF_SET_PLL_P(KR_CH_PPL_P);

			rf_set_vco_trim(VCO_COARSE_KR1);

			reg_band.cur_ch_ext = 0;

			rf_pll_ref_f = KR_CH_REF_F;
			rf_pll_space_f = KR_CH_SPACE_F;
			break;

   		case REGION_CHINA2:
			rssi_scan_c.start_ch = CN2_CH_NUM_S;
			rssi_scan_c.stop_ch = CN2_CH_NUM_E;
   
			EMI_WRITE(RR_PLL_S, CN2_CH_PLL_S);

			RF_SET_PLL_K(1048575);
			
			RF_SET_PLL_P(CN2_CH_PPL_P);

			rf_set_vco_trim(VCO_COARSE_CN2);

			reg_band.cur_ch_ext = 0;

			rf_pll_ref_f = CN2_CH_REF_F;
			rf_pll_space_f = CN2_CH_SPACE_F;			
			break;


		case REGION_JAPAN:
			rssi_scan_c.start_ch = JP_CH_NUM_S;
			rssi_scan_c.stop_ch = JP_CH_NUM_E;

			EMI_WRITE(RR_PLL_S, JP_CH_PLL_S);

			RF_SET_PLL_K(1048575);

			RF_SET_PLL_P(JP_CH_PPL_P);
			rf_set_vco_trim(VCO_COARSE_JP);			

			rf_pll_ref_f = JP_CH_REF_F;
			rf_pll_space_f = JP_CH_SPACE_F;
					 
			break;	   

	}

	return TRUE;
	
}

void rf_set_fb_trim(uint8 ch)
{
	if(reg_band.region != REGION_USA)
		return;

	if(ch < 31)
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (8<<4) | 3);
	else if(ch < 33)
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (7<<4) | 3);
	else if(ch < 34)
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (6<<4) | 3);
	else if(ch < 35)
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (5<<4) | 3);
	else if(ch < 36)
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (4<<4) | 3);
	else if(ch < 40)
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (3<<4) | 3);
	else if(ch < 42)
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (2<<4) | 3);
	else if(ch < 49)
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (1<<4) | 3);
	else if(ch < 50)
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (2<<4) | 3);
	else
		EMI_WRITE(RR_TX_PA_FB_RES_TRIM, (3<<4) | 3);

}

//!---------------------------------------------------------------
//!	@brief
//!		Set RF channel
//!
//! @param
//!		Channel number
//!
//! @return
//!  	Sucess(TRUE) /fail (FALSE)
//!---------------------------------------------------------------
BOOL rf_set_ch( uint8 ch, uint8 ch_ext)
{					
	uint8 rf_ch_offset = 0;
	BOOL find_ch = TRUE;
		
	if( (ch < rssi_scan_c.start_ch) || (ch > rssi_scan_c.stop_ch) ) return FALSE;

	if(!find_ch) return FALSE;
	
	switch(	reg_band.region)
	{
	case REGION_USA:
	{
		if(	ch < 39)
		{
			EMI_WRITE(RR_PLL_S, US_CH_PLL_S);
			RF_SET_PLL_P( US_CH_PPL_P);
			rf_pll_ref_f = 	US_CH_REF_F;
			rf_ch_offset = 0;
		}
		else
		{
			EMI_WRITE(RR_PLL_S, 1);
			RF_SET_PLL_P( US_CH_PPL_P );		   
			rf_pll_ref_f = -1040383;
			rf_ch_offset = 38;
		}						
										

		if( ch <= 11 )
		{
			rf_set_vco_trim(VCO_COARSE_US1);
		}
		else if( ch <= 21 )
		{
			rf_set_vco_trim(VCO_COARSE_US2);
		}
		else if( ch <= 33 )
		{
			rf_set_vco_trim(VCO_COARSE_US3);
		}
		else if( ch <= 41 )
		{
			rf_set_vco_trim(VCO_COARSE_US4);
		}
		else
		{
			rf_set_vco_trim(VCO_COARSE_US5);
		}
	}
	break;

	case REGION_KOREA:
	{
		if(	ch < 24)
		{
			EMI_WRITE(RR_PLL_S, KR_CH_PLL_S);
			RF_SET_PLL_P( KR_CH_PPL_P);
			rf_pll_ref_f = 	KR_CH_REF_F;
			rf_ch_offset = 0;
		}
		else
		{
			EMI_WRITE(RR_PLL_S, 0);
			RF_SET_PLL_P( KR_CH_PPL_P + 1);		   
			rf_pll_ref_f = 5461;
			rf_ch_offset = 23;
		}								  

		if( ch <= 18 )
		{
			rf_set_vco_trim(VCO_COARSE_KR1);
		}
		else
		{
			rf_set_vco_trim(VCO_COARSE_KR2);
		}
	}
	break;

	case REGION_CHINA1:
	{
		if( ch < 20 )
		{
			EMI_WRITE( RR_PLL_S, CN1_CH_PLL_S);
			RF_SET_PLL_P(CN1_CH_PPL_P);
			rf_pll_ref_f = CN1_CH_REF_F;
			rf_ch_offset = 0;
		}
		else
		{
			EMI_WRITE( RR_PLL_S, CN1_CH_PLL_S + 1);
			RF_SET_PLL_P(CN1_CH_PPL_P);
			rf_pll_ref_f = -1044479;
			rf_ch_offset = 19;
		}
	}
	break;

	case REGION_CHINA2:
	{
		if( ch < 5 )
		{
			EMI_WRITE( RR_PLL_S, CN2_CH_PLL_S);
			RF_SET_PLL_P(CN2_CH_PPL_P);
			rf_pll_ref_f = CN2_CH_REF_F;
			rf_ch_offset = 0;
		}
		else
		{
			EMI_WRITE( RR_PLL_S, 0);
			RF_SET_PLL_P(CN2_CH_PPL_P + 1);
			rf_pll_ref_f = 1365;
			rf_ch_offset = 4;
		}
	}
	break;

	case REGION_JAPAN:
	{
		if(ch < 29)
		{
			EMI_WRITE( RR_PLL_S, JP_CH_PLL_S);
			RF_SET_PLL_P(JP_CH_PPL_P);
			rf_pll_ref_f = JP_CH_REF_F;
			rf_ch_offset = 0;
		}
		else
		{
			EMI_WRITE( RR_PLL_S, 0);
			RF_SET_PLL_P(JP_CH_PPL_P + 1);
			rf_pll_ref_f = 0;
			rf_ch_offset = 28;
		}
		rf_pll_cur_f = rf_pll_cur_f + (int32) ch_ext;
	}
	break;


	}

	rf_pll_cur_f = (uint32) ch - rssi_scan_c.start_ch - rf_ch_offset;
	rf_pll_cur_f = rf_pll_cur_f * ((int32) rf_pll_space_f);
	rf_pll_cur_f = rf_pll_cur_f + (int32) rf_pll_ref_f;

	RF_SET_PLL_F(rf_pll_cur_f);
	
	reg_band.cur_ch = ch;
	reg_band.cur_ch_ext= ch_ext;

	delay_10us(10); // 100 us,  wait for lock time

	debug_msg_str("ch = %02d", ch);

	rf_set_fb_trim(ch);
	
	return TRUE;
}


//!---------------------------------------------------------------
//!	@brief
//!		Set RF channel offset	
//!
//! @param
//!		offset 
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
BOOL rf_set_ch_offset(uint8 offset)
{	

	int32 pll_f_offset;
	
	pll_f_offset = rf_pll_cur_f + (int32) offset;
	RF_SET_PLL_F(pll_f_offset);

	return TRUE;		
}



//!---------------------------------------------------------------
//!	@brief
//!		Set tx filter bandwidth
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rf_set_tx_filt(uint8 tari)
{
   	uint8 	temp;

	temp = EMI_READ(RR_TX_LPF_BW) & 0xE0;
	
	switch(tari)
	{
#ifdef __FEATURE_TARI_25__
		case TARI_25U:
			EMI_WRITE(RR_TX_LPF_BW, temp | 0x19);
			break;
#endif			
#ifdef __FEATURE_TARI_12P5__				
		case TARI_12_5U:
			EMI_WRITE(RR_TX_LPF_BW, temp | 0x0A);
			break;
#endif
#ifdef __FEATURE_TARI_6P25__
		case TARI_6_25U:			
			EMI_WRITE(RR_TX_LPF_BW, temp | 0x02);
			break;
#endif			
	}	
	
}

//!---------------------------------------------------------------
//!	@brief
//!		Set rx filter bandwidth 
//!
//! @param
//!		blf: BLF_40 / BLF_80 / BLF_160 / BLF_200 / BLF_213 /
//!			  BLF_250 / BLF_300 / BLF_320 / BLF_640
//!		rx_mod: FM_0, MI2, MI4, MI8
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rf_set_rx_blf(uint16 blf, uint8 rx_mod)
{
	uint8 temp = EMI_READ( RR_RX_MIX_DCOC) & 0xF8 ;
	switch(blf)
	{
#ifdef __FEATURE_BLF_040__
		case BLF_40: 	
			if(rx_mod != MI8)
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0xEC);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0xEC);
			}
			else
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0xFC);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0xFC);
			}
															
			EMI_WRITE( RR_RX_DCOC_BW, 0xEE);	
			EMI_WRITE(RR_RX_MIX_DCOC, temp | 0x07);
			break;		
#endif
#ifdef __FEATURE_BLF_080__
		case BLF_80:   	
			if(rx_mod != MI8)
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0xAC);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0xAC);
			}
			else
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0xC0);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0xC0);
			}
			EMI_WRITE( RR_RX_DCOC_BW, 0xCC);	
			EMI_WRITE(RR_RX_MIX_DCOC, temp | 0x06);  				
			break;
#endif
#ifdef __FEATURE_BLF_160__
		case BLF_160: 
#ifdef __FEATURE_RX_MOD_M8__
			if(rx_mod != MI8)
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x6C);
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x6C);
			}
#endif			
#if defined (__FEATURE_RX_MOD_FM0__) || defined (__FEATURE_RX_MOD_M2__) || defined (__FEATURE_RX_MOD_M4__)
			else
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x94);
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x94);
			}
#endif			
			EMI_WRITE( RR_RX_DCOC_BW, 0xAA);	
			EMI_WRITE(RR_RX_MIX_DCOC, temp | 0x05);  	
			break;
#endif
#ifdef __FEATURE_BLF_200__
		case BLF_200: 
			if(rx_mod != MI8)
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x50);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x50);
			}
			else
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x74);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x74);
			}		
			EMI_WRITE( RR_RX_DCOC_BW, 0x88);	
			EMI_WRITE(RR_RX_MIX_DCOC, temp | 0x04);   
			break;
#endif	
#ifdef __FEATURE_BLF_213__
		case BLF_213_3:
			EMI_WRITE(RR_RX_FILT_BW_I, 0x48);			
			EMI_WRITE(RR_RX_FILT_BW_Q, 0x48);
			EMI_WRITE( RR_RX_DCOC_BW,  0x88);	
			EMI_WRITE(RR_RX_MIX_DCOC, temp | 0x04);   
			break;
#endif
#ifdef __FEATURE_BLF_250__
		case BLF_250:	
			if(rx_mod != MI8)
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x3C);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x3C);
			}
			else
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x58);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x58);
			}	
			EMI_WRITE( RR_RX_DCOC_BW, 0x66);	
			EMI_WRITE(RR_RX_MIX_DCOC, temp | 0x03);   			
			break;
#endif
#ifdef __FEATURE_BLF_300__
		case BLF_300:
			if(rx_mod != MI8)
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x2C);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x2C);
			}
			else
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x44);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x44);
			}	
			EMI_WRITE( RR_RX_DCOC_BW, 0x44);	
			EMI_WRITE(RR_RX_MIX_DCOC, temp | 0x02);   		
			break;
#endif
#ifdef __FEATURE_BLF_320__
		case BLF_320:
			if(rx_mod != MI8)
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x24);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x24);
			}
			else
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x40);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x40);
			}	
			EMI_WRITE( RR_RX_DCOC_BW, 0x22);	
			EMI_WRITE(RR_RX_MIX_DCOC, temp | 0x01);   
			break;
#endif
#ifdef __FEATURE_BLF_640__
		case BLF_640:
			if(rx_mod != MI8)
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x08);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x08);
			}
			else
			{
				EMI_WRITE(RR_RX_FILT_BW_I, 0x10);			
				EMI_WRITE(RR_RX_FILT_BW_Q, 0x10);
			}	
			EMI_WRITE( RR_RX_DCOC_BW, 0x00);	
			EMI_WRITE(RR_RX_MIX_DCOC, temp | 0x00); 		  	
			break;
#endif	
		default:
			return;
	}

//	rf_set_rx_aft(blf);
}	   


//!---------------------------------------------------------------
//!	@brief
//!		Set tx AFT value	
//!
//! @param
//!  	tari: tari type
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rf_set_tx_aft(uint8	tari)
{
	uint8 temp;

	EMI_SET_BIT( RR_TX_LPF_AFT, BIT6 );			 //SEL_CAL_TGXLPF(AFT Val)

	temp = EMI_READ(RR_TX_LPF_AFT) & 0xE0;
	
	 
	if(tari > TARI_12_5U)				   // tari : 25us
	{
		EMI_WRITE(RR_TX_LPF_AFT, temp | 0x10);
	}
	else 
#ifdef __FEATURE_TARI_6P25__			
	if(tari > TARI_6_25U)		   		// tari : 12.5us
#endif	
	{
		EMI_WRITE(RR_TX_LPF_AFT, temp | 0x08);
	}
#ifdef __FEATURE_TARI_6P25__	
	else									// tari : 6.25us
	{
		EMI_WRITE(RR_TX_LPF_AFT, temp | 0x04);
	}	
#endif
	EMI_SET_BIT( RR_TX_SUB_ENABLE, BIT5);	// Enable TX_AFT  
	delay_10us(20); // 200 us
	EMI_CLR_BIT( RR_TX_SUB_ENABLE, BIT5);	// Disable TX_AFT 			   	

}


//!---------------------------------------------------------------
//!	@brief
//!		Set rx AFT value
//!
//! @param
//!		blf: BLF_40 / BLF_80 / BLF_160 / BLF_200 / BLF_213 /
//!			  BLF_250 / BLF_300 / BLF_320 / BLF_640
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rf_set_rx_aft(uint16 blf)
{
	switch(blf)
	{
#ifdef __FEATURE_BLF_040__		
		case BLF_40: 	
			EMI_WRITE( RR_RX_FILT_BW_I	, 	0x80  );	 // Narrow Band
			EMI_WRITE( RR_RX_FILT_BW_Q  , 	0x80  );	 // Narrow Band
			EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x3E);
			break;		
#endif
#ifdef __FEATURE_BLF_080__
		case BLF_80:   	
			EMI_WRITE( RR_RX_FILT_BW_I	, 	0x80  );	// Narrow Band
			EMI_WRITE( RR_RX_FILT_BW_Q  , 	0x80  );	// Narrow Band
			EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x20);
			break;
#endif
#ifdef __FEATURE_BLF_160__
		case BLF_160: 			
			EMI_WRITE( RR_RX_FILT_BW_I	, 	0x00  );	// Wide Band
			EMI_WRITE( RR_RX_FILT_BW_Q  , 	0x00  );  	// Wide Band
			EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x10);
			break;
#endif
#ifdef __FEATURE_BLF_200__
		case BLF_200: 
			EMI_WRITE( RR_RX_FILT_BW_I	, 	0x00  );    // Wide Band
			EMI_WRITE( RR_RX_FILT_BW_Q  , 	0x00  );	// Wide Band  	
			EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x0C);
			break;
#endif
#ifdef __FEATURE_BLF_213__
		case BLF_213_3:
			EMI_WRITE( RR_RX_FILT_BW_I	, 	0x00  );	// Wide Band
			EMI_WRITE( RR_RX_FILT_BW_Q  , 	0x00  );  	// Wide Band
			EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x0C);
			break;
#endif
#ifdef __FEATURE_BLF_250__
		case BLF_250:	
			EMI_WRITE( RR_RX_FILT_BW_I	, 	0x00  );	// Wide Band
			EMI_WRITE( RR_RX_FILT_BW_Q  , 	0x00  );  	// Wide Band
			EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x0A);
			break;
#endif
#ifdef __FEATURE_BLF_300__
		case BLF_300:
			EMI_WRITE( RR_RX_FILT_BW_I	, 	0x00  );	// Wide Band
			EMI_WRITE( RR_RX_FILT_BW_Q  , 	0x00  );  	// Wide Band
			EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x08);	
			break;
#endif
#ifdef __FEATURE_BLF_320__
		case BLF_320:
 			EMI_WRITE( RR_RX_FILT_BW_I	, 	0x00  );	// Wide Band
			EMI_WRITE( RR_RX_FILT_BW_Q  , 	0x00  );  	// Wide Band
			EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x08);
			break;
#endif
#ifdef __FEATURE_BLF_640__
		case BLF_640:
			EMI_WRITE( RR_RX_FILT_BW_I	, 	0x00  );	// Wide Band
			EMI_WRITE( RR_RX_FILT_BW_Q  , 	0x00  );  	// Wide Band 
			EMI_WRITE(RR_RX_FILT_AFT, BIT7 | 0x04);	
			break;
#endif	
		default:
			return;
	}

	EMI_SET_BIT( RR_RX_SUB_ENABLE, BIT4);	// Enable RX_AFT		   
	delay_10us(20); //200 us
	EMI_CLR_BIT( RR_RX_SUB_ENABLE, BIT4);	// Disable TX_AFT
}



//!---------------------------------------------------------------
//!	@brief
//!		Set next RF channel
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void rf_set_next_ch()
{
 	static int8 rch = 0;

	do
	{
		rch++;
		rch = rch % reg_fh_tbl.size;

		if( (rch < rssi_scan_c.start_ch) || (rch > rssi_scan_c.stop_ch) )
			continue;
		
	} while(!rf_set_ch( reg_fh_tbl.item[rch], reg_band.cur_ch_ext ));

}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void set_vco_tune(uint8 trim)
{	
	delay_10us(5);  // 50us
	RF_AFC_SET_TRIM(trim);		
	RF_AFC_ON();
	delay_10us(10); // 100us
	RF_AFC_OFF();
}

 
//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
BOOL rf_vco_coarse_tune()
{	
	uint16 vco_afc_l[VCO_TRM_MAX] = {0,}; // Operating Region
	uint16 vco_afc_h[VCO_TRM_MAX] = {0,}; // Operating Region
	uint16 region_freq_l[VCO_REGION_MAX] = { BOT_FREQ_S, CN1_FREQ_S, EU_FREQ_S, US1_FREQ_S, US2_FREQ_S, US3_FREQ_S, US4_FREQ_S, US5_FREQ_S, KR1_FREQ_S, KR2_FREQ_S, CN2_FREQ_S, JP_FREQ_S, TOP_FREQ_S};
	uint16 region_freq_h[VCO_REGION_MAX] = { BOT_FREQ_E, CN1_FREQ_E, EU_FREQ_E, US1_FREQ_E, US2_FREQ_E, US3_FREQ_E, US4_FREQ_E, US5_FREQ_E, KR1_FREQ_E, KR2_FREQ_E, CN2_FREQ_E, JP_FREQ_E, TOP_FREQ_E};
	uint16 temp = 0;
	uint8 i;
	uint8 j;
	uint8 t_s;	
	uint8 t_e;		
	uint8 vco_max = 0;
	uint8 ret = TRUE;
	
	uint16 region_freq_curr_cent;
	uint16 vco_afc_curr_cent;
	uint16 vco_afc_next_cent;

//	RF_SET_TX_OFF();
//	RF_SET_LDO_ON();
//	RF_SET_PLL_ON();
	delay_10us(4); // 40us

	for(i = 0; i < VCO_REGION_MAX; i++)
	{
		rf_vco_trim[i] = 0xFF;
	}
	/////////////////////////////////
						  
	RF_AFC_SET_CNT(192);
	RF_AFC_MUX_ON();
	delay_10us(4); // 40us

	debug_msg_str_rf("rf_vco_coarse_tune");


	//t_s = 0;
	t_e = 0;

	for(i = 0 ; i < VCO_TRM_MAX; i+=3)
	{																		   
		set_vco_tune(i);

		for(j = 0; j < 3; j++)
		{
			if( (i == 15) && ( j > 0) ) break;
				
			MSB(vco_afc_l[i+j]) = EMI_READ(RR_AFC_CNT_H_0 + j*4);
			LSB(vco_afc_l[i+j]) = EMI_READ(RR_AFC_CNT_L_0 + j*4);	
							
			MSB(vco_afc_h[i+j]) = EMI_READ(RR_AFC_CNT_H_1 + j*4);
			LSB(vco_afc_h[i+j]) = EMI_READ(RR_AFC_CNT_L_1 + j*4);			

			vco_afc_l[i+j] = vco_afc_l[i+j] + CORE_AFC_ADJ_COEF_L;
			vco_afc_h[i+j] = vco_afc_h[i+j] + CORE_AFC_ADJ_COEF_H;

			vco_afc_l[i+j] = vco_afc_l[i+j] * 8;
			vco_afc_h[i+j] = vco_afc_h[i+j] * 8;

			if( temp < vco_afc_h[i+j] ) 
			{				
				vco_max = i+j;				
			}

			temp = vco_afc_h[i+j]; 					

			debug_msg_str_rf("[%d] vco_afc_l = %d",(int)i+j, vco_afc_l[i+j]);
			debug_msg_str_rf("[%d] vco_afc_h = %d",(int)i+j, vco_afc_h[i+j]);

			//t_s++;
			// jsyi, AFC ERROR... RETRY  
			if( ((vco_afc_h[i+j]- vco_afc_l[i+j] ) < VCO_BAND_MIN_BW ) ||
				 (vco_afc_h[i+j] < VCO_RANGE_MIN) || 
				 (vco_afc_l[i+j] < VCO_RANGE_MIN)  	 )	
			{
				RF_SET_PLL_OFF();
				RF_SET_LDO_OFF();
				delay_10us(10); // 100us								
				RF_SET_LDO_ON();
				RF_SET_PLL_ON();				
				delay_10us(10); // 100us
				i = 0;
				break;
			} 			
		}
	}
	
	debug_msg_str_rf("t_s = %d",(int) t_s);
	RF_AFC_MUX_OFF();
	//RF_SET_PLL_OFF();
	//RF_SET_LDO_OFF();
	
	for(i = 0; i < VCO_REGION_MAX; i++)
	{
		t_s = UNCAL;
		t_e = 0;
			
		for(j = 0; j <= vco_max; j++)
		{
			if( (vco_afc_l[j] < region_freq_l[i]) && (vco_afc_h[j] > region_freq_h[i]) )
			{
				t_s = j;
				break;
			}
		}

		debug_msg_str_rf("[%d] t_s = %d",(int)i, (int)t_s);

		for(j = t_s ; j <= vco_max; j++)
		{
			if( (vco_afc_l[j] < region_freq_l[i]) && (vco_afc_h[j] > region_freq_h[i]) )
			{
				t_e++;
				ret = TRUE;
			}
		}

		debug_msg_str_rf("[%d] t_num = %d",(int)i, (int)t_e);

		if((t_s != UNCAL) && (t_s < vco_max) && (t_e > 1))
		{
			region_freq_curr_cent = (region_freq_h[i] + region_freq_l[i]) >> 1;
			vco_afc_curr_cent = (vco_afc_h[t_s] + vco_afc_l[t_s]) >> 1;
			vco_afc_next_cent = (vco_afc_h[t_s + 1] + vco_afc_l[t_s + 1]) >> 1;

			debug_msg_str_rf("[%d] region_freq_h[i] = %d",(int)i, (int)region_freq_h[i]);
			debug_msg_str_rf("[%d] region_freq_l[i] = %d",(int)i, (int)region_freq_l[i]);
			debug_msg_str_rf("[%d] vco_afc_h[t_s] = %d",(int)i, (int)vco_afc_h[t_s]);
			debug_msg_str_rf("[%d] vco_afc_l[t_s] = %d",(int)i, (int)vco_afc_l[t_s]);
			debug_msg_str_rf("[%d] vco_afc_h[t_s + 1] = %d",(int)i, (int)vco_afc_h[t_s + 1]);
			debug_msg_str_rf("[%d] vco_afc_l[t_s + 1] = %d",(int)i, (int)vco_afc_l[t_s + 1]);
			debug_msg_str_rf("[%d] region_freq_curr_cent = %d",(int)i, (int)region_freq_curr_cent);
			debug_msg_str_rf("[%d] vco_afc_curr_cent = %d",(int)i, (int)vco_afc_curr_cent);
			debug_msg_str_rf("[%d] vco_afc_next_cent = %d",(int)i, (int)vco_afc_next_cent);

			if((vco_afc_next_cent - region_freq_curr_cent) < (region_freq_curr_cent - vco_afc_curr_cent))
				t_s++;

			debug_msg_str_rf("[%d] t_s = %d",(int)i, (int)t_s);

		}
		
		rf_vco_trim[i] = t_s;
	
		debug_msg_str_rf("core_vco_trim[%d] = %d",(int)i, (int)rf_vco_trim[i]);

	}

	debug_msg_str_rf("core_rf_vco_coarse_tune = %d",(int)ret);
	debug_msg_str_rf("vco_max = %d",(int) vco_max);					 	

	return ret;
}

void rf_rotate_phase(void)
{
	const uint8 phase_rotate[9] = { 0x0F, 0x0A, 0x07, 0x03, 0x10, 0x13, 0x17, 0x1A, 0x1F};
	static uint8 phase_idx = 0;

	phase_idx = (phase_idx + 1) % sizeof(phase_rotate);
	EMI_WRITE(RR_PLL_PHASE_SHIFT_I, phase_rotate[phase_idx]);	 		
	EMI_WRITE(RR_PLL_PHASE_SHIFT_Q, phase_rotate[phase_idx]);

	debug_msg_str_rf("rf_rotate_phase = %d", phase_idx);
}


void rf_get_temperature(EVENT e)
{
	const int t[] = {-53,-44,-34,-24,-15,-5,4,14,24,33,43,52,62,72,81,91};	
	int16 temp;
	uint8 ldo;

	ldo = EMI_READ(RR_BLOCK_ENABLE);
	
	RF_SET_LDO_ON();		
	EMI_SET_BIT(RR_TEMP_EN,0x80); // TEMP_EN
	delay_10us(100); // do not modify
	temp = EMI_READ(RR_TEMP_SENSE) & 0x0F;

#ifdef __FEATURE_USE_XTAL__
	if(temp < 10)
	{
//		RF_SET_PLL_F(rf_pll_cur_f + 1);	
		RF_SET_PLL_F(rf_pll_cur_f + (712/4));	
	}
	else
	{
//		RF_SET_PLL_F(rf_pll_cur_f + 4);
		RF_SET_PLL_F(rf_pll_cur_f + 712);
	}

#endif				
//	temp = (96*temp-532)/10;
	temp = t[temp];
			
	// restore
	EMI_CLR_BIT(RR_TEMP_EN,0x80); // TEMP_EN
	EMI_WRITE(RR_BLOCK_ENABLE,ldo);

	RFID_SYSTEM_TEMP = (int8) temp;
	
	e.handler = rf_get_temperature;
	e.param = EVENT_ENTRY;
	event_post_delayed(e, (1000*10) );	// every 10s

//	debug_msg_str("rf_get_temperature");
	
}

#ifdef __FEATURE_RF_TEMP_COMP__
//!---------------------------------------------------------------
//!	@brief
//!		Temperature compensation for Tx power
//!
//! @param
//!		None
//!
//! @return
//!		Returns temperature sensor value
//!  	0: -40 deg C
//!  	1: -20 deg C
//!  	2: 0 deg C
//!  	3: 20 deg C
//!  	4: 40 deg C
//!  	5: 60 deg C
//!  	6: 80 deg C
//!  	7: 100 deg C
//!---------------------------------------------------------------
uint8 rf_temp_comp()
{
	uint8 temp;
/*	uint8 tx_s;

	tx_s = pmode;

	if(tx_s == PMODE_S2_IDLE) RF_SET_LDO_ON();

	EMI_SET_BIT(RR_EN_TEMP, 0x80);

	delay_10us(50); // 500 us

	temp = EMI_READ(RR_TEMP_SENSE) & 0x0F;
	
	if(tx_s == PMODE_S2_IDLE) RF_SET_LDO_OFF();

	if(rf_temp_comp_enable == TRUE)	rf_tgain_temp_offset_idx = temp_comp_table[temp];
	else rf_tgain_temp_offset_idx = 0;
*/
	return temp;   
}

#endif

void rf_set_gain(uint8 gain_i, uint8 gain_q)
{
	uint8 remainder;
	uint8 index;

	index = gain_i/6;
	remainder = gain_i % 6;

	EMI_WRITE(RR_RX_GAIN_H_I, MSB(rf_gain_table[index]));
	EMI_WRITE(RR_RX_GAIN_L_I, LSB(rf_gain_table[index]) + remainder);	

	index = gain_q/6;
	remainder = gain_q % 6;

	EMI_WRITE(RR_RX_GAIN_H_Q, MSB(rf_gain_table[index]));
	EMI_WRITE(RR_RX_GAIN_L_Q, LSB(rf_gain_table[index]) + remainder);	
																				   	
}
 /*
uint8 rf_get_gain()
{
	uint16 cur_gain;
	uint8 remainder;
	uint8 i;

	cur_gain = TO_U16(EMI_READ(RR_RX_GAIN_H_I), EMI_READ(RR_RX_GAIN_L_I));
	reminder = cur_gain & 0x07;
	cur_gain -= remainder;

	for(i=0; i<sizeof(rf_gain_table); i++)
	{
		if(cur_gain ==  rf_gain_table[i])
			break;
	}

	return (uint8)((i * 6) + remainder);
}

*/
