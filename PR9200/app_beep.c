//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	app_beep.c
//! @brief  Example of user application - Beep
//! 
//! $Id: app_beep.c 1605 2012-05-10 05:36:28Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release

//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "pr9200.h"
#include "timer.h"
#include "hal.h"
#include "app_beep.h"


#define BEEP_NOTE_MAX		3
//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------  
hal_gpio_out_type 		beep_ctxt = {HAL_GPIO_DEV_NONE,}; // configure your beep port
hal_gpio_out_type 		led_working_ctxt = {HAL_GPIO_DEV_NONE,}; // configure your led port
hal_timer_type			beep_timer;


//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    
typedef struct
{
	uint32 	cnt_val;
	uint32	duration;
} beep_note_type;

typedef struct
{
	const beep_note_type 	note[BEEP_NOTE_MAX];
	uint8 					cur_note;
	uint32 					cur_duration;
} beep_perform_ctxt_type;

beep_perform_ctxt_type beep_perform_ctxt = {{
	{BEEP_NOTE_C,BEEP_DUR_QUARTER},	
	{BEEP_NOTE_D,BEEP_DUR_QUARTER}, 
	{BEEP_NOTE_E,BEEP_DUR_QUARTER}}, 0, 0};

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  


//!---------------------------------------------------------------
//!	@brief
//!		Stop the beep operaion
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void beep_stop()
{
	beep_timer.stop_cb();
	hal_timer_deregister();	
	led_working_ctxt.off_cb(&led_working_ctxt);  // led off
}

//!---------------------------------------------------------------
//!	@brief
//!		Interrupt service routine for beep
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void beep_isr()
{
	if( beep_perform_ctxt.cur_note >= BEEP_NOTE_MAX)
	{
		beep_stop();		
	}
	else if( beep_perform_ctxt.cur_duration > beep_perform_ctxt.note[beep_perform_ctxt.cur_note].duration)
	{
		TIMER3->LOAD = beep_perform_ctxt.note[beep_perform_ctxt.cur_note].cnt_val;
		beep_perform_ctxt.cur_note++;
		beep_perform_ctxt.cur_duration = 0;
	}
	else
	{			
		beep_perform_ctxt.cur_duration += beep_perform_ctxt.note[beep_perform_ctxt.cur_note].cnt_val;
		beep_ctxt.toggle_cb(&beep_ctxt);
	}
}

//!---------------------------------------------------------------
//!	@brief
//!		Initiliaze the Beep
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void beep_init()
{		
	hal_gpio_register(&beep_ctxt);	
	hal_gpio_register(&led_working_ctxt);

	beep_timer.irq_handler_cb = beep_isr;
	hal_timer_register(&beep_timer);
}

//!---------------------------------------------------------------
//!	@brief
//!		Executes the beep operation with Timer3
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void beep_perform(void)
{
	led_working_ctxt.on_cb(&led_working_ctxt);  // led on

	if(hal_timer_register(&beep_timer) != SYS_S_OK)
		return;

	beep_perform_ctxt.cur_note = 0;
	beep_perform_ctxt.cur_duration = 0;
	beep_timer.load_cb( beep_perform_ctxt.note[beep_perform_ctxt.cur_note].cnt_val );
}



