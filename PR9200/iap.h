//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file		iap.h
//! @brief	
//! 
//! $Id: iap.h 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/08/31	sjpark	initial release

#ifndef IAP_H
#define IAP_H

//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "commontypes.h"


#define IAP_START_ADDRESS	0xF000

// RCP Frame
#define MSG_PREAMBLE		0xBB
#define MSG_COMMAND			0x0
#define MSG_RESPONSE		0x1
#define MSG_FLASH_DATA		0x57
#define MSG_ENDMARK			0x7E

#define SPI_MODE_CHANGE		0x0A 

//RCP COMMAND
#define CMD_VERSION			0x03
#define CMD_VER_DEVICE		0x00
#define CMD_VER_FACTORY		0x02 
#define CMD_DOWNLOAD		0xB1
#define CMD_DUMP			0xB3
#define CMD_ERROR			0xFF
#define CMD_RESET			0x08

//RCP ERR CODE 
#define CMD_RSP_SUCCESS		0x00
#define CMD_RSP_UNDEF		0x18
#define CMD_RSP_ERCRC		0xFF

//RCP Frame Index
#define MSG_MSGTYPE_INDEX	0x0
#define MSG_CMD_INDEX		0x1
#define MSG_PLENH_INDEX		0x2
#define MSG_PLENL_INDEX		0x3
#define MSG_DUMMY_INDEX		0x2

//RCP Frame Len
#define MSG_HEAD_LEN        0x04	 // Preamable is not Include
#define MSG_TAIL_LEN        0x03
#define MSG_HEADTAIL_LEN    (MSG_HEAD_LEN + MSG_TAIL_LEN)
#define MSG_DATAPKT_LEN     0x16

//RCP COM MODE
#define MODE_IAP_UART    	0x0
#define MODE_IAP_SPI     	0x1
#define MODE_IAP_I2C		0x2

//protocal frame information struct
typedef union
{
	struct
	{
		uint8  msg;
		uint8  cmd;
		uint8  data_len_h;
		uint8  data_len_l;
		uint8  data[64];

	}__attribute__((packed))command;

	struct
	{
		uint8  msg;
		uint8  addr_h;
		uint8  addr_l;
		uint8  dummy;
		uint8  data[64];
	}__attribute__((packed))data;

	uint8 rx_buffer[68];
	
}rcp_packet_type;


//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
void iap_main(uint8 mode)__attribute__((section(".ARM.__at_0xF000")));

#endif

