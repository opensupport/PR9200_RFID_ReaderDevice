//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//! 
//! @file	sio.h
//! @brief	SIO Device Driver
//! 
//! $Id: sio.h 1759 2012-09-17 05:20:55Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release
#ifndef SIO_H
#define SIO_H
//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "config.h"
#include "commontypes.h"

#ifdef __FEATURE_UART_RCP__
#define SIO_PKT_QUEUE_MAX 	(1)
#else
#define SIO_PKT_QUEUE_MAX 	(1)
#endif
#define SIO_BYTE_BUF_MAX 	(256)

#define SIO_INIT_IRQ()		(GPIO1->DIR |= BIT0)
#define SIO_SET_IRQ()		(GPIO1->DATA &= ~BIT0)
#define SIO_CLR_IRQ()		(GPIO1->DATA |=  BIT0)

#ifndef _FEATURE_UART_RCP__
#define SIO_TST_IRQ()		(!(GPIO1->DATA & BIT0))
#else
#define SIO_TST_IRQ()		(0)
#endif

#define RCP_REQ_SIO_RSP		(0x0A)

typedef struct
{
	uint8 len;
	uint8 data[SIO_BYTE_BUF_MAX];
} sio_byte_buf_type;

typedef struct 
{
	uint8 count;
	uint8 head;
	uint8 tail;	
	sio_byte_buf_type pkt[SIO_PKT_QUEUE_MAX];
} sio_pkt_queue_type;

typedef enum
{	
	SIO_UART = 0,
	SIO_SPI,
	SIO_I2C
} sio_dev_type;

typedef void(*sio_nonparam_pf)(void);
typedef void(*sio_param_pf)(uint8 *inbyte, uint16 size);

typedef struct
{	
	sio_param_pf 	send;
	sio_param_pf	receive_cb;
	sio_nonparam_pf	init;
} sio_interface_type;



void sio_init(void);
sio_dev_type sio_get_io_sel(void);
void sio_register_io(sio_interface_type *io);
void sio_post_received_data(uint8* data, uint8 len);
void sio_byte_buf_flush(sio_byte_buf_type* ptr_self);
void sio_byte_buf_enqueue(sio_byte_buf_type* ptr_self, uint8 inbyte);
uint8 sio_byte_byte_dequeue(sio_byte_buf_type* ptr_self);
void sio_pkt_queue_flush(sio_pkt_queue_type* ptr_self);
void sio_enqueue_tx_pkt(uint8 *inbyte, uint8 size);
sio_byte_buf_type* sio_pkt_dequeue(sio_pkt_queue_type* ptr_self);
#endif

