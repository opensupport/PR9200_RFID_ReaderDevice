//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	uart.h
//! @brief	UART Device Driver
//! 
//! $Id: uart.h 1671 2012-07-13 00:36:02Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release



#ifndef UART_H
#define UART_H



#include <stdarg.h>
#include <stdio.h>
#include "commontypes.h"
#include "config.h"

// Reference clocks
#define UART_REF_CLK  	(SYS_DIG_CLK)

// Data read/write register bits
#define DR_OE           (1 << 11)
#define DR_BE           (1 << 10)
#define DR_PE           (1 << 9)
#define DR_FE           (1 << 8)

// Receive status register bits
#define RSR_OE          0x08
#define RSR_BE          0x04
#define RSR_PE          0x02
#define RSR_FE          0x01
            
// Flag register bits
#define RI				0x100
#define TXFE			0x080
#define RXFF			0x040
#define TXFF			0x020 
#define RXFE			0x010
#define BUSY			0x008
#define DCD				0x004
#define DSR				0x002
#define CTS				0x001

// Line control HIGH register bits
#define SPS        		0x80
#define WLEN_8BIT  		0x60
#define WLEN_7BIT   	0x40
#define WLEN_6BIT		0x20
#define WLEN_5BIT		0x00
#define FEN        		0x10
#define STP2       		0x08
#define EPS        		0x04
#define PEN        		0x02
#define BRK        		0x01

// Control register bits
#define CTSEN	(1 << 15)
#define RTSEN	(1 << 14)
#define OUT2	(1 << 13)
#define OUT1	(1 << 12)
#define RTS     (1 << 11)
#define DTR     (1 << 10)
#define RXE     (1 << 9)
#define TXE     (1 << 8)
#define LBE     (1 << 7)
#define SIRLP   (1 << 2)
#define SIREN   (1 << 1)
#define UARTEN  (1 << 0)

#define UART_M_ENABLE_FIFO  (1 << 0)
#define UART_M_ENABLE_IRDA  (1 << 1)
#define UART_M_RAW          (1 << 2)

#define UART_RX_FIFO_EMPTY  0
#define UART_TX_FIFO_FULL   1

#define UART_M_RTS          (1 << 0)
#define UART_M_CTS          (1 << 1)
#define UART_M_DTR          (1 << 2)
#define UART_M_DSR          (1 << 3)
#define UART_M_DCD          (1 << 4)
#define UART_M_RI           (1 << 5)

// Interrupt FIFO register bits               
#define IFLS_RXL_MSB    5
#define IFLS_RXL_LSB    3
#define IFLS_TXL_MSB    2
#define IFLS_TXL_LSB    0

#define INT_OE         	(1<<10)
#define INT_BE          (1<<9)
#define INT_PE          (1<<8)
#define INT_FE          (1<<7)
#define INT_RT          (1<<6)
#define INT_TX          (1<<5)
#define INT_RX          (1<<4)
#define INT_DSR         (1<<3)
#define INT_DCD         (1<<2)
#define INT_CTS         (1<<1)
#define INT_RI          (1<<0)

#if defined(__FEATURE_UART_RCP__) || defined(__FEATURE_GPIO_SIO_SEL__) 	
void uart_init(void);	  
void uart_tx_pkt (uint8 *inbyte, uint16 size);
#endif

#endif
