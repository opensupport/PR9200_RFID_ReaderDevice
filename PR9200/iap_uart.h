//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file		iap_uart.h
//! @brief	
//! 
//! $Id: iap_uart.h 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/08/31	sjpark	initial release

#ifndef	IAP_UART_H
#define IAP_UART_H

#include "commontypes.h"

#define UART_CLK (19200000UL)


extern void iap_uart_init(void);
extern uint8 iap_uart_get_status(void);
extern int32 iap_uart_get_char(void);
extern void iap_uart_put_char(uint8);
extern void iap_uart_send_frame(uint8 cmd, uint8 *data, uint8 len,  uint8 h_crc, uint8 l_crc);

#endif 

