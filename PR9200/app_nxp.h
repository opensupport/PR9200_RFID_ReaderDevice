//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	app_nxp.h
//! @brief  Example of user application - NXP requirement
//! 
//! $Id: app_nxp.h 1656 2012-07-05 02:56:46Z sjpark $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/05/10	jsyi		initial release


#ifndef APP_NXP_H
#define APP_NXP_H

#include "commontypes.h"
#include "event.h"


//!-------------------------------------------------------------------
//! Definitions
//!------------------------------------------------------------------- 

typedef enum
{
	EVENT_NXP_CLR_WORD = EVENT_CUSTOM_BASE,
	EVENT_NXP_CHECK_WORD1,
	EVENT_NXP_CHECK_WORD1_EXPIRED,
	EVENT_NXP_LED_MODE0,
	EVENT_NXP_WAIT_EVENT,
	EVENT_NXP_READ_EPC,
	EVENT_NXP_CHECK_EPC,
	EVENT_NXP_LED_MODE1,
	EVENT_NXP_LED_MODE2, 	  
	EVENT_NXP_WAIT_IN1_INACTIVE,
} nxp_fsm_event_type;

typedef struct
{
	uint8	sw_status;
	uint8	bt_status;
	uint8 	led_status;
	uint8	inv_completed;

	uint16	tag_detected;

//	uint8    epc_cnt;
	uint16 	word1;
	uint16	word2;
	nxp_fsm_event_type	next_state;
} nxp_ctxt_type;


#define RCP_CMD_CUSTOM_READ_I2C		RCP_CMD_CUSTOM_00
#define RCP_CMD_CUSTOM_WRITE_I2C	RCP_CMD_CUSTOM_01
#define RCP_CMD_CUSTOM_SET_LED_TIME	RCP_CMD_CUSTOM_02


#define LED_PERFORM		(0)
#define LED_STOP			(1)


//#define NXP_EPC_TAG_MAX			10

	
#ifdef	__DEBUG_NXPFSM__
#define	debug_msg_str_nxp_fsm(a,...)	debug_msg_str(a,##__VA_ARGS__)
#else
#define	debug_msg_str_fsm(a,...)
#endif


//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
void nxp_init(void);
void nxp_event_dispatch(EVENT e);
void nxp_get_epc(uint8 type, uint8 len, uint8* data);
void nxp_complete_inventory(uint8 discontinued, uint8 detected);

void nxp_parse_rcp(void* p);
void nxp_input1_handler(void);
void nxp_input2_handler(void);
void nxp_led_handler(void);
void nxp_ucodei2c_set_rf_interface(uint8 mode);

#endif
