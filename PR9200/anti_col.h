//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	anti_col.h
//! @brief  Anti-collision algorithm
//! 
//! $Id: anti_col.h 1632 2012-06-26 06:57:30Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/05/14	jsyi		initial release


#ifndef ANTI_COL_H
#define ANTI_COL_H

#include "commontypes.h"
#include "config.h"

#ifdef	__DEBUG_ANTICOL__
#define	debug_msg_str_anticol(a,...)	debug_msg_str(a,##__VA_ARGS__)
#else
#define	debug_msg_str_anticol(a,...)
#endif

//!-------------------------------------------------------------------
//! Fuction Definitions
//!-------------------------------------------------------------------
void anticol_set_mode(uint8 mode);
void anticol_reset_mode(void);
void anticol_update_round(uint8 slot_tag, uint8 slot_col, uint8 slot_no_tag);
uint8 anticol_get_qval(uint8 q);
#endif

