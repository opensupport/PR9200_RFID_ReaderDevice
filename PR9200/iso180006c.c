//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	iso180006c.c
//! @brief	Air interface protocol handler (EPC Gen2 protocol stack)
//! 
//! $Id: iso180006c.c 1755 2012-09-14 08:33:41Z jsyi $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi	initial release
//! 2011/07/25	sjpark	modified for PR9200

//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  
#include <STRING.H>
#include "PR9200.h"
#include "registry.h"
#include "modem.h"
#include "rfidtypes.h"
#include "utility.h"
#include "anti_col.h"
#include "rf.h"
#include "iso180006c.h"

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------        

extern 	modem_status_type modem_s;

//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 
#define ISO180006C_SEL_CMD					(0x0A)
#define ISO180006C_QRY_CMD 					(0x08)
#define ISO180006C_QRYADJ_CMD				(0x09)
#define ISO180006C_QRYREP_CMD				(0x00)
#define ISO180006C_ACK_CMD					(0x01)
#define ISO180006C_NACK_CMD					(0xC0)

#define	ISO180006C_REQ_RN_CMD				(0xC1)
#define ISO180006C_READ_CMD					(0xC2)
#define ISO180006C_WRITE_CMD				(0xC3)
#define	ISO180006C_KILL_CMD					(0xC4)
#define	ISO180006C_LOCK_CMD					(0xC5)
#define	ISO180006C_ACCESS_CMD				(0xC6)

#define	ISO180006C_BLOCKWRITE_CMD			(0xC7)
#define	ISO180006C_BLOCKERASE_CMD			(0xC8)

#define	NXP_READPROTECT_CMD					(0xE001)
#define	NXP_RESET_READPROTECT_CMD			(0xE002)
#define	NXP_CHANGE_EAS						(0xE003)
#define	NXP_EAS_ALARM						(0xE004)
#define	NXP_INV_EAS_ALARM					(0x1FFB)
#define	NXP_CALIBRATE						(0xE005)

typedef struct 
{
	uint8 byte_length;
	uint8 ebv[5];
} evb_type;

//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    
iso180006c_prm_type iso180006c_ctxt;
iso180006c_rfid_pkt_type iso180006c_rfid_pkt_c = {0,};
iso180006c_tag_buffer_type tag_buf;
iso180006c_inventory_prm_type inventory_prm;
uint8 iso180006c_handle[2] = {0,};
// modify jwkim, 20121025
uint8	error_value = 0;
// end modify, 20121025

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  

//!---------------------------------------------------------------
//!	@brief
//!		Initialize TypeC paramerters
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void iso180006c_init()
{
	iso180006c_ctxt.qry.q = reg_prt_prm.qry_q;
	iso180006c_ctxt.sel.mem_bank = 0x01;	
	iso180006c_set_anticol_mode(reg_anticol.mode);
}

//!---------------------------------------------------------------
//!	@brief
//!		Set modulation parameters
//!
//! @param
//!		blf: BLF_40 / BLF_80 / BLF_160 / BLF_200 / BLF_213 /
//!			  BLF_250 / BLF_300 / BLF_320 / BLF_640
//!		rx_mod: FM0 / MI2 / MI4 / MI8
//!
//! @return
//!  	none
//!---------------------------------------------------------------
BOOL iso180006c_set_modulation_param(modem_rx_blf_type rx_blf, modem_rx_mod_type rx_mod, modem_rx_dr_type rx_dr)
{
	modem_set_m(rx_mod);
	modem_set_blf(rx_blf, rx_dr); // tari = f(rx_blf,rx_dr);
	rf_set_tx_filt(modem_c.tx_tari);
	rf_set_rx_blf(rx_blf,rx_mod);	
	modem_set_ted(rx_blf,rx_mod);
 	modem_set_agc();
	modem_set_dcoc();

	iso180006c_ctxt.qry.dr = rx_dr;
	iso180006c_ctxt.qry.m = rx_mod >> 6;
	iso180006c_ctxt.qry.trext = (iso180006c_ctxt.qry.m & 0x02) ? 0 : 1;

	inventory_prm.update = TRUE;
	
	return TRUE;
}

//!---------------------------------------------------------------
//!	@brief
//!		Set inventory mode
//!
//! @param
//!		mode: inventory mode type
//!
//! @return
//!		None
//!
//!--------------------------------------------------------------- 
void iso180006c_set_anticol_mode(protocol_anti_col_type mode)
{	
	anticol_set_mode(mode);

	switch(mode)
	{
		case ANTI_COL_MODE0:
			iso180006c_ctxt.sel.target = 0x00;
			iso180006c_ctxt.sel.action = 0x00;
			iso180006c_ctxt.sel.pointer = 0x0000;
			iso180006c_ctxt.sel.len = 0x00;
			iso180006c_ctxt.sel.truncate = 0x00;
			iso180006c_ctxt.qry.session = 0x00;
			break;
		case ANTI_COL_MODE1:
			iso180006c_ctxt.qry.session = 0x01;
			iso180006c_ctxt.sel.mem_bank = 0x00;
			break;
		case ANTI_COL_MODE2:
		case ANTI_COL_MODE3:
		case ANTI_COL_MODE4:
		case ANTI_COL_MODE5:
		case ANTI_COL_MODE6:
			iso180006c_ctxt.qry.session = 0x02;
			iso180006c_ctxt.sel.mem_bank = 0x00;
			break;
	}

	inventory_prm.anti_col = mode;
	inventory_prm.update = TRUE;	
		
}

//!---------------------------------------------------------------
//!	@brief
//!		Reset inventory mode
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!--------------------------------------------------------------- 
void iso180006c_reset_inventory_mode(void)
{
	anticol_reset_mode();
}


//!---------------------------------------------------------------
//!	@brief
//!		Convert uint16 to Extensible bit vectors (EBV) format
//!
//! @param
//!		uint16 source, 
//!		evb_type *dst
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void build_ebv(uint16 source, evb_type *dst)
{
	//memset((void*) dst, 0x00, sizeof(evb_type));
	dst->byte_length = 0;

	if(source < (1 << 7) )
	{
		dst->byte_length = 1;
		dst->ebv[0] = (uint8) source;
	}
	else if(source < (1<< 14))
	{
		dst->byte_length = 2;
		dst->ebv[0] = source >> 7;
		dst->ebv[0] += 0x80;
		dst->ebv[1] = source & 0x7F;
	}
	else
	{
		dst->byte_length = 3;
		dst->ebv[0] = source >> 14;
		dst->ebv[0] += 0x80;
		dst->ebv[1] = (0x7F) & (source >> 7);
		dst->ebv[1] += 0x80;
		dst->ebv[2] = source & 0x7F;
	}
}


//!---------------------------------------------------------------
//!	@brief
//!		Append src bit to rfid_packet_type
//!
//! @param
//!		rfid_packet_type
//!		src (LSB aligned source byte)
//!		length (Bit Length)
//!
//! @return
//!		uint8 - aligned value
//!
//!---------------------------------------------------------------
void bit_append(rfid_packet_type* dst, uint8* src, uint8 length)
{
  	uint8 i;
	uint8 dstbyte;
	uint8 dstbit;
	
	for(i = 0; i < length; i++)
	{
		dstbyte = dst->bit_length >> 3;
		dstbit =  dst->bit_length % 8;

		if ( *src & (1 << (length - i - 1) ) ) 				// if srcbit is '1' in source byte
		{
			dst->air_pkt[dstbyte] |=  (1 << (7 - dstbit) ); // set dstbit in destination byte
		}
		else  												// else srcbit is '0' in source byte
		{
			dst->air_pkt[dstbyte] &= ~(1 << (7 - dstbit) ); // clear dstbit in destination byte
		}
		dst->bit_length++;
	}
}


//!---------------------------------------------------------------
//!	@brief
//!		Returns a tag_buf ID stored in the tag_buf Buffer
//!
//! @param
//!		None
//!
//! @return
//!		rfid_pkt_rx_type*: Tag ID pointer
//!
//!---------------------------------------------------------------
rfid_pkt_rx_type* iso180006c_pop_tag()
{
	if( tag_buf.count == 0 )
	{
		return &tag_buf.id[0];
	}
	
	tag_buf.count--;	
	return &tag_buf.id[tag_buf.count];
}

//!---------------------------------------------------------------
//!	@brief
//!		Build type C Select Air Packet
//!
//! @param
//!		rfid_packet_type *spkt
//!
//!	@dependency
//!		iso180006c_ctxt.sel ( from RCP )
//!
//! @return
//!		struct rfid_packet_type (Type C Select Air Packet)
//!
//!---------------------------------------------------------------
void iso180006c_pktbuild_sel(rfid_packet_type *spkt)
{	
	evb_type ptr;
	uint8 cmd = ISO180006C_SEL_CMD;
	uint8 i;
	uint8 len_byte;
	uint8 len_dbit;
	uint8 lsb_aligned;
	
	spkt->bit_length = 0;
	build_ebv(iso180006c_ctxt.sel.pointer, &ptr);

	// Packet Build 
	bit_append(spkt, &cmd, 4); // code(4)
	bit_append(spkt, &iso180006c_ctxt.sel.target, 3); // target(3)
	bit_append(spkt, &iso180006c_ctxt.sel.action, 3); // action(3) 
	bit_append(spkt, &iso180006c_ctxt.sel.mem_bank, 2); // membank(2)
		
	for(i = 0; i < ptr.byte_length; i++)
	{
		bit_append(spkt, &ptr.ebv[i], 8) ; // pointer(EBV)
	}

	bit_append(spkt, &iso180006c_ctxt.sel.len, 8); // length(8)
		
	// mask append (length)
	len_byte = (iso180006c_ctxt.sel.len + 7 ) >> 3;
	len_dbit = iso180006c_ctxt.sel.len % 8;

	for( i = 0 ; i < len_byte; i++)
	{	
		if( ((i + 1) == len_byte) && (len_dbit != 0) )
		{
			lsb_aligned = iso180006c_ctxt.sel.mask[i] >> (8 - len_dbit); // if RCP alined with LSB
			bit_append(spkt, &lsb_aligned, len_dbit);
			
			//debug_msg_str2("[%d] %02X",(int) i, (int) lsb_aligned);
		}
		else
		{
			bit_append(spkt, &iso180006c_ctxt.sel.mask[i], 8);
			
			//debug_msg_str2("[%d] %02X",(int) i, (int) eeprom_items_c.param.TYPECprm.Sel.Mask[i]);
		}
	}

	//debug_msg_str("len_byte = %02X",(int) len_byte );
	
	bit_append(spkt, &iso180006c_ctxt.sel.truncate, 1); // truncate(1)
	
}


//!---------------------------------------------------------------
//!	@brief
//!		Build type C Query Air Packet
//!
//! @param
//!		rfid_packet_type *qpkt
//!
//!		Dependency:
//!		eeprom_items_c.item.typec_prm.qry ( from RCP )
//!
//! @return
//!		struct rfid_packet_type (Type C Query Air Packet)
//!
//!---------------------------------------------------------------
void iso180006c_pktbuild_qry(rfid_packet_type *qpkt)
{
	uint8 cmd = ISO180006C_QRY_CMD;

	qpkt->bit_length = 0;	

	//! Packet Build
	bit_append(qpkt, &cmd, 4); // Command(4)
	bit_append(qpkt, &iso180006c_ctxt.qry.dr, 1); // DR(1)
	bit_append(qpkt, &iso180006c_ctxt.qry.m, 2); // M(2)
	iso180006c_ctxt.qry.trext = (iso180006c_ctxt.qry.m & 0x02) ? 0 : 1;
	bit_append(qpkt, &iso180006c_ctxt.qry.trext, 1); // TRext(1)
	bit_append(qpkt, &iso180006c_ctxt.qry.sel, 2); // Sel(2)
	bit_append(qpkt, &iso180006c_ctxt.qry.session, 2); // Session (2)
	bit_append(qpkt, &iso180006c_ctxt.qry.target, 1); // Target(1)
	bit_append(qpkt, &iso180006c_ctxt.qry.q, 4); // Q(4)
}

#if (0)
//!---------------------------------------------------------------
//!	@brief
//!		Build type C NACK Air Packet
//!
//! @param
//!		rfid_packet_type *spkt
//!
//!		Dependency:
//!		iso180006c_ctxt.sel ( from RCP )
//!
//! @return
//!		struct air_packet_type (Type C NACK Air Packet)
//!
//!---------------------------------------------------------------
void iso180006c_pktbuild_nack(rfid_packet_type *spkt)
{	
	uint8 cmd = ISO180006C_NACK_CMD;

	memset((void*) spkt, 0x00, sizeof(rfid_packet_type));

	//! Packet Build 
	bit_append(spkt, &cmd, 8); // code(8)
}
#endif

#if (0)
//!---------------------------------------------------------------
//!	@brief
//!		Build type C QueryAdjust Air Packet
//!
//! @param
//!		rfid_packet_type *qpkt
//!
//!		Dependency
//!		iso180006c_ctxt.qry ( from RCP )
//!
//! @return
//!		struct air_packet_type (Type C Query Air Packet)
//!
//!---------------------------------------------------------------
void iso180006c_pktbuild_qryadj(rfid_packet_type *qpkt)
{
	uint8 cmd = ISO180006C_QRYADJ_CMD;

	memset((void*) qpkt, 0x00, sizeof(rfid_packet_type));

	//! Packet Build
	bit_append(qpkt, &cmd, 4); // Command(2)
	bit_append(qpkt, &iso180006c_ctxt.qry.session, 2); // Session(2)
	bit_append(qpkt, &iso180006c_ctxt.qry.updn, 3); // Updn(3)
}
#endif

//!---------------------------------------------------------------
//!	@brief
//!		Build type C QueryRep Air Packet
//!
//! @param
//!		rfid_packet_type *qpkt
//!
//!	Dependency
//!		iso180006c_ctxt.qry ( from RCP )
//!
//! @return
//!		struct rfid_packet_type (Type C Query Air Packet)
//!
//!---------------------------------------------------------------
void iso180006c_pktbuild_qryrep(rfid_packet_type *qpkt)
{
	uint8 cmd = ISO180006C_QRYREP_CMD;

	//memset((void*) qpkt, 0x00, sizeof(rfid_packet_type));
	qpkt->bit_length = 0;

	//! Packet Build
	bit_append(qpkt, &cmd, 2); // Command(2)
	bit_append(qpkt, &iso180006c_ctxt.qry.session, 2); // Sel(2)
}


//!---------------------------------------------------------------
//!	@brief
//!		Build type C Read Air Packet
//!
//! @param
//!		rfid_packet_type *rpkt (Return value)
//!		enum protocol_c_membank_type
//!			MEM_RESERVE = 0x00
//!			MEM_EPC		= 0x01
//!			MEM_TID		= 0x02
//!			MEM_USER	= 0x03
//!		uint16 wort_prt (WordPtr)
//!		uint8 word_count (WordCount)
//!
//! @return
//!		NONE
//!
//!---------------------------------------------------------------
void typec_apbuild_read(rfid_packet_type *rpkt, protocol_c_membank_type mb, uint16 wort_prt, uint8 word_count)
{
	evb_type ptr;
	uint8 cmd = ISO180006C_READ_CMD;
	int i;

	//memset((void*) rpkt, 0x00, sizeof(rfid_packet_type));
	rpkt->bit_length = 0;	
	build_ebv(wort_prt, &ptr);

	// Packet Build     
	bit_append(rpkt, &cmd, 8); // Command(8)
	bit_append(rpkt, (uint8*) &mb, 2); // MemBank(2)
	for(i = 0; i < ptr.byte_length; i++)
	{
		bit_append(rpkt, &ptr.ebv[i], 8); // WordPtr(EBV)
	}
	bit_append(rpkt, &word_count, 8); // WordCount(8)
	bit_append(rpkt, &iso180006c_handle[0], 8); // RN(16)
	bit_append(rpkt, &iso180006c_handle[1], 8); // RN(16)
	
}


//!---------------------------------------------------------------
//!	@brief
//!		Build type C Write Air Packet
//!
//! @param
//!		rfid_packet_type *rpkt (Return value)
//!		enum protocol_c_membank_type
//!			MEM_RESERVE = 0x00
//!			MEM_EPC		= 0x01
//!			MEM_TID		= 0x02
//!			MEM_USER	= 0x03
//!		uint16 wort_prt (WordPtr)
//!		uint16 word_data (WordCount)
//!
//! @return
//!		struct rfid_packet_type (Type C Query Air Packet)
//!
//!---------------------------------------------------------------
void typec_apbuild_write(rfid_packet_type *rpkt, protocol_c_membank_type mb, uint16 wort_prt, uint16 word_data)
{
	evb_type ptr;
	uint8 cmd = ISO180006C_WRITE_CMD;
	int i;

	//memset((void*) rpkt, 0x00, sizeof(rfid_packet_type));
	rpkt->bit_length = 0;
	build_ebv(wort_prt, &ptr);
	
	// Packet Build     
	bit_append(rpkt, &cmd, 8); // Command(8)
	bit_append(rpkt, (uint8*) &mb, 2); // MemBank(2)
	for(i = 0; i < ptr.byte_length; i++)
	{
		bit_append(rpkt, &ptr.ebv[i], 8); // WordPtr(EBV)
	}
	bit_append(rpkt, &MSB(word_data), 8); // word_data(8)
	bit_append(rpkt, &LSB(word_data), 8); // word_data(8)
	bit_append(rpkt, &iso180006c_handle[0], 8); // RN(16)
	bit_append(rpkt, &iso180006c_handle[1], 8); // RN(16)
}


//!---------------------------------------------------------------
//!	@brief
//!		Build type C Block Write Air Packet
//!
//! @param
//!		rfid_packet_type *rpkt (Return value)
//!		enum protocol_c_membank_type
//!			MEM_RESERVE = 0x00
//!			MEM_EPC		= 0x01
//!			MEM_TID		= 0x02
//!			MEM_USER	= 0x03
//!		uint16 wort_prt (WordPtr)
//!		uint16 word_data (WordCount)
//!
//! @return
//!		struct rfid_packet_type (Type C Query Air Packet)
//!
//!---------------------------------------------------------------
void typec_apbuild_blockwrite(rfid_packet_type *rpkt, protocol_c_membank_type mb, uint16 wort_prt, uint8 word_count, const uint16 *word_data)
{
	evb_type ptr;
	uint8 cmd = ISO180006C_BLOCKWRITE_CMD;
	int i;

	//memset((void*) rpkt, 0x00, sizeof(rfid_packet_type));
	rpkt->bit_length = 0;
	build_ebv(wort_prt, &ptr);
	
	// Packet Build     
	bit_append(rpkt, &cmd, 8); // Command(8)
	bit_append(rpkt, (uint8*) &mb, 2); // MemBank(2)
	
	for(i = 0; i < ptr.byte_length; i++)
	{
		bit_append(rpkt, &ptr.ebv[i], 8); // WordPtr(EBV)
	}
	
	bit_append(rpkt, &word_count, 8);	// word_count(8)
	
	for(i = 0; i < word_count; i++)
	{
		bit_append(rpkt, &LSB(word_data[i]), 8); // word_data(8)
		bit_append(rpkt, &MSB(word_data[i]), 8); // word_data(8)
	}
	
	bit_append(rpkt, &iso180006c_handle[0], 8); // RN(16)
	bit_append(rpkt, &iso180006c_handle[1], 8); // RN(16)
}


//!---------------------------------------------------------------
//!	@brief
//!		Build type C Block Erase Air Packet
//!
//! @param
//!		rfid_packet_type *rpkt (Return value)
//!		enum protocol_c_prm_read_erase_type
//!			MEM_RESERVE = 0x00
//!			MEM_EPC		= 0x01
//!			MEM_TID		= 0x02
//!			MEM_USER	= 0x03
//!		uint16 wort_prt (WordPtr)
//!		uint16 word_data (WordCount)
//!
//! @return
//!		struct rfid_packet_type (Type C Query Air Packet)
//!
//!---------------------------------------------------------------
void typec_apbuild_blockerase(rfid_packet_type *rpkt, protocol_c_membank_type mb, uint16 wort_prt, uint8 word_count)
{
	evb_type ptr;
	uint8 cmd = ISO180006C_BLOCKERASE_CMD;
	int i;

	//memset((void*) rpkt, 0x00, sizeof(rfid_packet_type));
	rpkt->bit_length = 0;
	build_ebv(wort_prt, &ptr);
	
	// Packet Build     
	bit_append(rpkt, &cmd, 8); // Command(8)
	bit_append(rpkt, (uint8*) &mb, 2); // MemBank(2)
	
	for(i = 0; i < ptr.byte_length; i++)
	{
		bit_append(rpkt, &ptr.ebv[i], 8); // WordPtr(EBV)
	}
	
	bit_append(rpkt, &word_count, 8);	// word_count(8)
	
	bit_append(rpkt, &iso180006c_handle[0], 8); // RN(16)
	bit_append(rpkt, &iso180006c_handle[1], 8); // RN(16)
}

//!---------------------------------------------------------------
//!	@brief
//!		Build type C Kill Air Packet
//!
//! @param
//!		rfid_packet_type *rpkt (Return value)
//!		uint8 xored_pw_msb
//!		uint8 xored_pw_lsb
//!
//! @return
//!		NONE
//!
//!---------------------------------------------------------------
void typec_apbuild_kill(rfid_packet_type *rpkt, uint8 xored_pw_msb, uint8 xored_pw_lsb, uint8 recom)
{
	uint8 cmd = ISO180006C_KILL_CMD;

	
	//memset((void*) rpkt, 0x00, sizeof(rfid_packet_type));
	rpkt->bit_length = 0;
	
	//! Packet Build     
	bit_append(rpkt, &cmd, 8); // Command(8)
	bit_append(rpkt, (uint8*) &xored_pw_msb, 8); // Password MSB(8)
	bit_append(rpkt, (uint8*) &xored_pw_lsb, 8); // Password LSB(8)
	bit_append(rpkt, (uint8*) &recom, 3); // RFU/Recom (3)
	bit_append(rpkt, &iso180006c_handle[0], 8); // RN(16)
	bit_append(rpkt, &iso180006c_handle[1], 8); // RN(16)
}


//!---------------------------------------------------------------
//!	@brief
//!		Build type C Access Air Packet
//!
//! @param
//!		rfid_packet_type *rpkt (Return value)
//!		uint8 xored_pw_msb
//!		uint8 xored_pw_lsb
//!
//! @return
//!		NONE
//!
//!---------------------------------------------------------------
void typec_apbuild_access(rfid_packet_type *rpkt, uint8 xored_pw_msb, uint8 xored_pw_lsb)
{
	uint8 cmd = ISO180006C_ACCESS_CMD;
//	uint8 rfu = 0;
	//memset((void*) rpkt, 0x00, sizeof(rfid_packet_type));
	rpkt->bit_length = 0;

	// Packet Build     
	bit_append(rpkt, &cmd, 8); // Command(8)
	bit_append(rpkt, (uint8*) &xored_pw_msb, 8); // Password MSB(8)
	bit_append(rpkt, (uint8*) &xored_pw_lsb, 8); // Password LSB(8)
	bit_append(rpkt, &iso180006c_handle[0], 8); // RN(16)
	bit_append(rpkt, &iso180006c_handle[1], 8); // RN(16)
}


//!---------------------------------------------------------------
//!	@brief
//!		EPC Read + Handle Acquisition & EPC Confirmation
//!
//! @param
//!		NONE
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
int8 iso180006c_select_inventory()
{
	uint8 slot_tag = 0;	
	uint8 slot_collision = 0;
	uint8 slot_no_tag = 0;
	uint32 slot_cur = 0;	
	uint32 slot_end;
	uint8 rount_q;	
	
	// tag_buf buffer full 
	rount_q = anticol_get_qval(iso180006c_ctxt.qry.q);
//	slot_end = MIN(((uint32)(0x01 << rount_q)), INVENTORY_MAX_TAG_IN_ROUND);
	slot_end = 	(uint32)(0x01 << rount_q);
	// 1)Packet Build	
	if(inventory_prm.update)
	{		
		iso180006c_pktbuild_sel(&iso180006c_rfid_pkt_c.sel);
		iso180006c_pktbuild_qry(&iso180006c_rfid_pkt_c.qry);
		iso180006c_pktbuild_qryrep(&iso180006c_rfid_pkt_c.qryrep);
		EMI_SET_BIT(RM_QryRep_session, iso180006c_ctxt.qry.session);
		inventory_prm.update = FALSE;
	}

	if(iso180006c_ctxt.sel.mem_bank) // MemBank != RFU
	{
		// 2) Send Select
		modem_transmit_pkt(	Frame_Sync, CRC_16, 
							(uint16) iso180006c_rfid_pkt_c.sel.bit_length, 
							&iso180006c_rfid_pkt_c.sel.air_pkt[0]); // Send Select >
							
		//delay_10us( (640/modem_c.rx_blf) );
		delay_10us((modem_c.tx_tari/62) * 4);
	}
	
	// 3) Send Query
	modem_transmit_pkt(	Preamble, CRC_5, 
						(uint16) iso180006c_rfid_pkt_c.qry.bit_length, 
						&iso180006c_rfid_pkt_c.qry.air_pkt[0]); // Send Query >
		
	for(slot_cur = 0;slot_cur < slot_end; slot_cur++)
	{	
		// tag_buf buffer full 
		if(tag_buf.count >=  TAG_ID_MAX)
			return INVENTORY_TAG_BUF_FULL;

		// 4) RN16 + 5) ACK
		if(modem_receive_pkt(Auto_ACK_En, NO_CRC, T1_TYPICAL, 16, NULL))
		{	
			// 6) EPC + 7) REQUEST RN
			if(modem_receive_pkt(Auto_QryRep_En, CRC_16, T1_TYPICAL, 0, &tag_buf.id[tag_buf.count]))
			{
				tag_buf.count++;
				slot_tag++;
				continue;
			}
			else
			{
				slot_collision++;				
			}
		}
		else
		{
			slot_no_tag++;
		}
		
		delay_10us( (640/modem_c.rx_blf) );
		
		modem_transmit_pkt(	Frame_Sync, NO_CRC, 
							(uint16) iso180006c_rfid_pkt_c.qryrep.bit_length, 
							&iso180006c_rfid_pkt_c.qryrep.air_pkt[0]); // Send QueryRep and flush FIFO

	}

	anticol_update_round(slot_tag, slot_collision, slot_no_tag);

	delay_10us( (640/modem_c.rx_blf) );

	return slot_tag;
}


//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
BOOL iso180006c_handle_acquisition(const uint8 *target_ptr, uint8 target_len, rfid_pkt_rx_type *pc_epc_crc)
{
	iso180006c_prm_type bacup_param = {0, };	
	rfid_pkt_rx_type handle;
	uint8 ret = FALSE;
	uint8 err_cnt = 0;

	memcpy((void*) &bacup_param, (void*) &iso180006c_ctxt, sizeof(iso180006c_prm_type));

	if(target_len != 0)
	{
		memcpy((void*) &iso180006c_ctxt.sel.mask[0], (void*) target_ptr, target_len);
		iso180006c_ctxt.sel.len = (uint8)(target_len<<3); // bit legnth
	}
	else
	{		
		iso180006c_ctxt.sel.len = 0;
	}

	iso180006c_ctxt.sel.target = 0x00;
	iso180006c_ctxt.sel.action = 0x00;
	iso180006c_ctxt.sel.mem_bank = 0x01;
	iso180006c_ctxt.sel.pointer = 0x0020; // 00h CRC-16, 10h PC, 20h EPC
	iso180006c_ctxt.sel.truncate = 0x00;
	iso180006c_ctxt.qry.trext = (iso180006c_ctxt.qry.m & 0x02) ? 0 : 1;
	//iso180006c_ctxt.qry.trext = 1;
	iso180006c_ctxt.qry.sel = 0x00;
	iso180006c_ctxt.qry.session = 0x00;
	iso180006c_ctxt.qry.target = 0x00;
	iso180006c_ctxt.qry.q = 0x00;

	iso180006c_pktbuild_sel(&iso180006c_rfid_pkt_c.sel);
	iso180006c_pktbuild_qry(&iso180006c_rfid_pkt_c.qry);

	while( err_cnt < 5)
	{
		delay_10us( (640/modem_c.rx_blf) );
		
		modem_transmit_pkt(	Frame_Sync, CRC_16, 
							(uint16) iso180006c_rfid_pkt_c.sel.bit_length, 
							&iso180006c_rfid_pkt_c.sel.air_pkt[0]); // Send Select >

		//delay_10us( (640/modem_c.rx_blf) );		
		delay_10us((modem_c.tx_tari/62) * 4);
			
		modem_transmit_pkt(	Preamble, CRC_5, 
							(uint16) iso180006c_rfid_pkt_c.qry.bit_length, 
							&iso180006c_rfid_pkt_c.qry.air_pkt[0]); // Send Query >

		// 4) RN16 + 5) ACK		
		if(!modem_receive_pkt(Auto_ACK_En, NO_CRC, T1_TYPICAL, 16, NULL))
		{
			err_cnt++;
			continue;
		}

		// 6) EPC + 7) REQUEST RN
		if(!modem_receive_pkt(Auto_ReqRn_En, CRC_16, T1_TYPICAL, 0, pc_epc_crc))
		{
			err_cnt++;
			continue;				
		}

		// 8) HANDLE			 
		if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_TYPICAL, 32, &handle))
		{
			err_cnt++;
			continue;
		}		

		iso180006c_handle[0] = handle.air_pkt[0];
		iso180006c_handle[1] = handle.air_pkt[1];
		
		rf_set_gain(pc_epc_crc->air_pkt[pc_epc_crc->byte_length]+3, pc_epc_crc->air_pkt[pc_epc_crc->byte_length+1]+3);
		
		ret = TRUE;
		
		break;

	};

	memcpy((void*) &iso180006c_ctxt, (void*) &bacup_param, sizeof(iso180006c_prm_type));
	inventory_prm.update = TRUE;
	
	return ret;

}



//!---------------------------------------------------------------
//!	@brief
//!		Read Tag Memory Bank
//!
//! @param
//!		protocol_c_membank_type mb
//!		uint8 wort_prt
//!		uint8 word_count
//!		rfid_packet_long_type *TagReadData
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_read(const protocol_c_membank_type mb, 
						const uint16 wort_prt, 
						const uint8 word_count, 
						rfid_packet_long_type *tag_data)
{
	uint8 ret;
//	uint16 err_len = 25;
	rfid_packet_type cmd_read;
	uint8 word_count_mod;
	CRC_type crc;
			
	if(word_count == 0)
	{
		word_count_mod = 32;
//		word_count_mod = 4;
		crc = NO_CRC;		
	}
	else
	{
		word_count_mod = word_count;
		crc = CRC_16;
	}
			
	tag_data->bit_length = 0;
		
	// 1) Packet Build Read Cmd (6.3.2.10.3.2)
	typec_apbuild_read(&cmd_read, mb,wort_prt, word_count);

	delay_10us( (640/modem_c.rx_blf) );
		
	// 2) Send Read Cmd
	modem_transmit_pkt( Frame_Sync, CRC_16, 
						(uint16) cmd_read.bit_length, &cmd_read.air_pkt[0]); //! Send Select >

	// 3) Read
	//ret = modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_TYPICAL, (1 + (word_count_mod*16) + 16 + 16), NULL);	
	ret = modem_receive_pkt(Auto_Tx_Disable, crc, T1_TYPICAL, (1 + (word_count_mod*16) + 16 + 16), NULL);

	if(!ret)
	{
		tag_data->bit_length = 8;
	}
	else
	{
		tag_data->bit_length = modem_get_rxpkt_len();
	}	

	modem_get_rx_byte(&tag_data->bit_length, &tag_data->air_pkt[0]);

	if(!ret && (tag_data->air_pkt[0] & 0x80 == 0x80))
		return FALSE;		
	
	if(word_count == 0)
		ret = TRUE;
	
	return ret;	


}

//!---------------------------------------------------------------
//!	@brief
//!		Write Tag Memory Bank
//!
//! @param
//!		protocol_c_prm_read_erase_type mb
//!		uint8 wort_prt
//!		uint8 word_count
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_req_rn(rfid_packet_type *new_rn)
{

	uint8 cmd = ISO180006C_REQ_RN_CMD;
	rfid_packet_type CmdReqRN;

	//memset((void*) &CmdReqRN, 0x00, sizeof(rfid_packet_type));
	CmdReqRN.bit_length = 0;

	// 1) Packet Build     
	bit_append(&CmdReqRN, &cmd, 8); // Command(8)
	bit_append(&CmdReqRN, &iso180006c_handle[0], 8); // RN(16)
	bit_append(&CmdReqRN, &iso180006c_handle[1], 8); // RN(16)

	// 2) Send Req_RN Cmd
	modem_transmit_pkt(Frame_Sync, CRC_16, 
						(uint16) CmdReqRN.bit_length, &CmdReqRN.air_pkt[0]); // Send Cmd >
 
	// 3) Read New RN
	if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_TYPICAL, 32, NULL)) return FALSE;

	new_rn->bit_length = 32;

	modem_get_rx_byte(&new_rn->bit_length, &new_rn->air_pkt[0] ); 

	if( new_rn->bit_length != 32) return FALSE;
		
	return TRUE;
}



//!---------------------------------------------------------------
//!	@brief
//!		Write Tag Memory Bank
//!
//! @param
//!		protocol_c_membank_type mb
//!		uint8 wort_prt
//!		uint8 word_count
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_write(const protocol_c_membank_type mb, const uint16 wort_prt, const uint16 word_data)
{
	rfid_packet_type write;
	rfid_pkt_rx_type rx;
	uint8 xored_data[2];

	if(!iso180006c_req_rn(&write)) return FALSE;

	xored_data[0] = LSB(word_data) ^ write.air_pkt[0]; // word_data XOR RN16
	xored_data[1] = MSB(word_data) ^ write.air_pkt[1]; // word_data XOR RN16

	MSB(word_data) = xored_data[0];
	LSB(word_data) = xored_data[1];
	
	// 3) Packet Build Read Cmd (6.3.2.10.3.2)
	typec_apbuild_write(&write, mb, wort_prt, word_data);

	delay_10us( (640/modem_c.rx_blf) );

	// 4) Send Write Cmd
	modem_transmit_pkt(Frame_Sync,CRC_16, 
					(uint16) write.bit_length, &write.air_pkt[0]); // Send Select >

	if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_20MS, 33, &rx))
	{
		return FALSE;
	}
	else
	{
		if( (rx.air_pkt[0] & 0x80) == 0x80 )
			return FALSE;

		bit_array_shift_l(&rx.air_pkt[0],&rx.air_pkt[0], (uint16)33, 1);
		if(!((iso180006c_handle[0] == rx.air_pkt[0]) && (iso180006c_handle[1] == rx.air_pkt[1])))
		{
			return FALSE;
		}
	}

	return TRUE;
}



//!---------------------------------------------------------------
//!	@brief
//!		BlockWrite Tag Memory Bank
//!
//! @param
//!		protocol_c_membank_type mb
//!		uint8 wort_prt
//!		uint8 word_count
//!		uint16 word_data
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_blockwrite(const protocol_c_membank_type mb, 
							const uint16 wort_prt,
							const uint8 word_count,
							const uint16 *word_data)
{
	rfid_packet_long_type cmd_block_write;
	rfid_pkt_rx_type rx;

	// 3) Packet Build BlockWrite Cmd (6.3.2.10.3.2)
	typec_apbuild_blockwrite((rfid_packet_type*) &cmd_block_write, mb, wort_prt, word_count, word_data);

	delay_10us( (640/modem_c.rx_blf) );
	
	// 4) Send BlockWrite Cmd
	modem_transmit_pkt(Frame_Sync, CRC_16, 
					(uint16) cmd_block_write.bit_length, &cmd_block_write.air_pkt[0]); // Send BlockWrite >

	if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_20MS, 33, &rx))
	{
		return FALSE;
	}
	else
	{
		bit_array_shift_l(&rx.air_pkt[0],&rx.air_pkt[0], (uint16)33, 1);
		if(!((iso180006c_handle[0] == rx.air_pkt[0]) && (iso180006c_handle[1] == rx.air_pkt[1])))
		{
			return FALSE;
		}
	}
	
	return TRUE;	
}


//!---------------------------------------------------------------
//!	@brief
//!		BlockErase Tag Memory Bank
//!
//! @param
//!		protocol_c_membank_type mb
//!		uint8 wort_prt
//!		uint8 word_count
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_blockerase(const protocol_c_membank_type mb,
							const uint16 wort_prt, 
							const uint8 word_count)
{
	rfid_packet_type cmd_block_erase;
	rfid_pkt_rx_type rx;
	rx_t1_type t1_type = T1_20MS;
	BOOL ret = TRUE;
	uint8 i;

	// 3) Packet Build BlockErase Cmd (6.3.2.10.3.2)
	typec_apbuild_blockerase(&cmd_block_erase, mb, wort_prt, word_count);

	for(i=0; i<2; i++)
	{	
		delay_10us( (640/modem_c.rx_blf) );

		// 4) Send BlockErase Cmd
		modem_transmit_pkt(Frame_Sync, CRC_16, 
						(uint16) cmd_block_erase.bit_length, &cmd_block_erase.air_pkt[0]); // Send BlockErase >
	
		if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, t1_type, 33, &rx))
		{
			t1_type = T1_20MS_OFFSET;
			ret =  FALSE;
			continue;
		}
		else
		{
			bit_array_shift_l(&rx.air_pkt[0],&rx.air_pkt[0], (uint16)33, 1);
			if(!((iso180006c_handle[0] == rx.air_pkt[0]) && (iso180006c_handle[1] == rx.air_pkt[1])))
			{
				ret =  FALSE;
			}
			else 
			{
				ret = TRUE;
			}
			break;
		}
	}

	return ret;	
}


//!---------------------------------------------------------------
//!	@brief
//!		Kill Tag
//!
//! @param
//!		uint32 kill_password (Kill Password)
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_kill(const uint32 kill_password, const uint8 recom)
{	
	rfid_pkt_rx_type rx;
	rfid_packet_type kill;
	rfid_packet_type new_rn;
	uint8 xored_data[2];
	
	// 1) Get New RN (1)
	if(!iso180006c_req_rn(&new_rn)) return FALSE;
	
	xored_data[0] = MSB0(kill_password) ^ new_rn.air_pkt[0]; // word_data XOR RN16
	xored_data[1] = MSB1(kill_password) ^ new_rn.air_pkt[1]; // word_data XOR RN16
	
	// 2) Packet Build kill Cmd (1)
	typec_apbuild_kill(&kill, xored_data[0], xored_data[1], 0);

   	delay_10us( (640/modem_c.rx_blf) );

	// 3) Send kill Cmd (1)
	modem_transmit_pkt(Frame_Sync,CRC_16, 
					(uint16) kill.bit_length, &kill.air_pkt[0]); // Send kill
	
	// 4) Read first reply and Req_RN
	if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_TYPICAL, 32, NULL)) return FALSE;

	// 5) Get New_RN (2)
	if(!iso180006c_req_rn(&new_rn)) return FALSE;
		
	xored_data[0] = MSB2(kill_password) ^ new_rn.air_pkt[0]; // word_data XOR RN16
	xored_data[1] = MSB3(kill_password) ^ new_rn.air_pkt[1]; // word_data XOR RN16
	
	// 6) Packet Build kill Cmd (2)
	typec_apbuild_kill(&kill, xored_data[0], xored_data[1], recom);

	delay_10us( (640/modem_c.rx_blf) );

	// 7) Send kill Cmd (2)
	modem_transmit_pkt(Frame_Sync,CRC_16, 
					(uint16) kill.bit_length, &kill.air_pkt[0]); // Send kill >

	if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_20MS, 33, &rx))
	{
		return FALSE;
	}
	else
	{
		bit_array_shift_l(&rx.air_pkt[0],&rx.air_pkt[0], (uint16)33, 1);
		if(!((iso180006c_handle[0] == rx.air_pkt[0]) && (iso180006c_handle[1] == rx.air_pkt[1])))
		{
			return FALSE;
		}
	}
	
	return TRUE;
}


//!---------------------------------------------------------------
//!	@brief
//!		Access Tag
//!
//! @param
//!		uint32 access_password (Access Password)
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_access(const uint32 access_password)
{	
	rfid_packet_type access;
	rfid_packet_type new_rn;
	uint8 xored_data[2];

	// 1) Get New_RN (1)
	if(!iso180006c_req_rn(&new_rn)) return FALSE;
	
	xored_data[0] = MSB0(access_password) ^ new_rn.air_pkt[0]; // word_data XOR RN16
	xored_data[1] = MSB1(access_password) ^ new_rn.air_pkt[1]; // word_data XOR RN16
	
	// 2) Packet Build access Cmd (1)
	typec_apbuild_access(&access, xored_data[0], xored_data[1]);

	delay_10us( (640/modem_c.rx_blf) );
	// 3) Send access Cmd (1)
	modem_transmit_pkt(Frame_Sync,CRC_16, 
					(uint16) access.bit_length, &access.air_pkt[0]); // Send Select >


	// 4) Read reply(1)
	if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_TYPICAL, 32, NULL)) 
	{
		error_value = 0x01;// modify jwkim, 20121025
		return FALSE;
	}

	// 5) Get New_RN (2)
	if(!iso180006c_req_rn(&new_rn)) return FALSE;

	xored_data[0] = MSB2(access_password) ^ new_rn.air_pkt[0]; // word_data XOR RN16
	xored_data[1] = MSB3(access_password) ^ new_rn.air_pkt[1]; // word_data XOR RN16

	// 6) Packet Build access Cmd (2)
	typec_apbuild_access(&access, xored_data[0], xored_data[1]);

	delay_10us( (640/modem_c.rx_blf) );
	// 7) Send access Cmd (2)
	modem_transmit_pkt(Frame_Sync,CRC_16, 
					(uint16) access.bit_length, &access.air_pkt[0]); // Send Select >

	// 8) Read reply (2)
	if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, T1_TYPICAL, 32, NULL))
	{
		error_value = 0x01;// modify jwkim, 20121025
		return FALSE;
	}

	return TRUE;
}


//!---------------------------------------------------------------
//!	@brief
//!		Lock Tag
//!
//! @param
//!		uint32 access_password (Access Password)
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_lock(const uint32 mask_and_action)
{	
	rfid_pkt_rx_type rx;
	rfid_packet_type lock;
	uint8 cmd = ISO180006C_LOCK_CMD;
	rx_t1_type t1_type = T1_20MS;
	uint8 ret = TRUE;
	uint8 i;


	lock.bit_length = 0;

	// 1) Packet Build lock Cmd
	bit_append(&lock, &cmd, 8); // Command(8)
	bit_append(&lock, (uint8*) &MSB1(mask_and_action), 4); 
	bit_append(&lock, (uint8*) &MSB2(mask_and_action), 8); 
	bit_append(&lock, (uint8*) &MSB3(mask_and_action), 8); 
	bit_append(&lock, &iso180006c_handle[0], 8); // RN(16)
	bit_append(&lock, &iso180006c_handle[1], 8); // RN(16)

	for(i=0; i<2; i++)
	{	
	 	delay_10us( (640/modem_c.rx_blf) );

		// 2) Send lock Cmd 
		modem_transmit_pkt(Frame_Sync,CRC_16, 
						(uint16) lock.bit_length, &lock.air_pkt[0]); // Send Select >
		
		if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, t1_type, 33, &rx))
		{
			t1_type = T1_20MS_OFFSET;
			ret =  FALSE;
			continue;
		}
		else
		{
			bit_array_shift_l(&rx.air_pkt[0],&rx.air_pkt[0], (uint16)33, 1);
			if(!((iso180006c_handle[0] == rx.air_pkt[0]) && (iso180006c_handle[1] == rx.air_pkt[1])))
			{
				ret = FALSE;				
			}
			else
			{
				ret =  TRUE;
			}
			break;
		}
	}
	return ret;
}  


#ifdef __FEATURE_SIM_TAG__
void iso180006c_push_simtag()
{
	uint8 sim_tag[] = {0x30, 0x00, 0x30, 0x35, 0x31, 0xCA, 0xE8, 0x0A, 0xEC, 0x80, 0x00, 0x27, 0x61, 0xE9, 0x87, 0xD5};	
	
//	tag_buf.count = 0;
	if(tag_buf.count >= TAG_ID_MAX)
		return;
	
	tag_buf.id[tag_buf.count].byte_length = sizeof(sim_tag);	
	memcpy((void*)&tag_buf.id[tag_buf.count].air_pkt[0],(void*)&sim_tag[0], sizeof(sim_tag));
	tag_buf.count++;

	delay_ms(10);
}
#endif


#ifdef __FEATURE_NXP_UCODE__ 
//!---------------------------------------------------------------
//!	@brief
//!		ReadProtect (Custom Commands)
//!
//! @param
//!		None
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_readprotect()
{
	rfid_pkt_rx_type rx;
	rfid_packet_type cmd_read_protect;		
	uint16 cmd = NXP_READPROTECT_CMD;
 	rx_t1_type t1_type = T1_20MS;
	BOOL ret = TRUE;
	uint8 i;

	cmd_read_protect.bit_length = 0;
	
	// 1) Packet Build     
	bit_append(&cmd_read_protect, &MSB(cmd), 8); // Command(16)
	bit_append(&cmd_read_protect, &LSB(cmd), 8); // Command(16)
	bit_append(&cmd_read_protect, &iso180006c_handle[0], 8); // RN(16)
	bit_append(&cmd_read_protect, &iso180006c_handle[1], 8); // RN(16)

	for(i=0; i<2; i++)
	{	
	 	delay_10us( (640/modem_c.rx_blf) );

	// 2) Send ReadProtect Cmd
		modem_transmit_pkt(Frame_Sync, CRC_16, 
						(uint16) cmd_read_protect.bit_length, &cmd_read_protect.air_pkt[0]); //! Send Select >
		
		if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, t1_type, 33, &rx))
		{
			t1_type = T1_20MS_OFFSET;
			ret = FALSE;
			continue;
		}
		else
		{
			bit_array_shift_l(&rx.air_pkt[0],&rx.air_pkt[0], (uint16)33, 1);
			if(!((iso180006c_handle[0] == rx.air_pkt[0]) && (iso180006c_handle[1] == rx.air_pkt[1])))
			{
				ret = FALSE;
			}
			else
			{
				ret = TRUE;
			}
			break;
		}
	}
	
	return ret;
	
}


//!---------------------------------------------------------------
//!  @brief
//!	 	Reset ReadProtect (Custom Commands)
//!
//!	@param
//!	 	uint32 reset_readprotect_password (Reset Password)
//!
//!	@return
//!	 	BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_reset_readprotect(uint32 reset_readprotect_password)
{
	rfid_pkt_rx_type rx;	 
	rfid_packet_type cmd_reset_read_protect;
	uint16 new_rn;
	uint8 xored_data[4]; 
	uint16 cmd = NXP_RESET_READPROTECT_CMD;
	rx_t1_type t1_type = T1_20MS;
	BOOL ret = TRUE;
	uint8 i;

	cmd_reset_read_protect.bit_length = 0;

	MSB(new_rn) = iso180006c_handle[0]; // RN16
	LSB(new_rn) = iso180006c_handle[1]; // RN16
 
	xored_data[0] = MSB0(reset_readprotect_password) ^ MSB(new_rn); // Password XOR RN16
	xored_data[1] = MSB1(reset_readprotect_password) ^ LSB(new_rn); // Password XOR RN16
	xored_data[2] = MSB2(reset_readprotect_password) ^ MSB(new_rn); // Password XOR RN16
	xored_data[3] = MSB3(reset_readprotect_password) ^ LSB(new_rn); // Password XOR RN16

	bit_append(&cmd_reset_read_protect, &MSB(cmd), 8); // Command(16)
	bit_append(&cmd_reset_read_protect, &LSB(cmd), 8); // Command(16)
	bit_append(&cmd_reset_read_protect, (uint8*) &xored_data[0], 8); // Password MSB(8)
	bit_append(&cmd_reset_read_protect, (uint8*) &xored_data[1], 8); // Password LSB(8)
	bit_append(&cmd_reset_read_protect, (uint8*) &xored_data[2], 8); // Password MSB(8)
	bit_append(&cmd_reset_read_protect, (uint8*) &xored_data[3], 8); // Password LSB(8)
 	bit_append(&cmd_reset_read_protect, &MSB(new_rn), 8); // RN(16)
	bit_append(&cmd_reset_read_protect, &LSB(new_rn), 8); // RN(16)

 	for(i=0; i<2; i++)
	{	
	 	delay_10us( (640/modem_c.rx_blf) );
	   	
		// 2) Send Reset ReadProtect Cmd
		modem_transmit_pkt(Frame_Sync, CRC_16, 
						(uint16) cmd_reset_read_protect.bit_length, &cmd_reset_read_protect.air_pkt[0]); //! Send Select >
	
		if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, t1_type, 33, &rx))
		{
			t1_type = T1_20MS_OFFSET;
			ret = FALSE;
			continue;
		}
		else
		{
			bit_array_shift_l(&rx.air_pkt[0],&rx.air_pkt[0], (uint16)33, 1);
			if(!((iso180006c_handle[0] == rx.air_pkt[0]) && (iso180006c_handle[1] == rx.air_pkt[1])))
			{
				ret = FALSE;
			}
			else
			{
				ret = TRUE;
			}	 
			break;
		}
	}
				
	return ret;

}
 
 
//!---------------------------------------------------------------
//!  @brief
//!	  	ChangeEAS (Custom Commands)
//!
//!	@param
//!	  	uint8 change (1 = set EAS system bit, 0 = reset EAS system bit)
//!
//!	@return
//!	  	BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_change_eas(uint8 change)
{
	rfid_pkt_rx_type rx;
	rfid_packet_type cmd_change_eas;  
	uint16 cmd = NXP_CHANGE_EAS;
	rx_t1_type t1_type = T1_20MS;
	BOOL ret = TRUE;
	uint8 i;
	
	// 1) Packet Build
	cmd_change_eas.bit_length = 0;
	bit_append(&cmd_change_eas, &MSB(cmd), 8); // Command(16)
	bit_append(&cmd_change_eas, &LSB(cmd), 8); // Command(16)
	bit_append(&cmd_change_eas, &change, 1); // changeEAS(1)
	bit_append(&cmd_change_eas, &iso180006c_handle[0], 8); // RN(16)
	bit_append(&cmd_change_eas, &iso180006c_handle[1], 8); // RN(16)

	for(i=0; i<2; i++)
	{	
	 	delay_10us( (640/modem_c.rx_blf) );  
	
		// 2) Send ChangeEAS Cmd
		modem_transmit_pkt(Frame_Sync, CRC_16, 
						(uint16) cmd_change_eas.bit_length, &cmd_change_eas.air_pkt[0]); //! Send Select >
	
		if(!modem_receive_pkt(Auto_Tx_Disable, CRC_16, t1_type, 33, &rx))
		{
			t1_type = T1_20MS_OFFSET;
			ret = FALSE;
			continue;
		}
		else
		{
			bit_array_shift_l(&rx.air_pkt[0],&rx.air_pkt[0], (uint16)33, 1);
			if(!((iso180006c_handle[0] == rx.air_pkt[0]) && (iso180006c_handle[1] == rx.air_pkt[1])))
			{
				ret = FALSE;
			}
			else
			{
				ret = TRUE;
			}
			break;
		}
	}
				
	return ret;
}
 

//!---------------------------------------------------------------
//!  @brief
//!	  	EAS_Alarm (Custom Commands)
//!
//!	@param
//!		uint8 dr
//!		uint8 m
//!		uint8 trext
//!		rfid_packet_long_type *TagReadData
//!	  
//!
//!	@return
//!	  	BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_eas_alarm(rfid_packet_long_type *tag_data)
{	
	uint16 i, cnt = 10, valid_cnt = 0;
	uint8 ret = FALSE;
	BOOL cmp = TRUE; 
	rfid_packet_type eas_alarm;
	rfid_packet_long_type temp;	

	uint16 cmd = NXP_EAS_ALARM;
//	uint16 inv_cmd = NXP_INV_EAS_ALARM;
	uint16 inv_cmd = (uint16) ~NXP_EAS_ALARM;

	tag_data->air_pkt[0] = 0; // set header = 0

	// 1) Packet Build
	eas_alarm.bit_length = 0;
  
	bit_append(&eas_alarm, &MSB(cmd), 8); // Command(16)
	bit_append(&eas_alarm, &LSB(cmd), 8); // Command(16)
	bit_append(&eas_alarm, &MSB(inv_cmd), 8); // Inverted Command(16)
	bit_append(&eas_alarm, &LSB(inv_cmd), 8); // Inverted Command(16)
	bit_append(&eas_alarm, &iso180006c_ctxt.qry.dr, 1); // DR(1)
	bit_append(&eas_alarm, &iso180006c_ctxt.qry.m, 2); // M(2)
	iso180006c_ctxt.qry.trext = (iso180006c_ctxt.qry.m & 0x02) ? 0 : 1;
	bit_append(&eas_alarm, &iso180006c_ctxt.qry.trext, 1); // TRext(1)
	
	while(cnt > 0)
	{
		delay_10us( (640/modem_c.rx_blf) );

		// 2) Send EAS_Alarm
		modem_transmit_pkt(Preamble, CRC_16, 
						(uint16) eas_alarm.bit_length, &eas_alarm.air_pkt[0]); // Send EAS Alarm >

		if(!modem_receive_pkt(Auto_Tx_Disable, NO_CRC, T1_TYPICAL, 65, NULL))		
		{
			ret = FALSE;
			break;
		}		
		tag_data->bit_length = 65;		
		modem_get_rx_byte(&tag_data->bit_length, &tag_data->air_pkt[0]); 

		temp.bit_length = tag_data->bit_length;
		for(i=0; i<temp.bit_length; i++)
		{
			if(!(temp.air_pkt[i] == tag_data->air_pkt[i])) cmp = FALSE;

			temp.air_pkt[i] = tag_data->air_pkt[i];
		}

		if(cmp == TRUE)
		{
			valid_cnt++;
		}
		else
		{
			valid_cnt = 0;
			cmp = TRUE;
		}

		if(valid_cnt > 2)
		{
			bit_array_shift_l(&tag_data->air_pkt[0],&tag_data->air_pkt[0], (uint16)64, 1);
			ret =  TRUE;
			break;
		}
		
		cnt--;
		
	}
	
	return ret;
}
 
//!---------------------------------------------------------------
//!	@brief
//!		Calibrate (Custom Commands)
//!
//! @param
//!		rfid_packet_long_type *TagReadData
//!
//! @return
//!		BOOL: TRUE - Success / FALSE - Fail
//!
//!---------------------------------------------------------------
BOOL iso180006c_calibrate(rfid_packet_long_type *tag_data)
{
	rfid_packet_type cmd_cal;			
	uint16 cmd = NXP_CALIBRATE;
	
	cmd_cal.bit_length = 0;
	// 1) Packet Build     
	bit_append(&cmd_cal, &MSB(cmd), 8); // Command(16)
	bit_append(&cmd_cal, &LSB(cmd), 8); // Command(16)
	bit_append(&cmd_cal, &iso180006c_handle[0], 8); // RN(16)
	bit_append(&cmd_cal, &iso180006c_handle[1], 8); // RN(16)

	delay_10us( (640/modem_c.rx_blf) );

	// 2) Send ReadProtect Cmd
	modem_transmit_pkt(Frame_Sync, CRC_16, 
					(uint16) cmd_cal.bit_length, &cmd_cal.air_pkt[0]); //! Send Select >

	if(!modem_receive_pkt(Auto_Tx_Disable, NO_CRC, T1_TYPICAL, 513, NULL))
	{
		return FALSE;
	}

	tag_data->bit_length = 513;
	modem_get_rx_byte(&tag_data->bit_length, &tag_data->air_pkt[0] );
	bit_array_shift_l(&tag_data->air_pkt[0],&tag_data->air_pkt[0], (uint16)512, 1);

	return TRUE;
}

#endif
