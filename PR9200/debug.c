//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	debug.c
//! @brief	Implementation of debug service
//! 
//! $Id: debug.c 1632 2012-06-26 06:57:30Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi		initial release


//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  
#include <stdio.h>
#include <stdarg.h>
#include "PR9200.h"
#include "rcp.h"




//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------


//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 


//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    


//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  


#ifdef	__DEBUG__

//!---------------------------------------------------------------
//!	@brief
//!		
//!
//! @param
//!		
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void debug_msg_str_(const char *Format, ...)
{
	char output[128];
	va_list argptr;
	uint8 num = 0;
	
	va_start(argptr,Format);
	num = (uint8) vsprintf(output, Format, argptr);
	va_end(argptr);	

	rcp_write_string(RCP_MSG_NOTI,RCP_MSD_DEBUG,(uint8*) &output[0], num);
}


#endif 


#ifdef	__AJT_DEBUG__

void debug_msg_str_2(const char *Format, ...)
{
	char output[128];
	va_list argptr;
	uint8 num = 0;
	
	va_start(argptr,Format);
	num = (uint8) vsprintf(output, Format, argptr);
	va_end(argptr);	

	rcp_write_string(RCP_MSG_NOTI,RCP_MSD_DEBUG,(uint8*) &output[0], num);
}

#endif
