//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	app_nxp_led.c
//! @brief  Example of user application - LED
//! 
//! $Id: app_nxp_led.c 1738 2012-08-31 07:56:46Z sjpark $ 
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/01/27	jsyi		initial release




//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "pr9200.h"
#include "hal.h"
#include "app_nxp.h"
#include "app_nxp_led.h"




#define LED_NOTE_MAX		3


//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------  
extern nxp_ctxt_type nxp_fsm_ctxt;
//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    

//const
nxp_led_perform_type LED_PERFORM_CONFIG[] =
{
	{LED_TIME_100MS, LED_TIME_900MS, 	LED_TIME_60S},	  // step8 led mode	
	{LED_TIME_60S, 	 LED_TIME_ZERO, 	LED_TIME_60S}, 	  // step14 led mode
	{LED_TIME_500MS, LED_TIME_500MS, 	LED_TIME_10S}	  // step16 led mode
	
	// configure your settings below! :)
};	

nxp_led_ctxt_type nxp_led_ctxt = {0, 0};

hal_gpio_out_type	led0 
	= {{HAL_GPIO_DEV_LED, 0, 4, HAL_GPIO_POL_NORMAL},
		NULL,NULL,NULL};
hal_gpio_out_type	led1 
	= {{HAL_GPIO_DEV_LED, 0, 5, HAL_GPIO_POL_NORMAL},
		NULL,NULL,NULL};
hal_gpio_out_type	led2 
	= {{HAL_GPIO_DEV_LED, 0, 6, HAL_GPIO_POL_NORMAL},
		NULL,NULL,NULL};


//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  




//!---------------------------------------------------------------
//!	@brief
//!		Turns off all LEDs
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------

void nxp_led_off()
{
	led0.off_cb(&led0);
	led1.off_cb(&led1);
	led2.off_cb(&led2);	
}

//!---------------------------------------------------------------
//!	@brief
//!		Turns on specific LED
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_led_on()
{
	
	if(nxp_led_ctxt.cur_val & 0x01)
		led0.on_cb(&led0);

	if(nxp_led_ctxt.cur_val & 0x02)
		led1.on_cb(&led1);		

	if(nxp_led_ctxt.cur_val & 0x04)
		led2.on_cb(&led2);
}


//!---------------------------------------------------------------
//!	@brief
//!		Initialize the LEDs
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_led_init()
{		

	hal_gpio_register(&led0);
	hal_gpio_register(&led1);
	hal_gpio_register(&led2);

	nxp_led_off();
}



//!---------------------------------------------------------------
//!	@brief
//!		Interrupt service routine for led
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_led_event(EVENT e)
{

	switch(e.param)
	{
	case EVENT_NXP_LED_STOP:
		{
			nxp_led_stop();		
			nxp_led_handler();			
		}
		break;
	case EVENT_NXP_LED_ON:
		{			
			e.param = EVENT_NXP_LED_OFF;
			event_post_delayed(e, LED_PERFORM_CONFIG[nxp_led_ctxt.cur_mode].on_cnt);
			
			nxp_led_on();
 		}
		break;
	case EVENT_NXP_LED_OFF:
		{
			e.param = EVENT_NXP_LED_ON;
			event_post_delayed(e, LED_PERFORM_CONFIG[nxp_led_ctxt.cur_mode].off_cnt);
			
			nxp_led_off();
		}
		break;

	}


}

//!---------------------------------------------------------------
//!	@brief
//!		Stop the LED operaion
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_led_stop()
{
	nxp_led_off();
	event_flush(nxp_led_event);
	nxp_fsm_ctxt.led_status = LED_STOP;	
}

//!---------------------------------------------------------------
//!	@brief
//!		Executes the led operation
//!
//! @param
//!		None
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------
void nxp_led_perform(uint8 mode, uint8 val)
{
	EVENT e;

	nxp_led_ctxt.cur_mode = mode;
	nxp_led_ctxt.cur_val = val;
	
	e.handler = nxp_led_event; 

 	e.param = EVENT_NXP_LED_ON;
	event_post(e);

	e.param = EVENT_NXP_LED_STOP;
	event_post_delayed(e, LED_PERFORM_CONFIG[nxp_led_ctxt.cur_mode].duration);

	nxp_fsm_ctxt.led_status = LED_PERFORM;
}


void nxp_led_set_time(uint8 mode, nxp_led_perform_type *param)
{
	LED_PERFORM_CONFIG[mode].on_cnt = param->on_cnt;
	LED_PERFORM_CONFIG[mode].off_cnt = param->off_cnt;
	LED_PERFORM_CONFIG[mode].duration = param->duration;			
}

