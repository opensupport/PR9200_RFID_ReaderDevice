//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file 	crc.c
//! @brief	CRC Module
//! 
//! $Id: crc.c 1597 2012-05-04 07:48:02Z jsyi $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2011/07/25	sjpark		initial release


//!-------------------------------------------------------------------                 
//! Include Files
//!-------------------------------------------------------------------  	
#include "PR9200.h"
#include "crc.h"


//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 



//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    


//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  


//!---------------------------------------------------------------
//!	@brief
//!		Polynomial functions for CRC (X16 + X12 + X5 + 1)
//!
//! @param
//!		b: a byte data
//!		acc: accumulated value
//!
//! @return
//!		accumulated value
//!
//!---------------------------------------------------------------
uint16 crc_polynomial(uint8 b, uint16 acc)
{
    acc  = (uint8)(acc >> 8) | (acc << 8);
    acc ^= b;
    acc ^= (uint8)(acc & 0xff) >> 4;
    acc ^= (acc << 8) << 4;
    acc ^= ((acc & 0xff) << 4) << 1;

    return acc;
}

//!---------------------------------------------------------------
//!	@brief
//!		Caculate 16-bit CRC on all the messages
//!
//! @param
//!		* msg:	data pointer
//!		length: data length
//! 	crc: CRC initial value(0xFFFF)
//!
//! @return
//!		crc value
//!
//!---------------------------------------------------------------
uint16 crc_sum(uint8* msg, uint16 length, uint16 crc)
{
	uint16 i;

    for(i = 0; i < length; i++) {
        crc = crc_polynomial(msg[i],crc);
    }
    return crc;
}

//!---------------------------------------------------------------
//!	@brief
//!		Check the CRC validity 	
//!
//! @param
//!		* msg: data pointer
//!		length: data length 
//!
//! @return
//!		valid(0) / invalid(Not 0)
//!
//!---------------------------------------------------------------	
uint32 crc_verify(uint8* msg, uint16 length)
{
	uint16 expected;

    expected = crc_sum(msg, length - 2, CRC_INIT);
    return !( ( expected       & 0xff) == msg[length - 1] &&
              ((expected >> 8) & 0xff) == msg[length - 2] );
}


//!---------------------------------------------------------------
//!	@brief
//!		Append the CRC value to the end of data
//!
//! @param
//!		* message: data pointer
//!		length: data length 
//!
//! @return
//!		None
//!
//!---------------------------------------------------------------	
void crc_append(uint8* message, uint16 length)
{
#ifdef	__FEATURE_ENABLE_RCP_CRC__

	uint16 crc;

    crc = crc_sum(message, length, CRC_INIT);
    message[length+1] = (uint8)(crc & 0xff);
    message[length] = (uint8)((crc >> 8) & 0xff);

#endif
}

