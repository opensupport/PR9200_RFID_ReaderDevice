//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	utility.c
//! @brief	Implementation of utility service
//! 
//! $Id: utility.c 1502 2012-03-16 09:41:05Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2007/09/01	jsyi		initial release



//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "PR9200.h" 
#include "utility.h"

//!-------------------------------------------------------------------                 
//! External Data References                                                  
//!-------------------------------------------------------------------


//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 


//!-------------------------------------------------------------------                 
//! Global Data Declaration                                                       
//!-------------------------------------------------------------------    


//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  

#ifdef __INTERNAL__LIB__
//!---------------------------------------------------------------
//!	@brief
//!		The strlen function calculates the length, in bytes, of src. 
//!
//! @param
//!		src: source string
//!
//! @return
//! 	The strlen function returns the length of src.
//!		
//!
//!---------------------------------------------------------------
int strlen (char *src)
{
	int i = 0;
	
	while (src[i] != 0) i++;
	
	return i;
}


//!---------------------------------------------------------------
//!	@brief
//!		The memset function sets the first len bytes in buf to c.
//!
//! @param
//!		*buf: buffer to initialize
//!		c: * byte value to set
//!		len:  buffer length
//!
//! @return
//!		The memset function returns dest.
//!
//!---------------------------------------------------------------
void* memset(void* buf, char c, int len)
{
	char *d;
	
	d = (char*) buf;	

	while (len-- != 0)
	{
		*d++ =  c;
	}	
	
	return (void*) d;
}


//!---------------------------------------------------------------
//!	@brief
//!		The memset function sets the first len bytes in buf to c.
//!
//! @param
//!		*dest: destination buffer
//!		*src: source buffer
//!		len:  buffer length
//!
//! @return
//!		The memset function returns dest.
//!
//!--------------------------------------------------------------
void* memcpy(void* dest, void *src, int len)
{
	char *d;
	char *s;
	
	d = (char*) dest;
	s = (char*) src;
	
	while (len-- != 0)
	{
	  *d++ = *s++;
	}	
	
	return (void*) d;
}

#endif

//!---------------------------------------------------------------
//!	@brief
//!		bit_align
//!
//! @param
//!		src: source byte
//!		BitLength: bit_length
//!		BitOffset: positive val : left shift, negative val : right shift
//!
//! @return
//!		uint8 - aligned value
//!
//!---------------------------------------------------------------
uint8 bit_align(uint8 source_byte, uint8 bit_length, int8 lsb_offset)
{	
	if(lsb_offset> 0)
	{
		return (source_byte & ( (1<< bit_length)-1 ) )<< lsb_offset;
	}
	else
	{
		return ((source_byte >> (0 - lsb_offset) ) & (  (1<< bit_length)-1 ) );		
	}
}


//!---------------------------------------------------------------
//!	@brief
//!		Bit Array Shift Left
//!
//! @param
//!		dst: destination byte (return value)
//!		src: source byte
//!		BitLength: bit_length
//!		BitOffset: left shift offset
//!
//!	@return
//!		NONE
//!
//!---------------------------------------------------------------
void bit_array_shift_l(uint8* dst, uint8* src, uint16 BitLength, uint8 BitOffset)
{
	uint16 i;

	for(i = 0; i < (((BitLength + BitOffset) + 7) >> 3) ; i++)
	{
		dst[i] = bit_align(src[i],8-BitOffset,BitOffset);
		
		if( ((BitLength >> 3) - i ) > 0 )
		{
			dst[i] |= bit_align(src[i+1],BitOffset,-8 + BitOffset);
		}
	}
}

/*
//!---------------------------------------------------------------
//!	@brief
//!		delay_long
//!
//! @param
//!		i: delay_long time
//!
//! @return
//!		NONE
//!
//!---------------------------------------------------------------

void delay_long (uint16 i)
{
	uint16 loop;
	
	for (loop=0; loop<i; loop++);	
}
*/


