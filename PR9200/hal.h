//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	hal.h
//! @brire	Hardware Abstract Layer
//! 
//! $Id: hal.h 1763 2012-09-24 06:56:31Z sjpark $			
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/03/02	sjpark	Initial release
//! 2012/03/13	jsyi	Revisied



#ifndef HAL_H
#define HAL_H

//!-------------------------------------------------------------------                 
//! Include Files
//!------------------------------------------------------------------- 
#include "config.h"
#include "commontypes.h"
#include "rfidtypes.h"

//!-------------------------------------------------------------------                 
//! Definitions
//!------------------------------------------------------------------- 

//!-------------------------------------------------------------------                 
//! Macros
//!------------------------------------------------------------------- 

//!-------------------------------------------------------------------                 
//! Strunctures and enumerations
//!------------------------------------------------------------------- 

//!-------------------------------------------------------------------                 
//! Fuction Definitions
//!-------------------------------------------------------------------  
SYS_S hal_init(void);
SYS_S hal_get_fhlbt_prm(hal_fhlbt_prm_type *prm);
SYS_S hal_set_fhlbt_prm(const hal_fhlbt_prm_type *prm);
SYS_S hal_set_sys_deep_sleep(void);
SYS_S hal_set_sys_sleep(void);
SYS_S hal_set_rfidblk_pwr(uint8 mode);
SYS_S hal_set_cw(uint8 mode);
SYS_S hal_get_rssi(uint16 *rssi16);
SYS_S hal_get_tx_pwr_lvl(int16 *pwr_pa);
SYS_S hal_set_tx_pwr_lvl(int16 pwr_pa);
SYS_S hal_set_ch(uint8 ch, uint8 ch_ext);
SYS_S hal_get_freq_hopping_table(hal_rf_freq_hopping_table_type *tbl);
SYS_S hal_set_freq_hopping_table(hal_rf_freq_hopping_table_type *tbl);
SYS_S hal_set_region(uint8  region);
SYS_S hal_get_region(uint8 *region);
SYS_S hal_get_ch(uint8 *ch, uint8 *ch_ext);
SYS_S hal_get_registry(uint16 reg_address, uint8 *ret_len, uint8 *ret_byte);
SYS_S hal_update_registry(void);
SYS_S hal_erase_registry(void);
SYS_S hal_start_download(void);
SYS_S hal_i2c_master_init(void);
SYS_S hal_i2c_master_read(uint8 slave_addd, uint8 * data,uint16 length);
SYS_S hal_i2c_master_write(uint8 slave_addd, uint8 * data,uint16 length);
SYS_S hal_gpio_register(void* hal_gpio_id);
SYS_S hal_timer_register(hal_timer_type* timer);
SYS_S hal_timer_deregister(void);
SYS_S hal_get_temperature(int8 *temp);
SYS_S hal_get_cw_detected(uint8 *cw_detected);
SYS_S hal_power_off(void);
SYS_S hal_set_serial_no(hal_serial_no_type *tbl);
SYS_S hal_get_power_off_time(uint16 *time);
SYS_S hal_set_power_off_time(uint16 time);
#endif

