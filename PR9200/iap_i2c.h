//!-------------------------------------------------------------------
//! Copyright (C) 2007-2012 PHYCHIPS
//!
//! @file	iap_i2c.h
//! @brief	I2C Device Driver for iap
//! 
//! $Id: iap_i2c.h 1763 2012-09-24 06:56:31Z sjpark $
//!-------------------------------------------------------------------
//! History
//!-------------------------------------------------------------------
//! 2012/08/31	sjpark	initial release

#ifndef IAP_I2C_H
#define IAP_I2C_H


void iap_i2c_slave_init( void );												 
void iap_i2c_send_frame(uint8 cmd, uint8 *data, uint8 len, uint8 h_crc, uint8 l_crc);	 
int32 iap_i2c_get_char(void);
uint8 iap_i2c_get_status(void);
#endif
